Imports System.Diagnostics
Imports System.Data
Imports System.Collections
Imports System.Collections.Generic
Imports System
Imports System.Web.Caching
Imports System.Reflection
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Text
Imports System.IO


Namespace DotNetSCORM.DataTools


    '*********************************************************************
    '
    ' CBO Class
    '
    ' Class that hydrates custom business objects with data
    '
    '*********************************************************************

    Public Class CBO


        Public Shared Function GetPropertyInfo(ByVal objType As Type) As ArrayList

            ' Use the cache because the reflection used later is expensive
            Dim objProperties As ArrayList = DirectCast(DataCache.GetCache(objType.FullName), ArrayList)

            If objProperties Is Nothing Then
                objProperties = New ArrayList()
                Dim objProperty As PropertyInfo
                For Each tempLoopVar_objProperty As PropertyInfo In objType.GetProperties()
                    objProperty = tempLoopVar_objProperty
                    objProperties.Add(objProperty)
                Next
                DataCache.SetCache(objType.FullName, objProperties)
            End If

            Return objProperties

        End Function

        Private Shared Function GetOrdinals(ByVal objProperties As ArrayList, ByVal dr As IDataReader) As Integer()

            Dim arrOrdinals As Integer() = New Integer(objProperties.Count) {}
            Dim intProperty As Integer

            If dr IsNot Nothing Then
                For intProperty = 0 To objProperties.Count - 1
                    arrOrdinals(intProperty) = -1
                    Try
                        arrOrdinals(intProperty) = dr.GetOrdinal(DirectCast(objProperties(intProperty), PropertyInfo).Name)
                        ' property does not exist in datareader
                    Catch
                    End Try
                Next
            End If

            Return arrOrdinals

        End Function

        Private Shared Function CreateObject(ByVal objType As Type, ByVal dr As IDataReader, ByVal objProperties As ArrayList, ByVal arrOrdinals As Integer()) As Object

            Dim objObject As Object = Activator.CreateInstance(objType)
            Dim intProperty As Integer
            For intProperty = 0 To objProperties.Count - 1

                ' fill object with values from datareader
                If DirectCast(objProperties(intProperty), PropertyInfo).CanWrite Then
                    If arrOrdinals(intProperty) <> -1 Then
                        If Convert.IsDBNull(dr.GetValue(arrOrdinals(intProperty))) Then
                            ' translate Null value
                            DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, Null.SetNull(DirectCast(objProperties(intProperty), PropertyInfo)), Nothing)
                        Else
                            Try
                                ' try implicit conversion first
                                DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, dr.GetValue(arrOrdinals(intProperty)), Nothing)
                            Catch
                                ' data types do not match
                                Try
                                    Dim pType As Type = DirectCast(objProperties(intProperty), PropertyInfo).PropertyType
                                    'need to handle enumeration conversions differently than other base types
                                    If pType.BaseType.Equals(GetType(System.Enum)) Then
                                        DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, System.[Enum].ToObject(pType, dr.GetValue(arrOrdinals(intProperty))), Nothing)
                                    Else
                                        ' try explicit conversion
                                        DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, Convert.ChangeType(dr.GetValue(arrOrdinals(intProperty)), pType), Nothing)
                                    End If
                                Catch
                                    ' error assigning a datareader value to a property
                                    DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, Null.SetNull(DirectCast(objProperties(intProperty), PropertyInfo)), Nothing)
                                End Try
                            End Try
                        End If
                    Else
                        ' property does not exist in datareader
                        DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, Null.SetNull(DirectCast(objProperties(intProperty), PropertyInfo)), Nothing)
                    End If
                End If
            Next

            Return objObject

        End Function

        Public Shared Function FillObject(ByVal dr As IDataReader, ByVal objType As Type) As Object

            Dim objFillObject As Object
            '				int intProperty;

            ' get properties for type
            Dim objProperties As ArrayList = GetPropertyInfo(objType)

            ' get ordinal positions in datareader
            Dim arrOrdinals As Integer() = GetOrdinals(objProperties, dr)

            ' read datareader
            If dr.Read() Then
                ' fill business object
                objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals)
            Else
                objFillObject = Nothing
            End If

            ' close datareader
            If dr IsNot Nothing Then
                dr.Close()
            End If

            Return objFillObject

        End Function

        Public Shared Function FillCollection(ByVal dr As IDataReader, ByVal objType As Type) As ArrayList

            Dim objFillCollection As New ArrayList()
            Dim objFillObject As Object
            '				int intProperty;

            ' get properties for type
            Dim objProperties As ArrayList = GetPropertyInfo(objType)

            ' get ordinal positions in datareader
            Dim arrOrdinals As Integer() = GetOrdinals(objProperties, dr)

            ' iterate datareader
            While dr.Read()
                ' fill business object
                objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals)
                ' add to collection
                objFillCollection.Add(objFillObject)
            End While

            ' close datareader
            If dr IsNot Nothing Then
                dr.Close()
            End If

            Return objFillCollection

        End Function

        Public Shared Function FillCollection(ByVal dr As IDataReader, ByVal objType As Type, ByVal objToFill As IList) As IList

            Dim objFillObject As Object
            '				int intProperty;

            ' get properties for type
            Dim objProperties As ArrayList = GetPropertyInfo(objType)

            ' get ordinal positions in datareader
            Dim arrOrdinals As Integer() = GetOrdinals(objProperties, dr)

            ' iterate datareader
            While dr.Read()
                ' fill business object
                objFillObject = CreateObject(objType, dr, objProperties, arrOrdinals)
                ' add to collection
                objToFill.Add(objFillObject)
            End While

            ' close datareader
            If dr IsNot Nothing Then
                dr.Close()
            End If

            Return objToFill

        End Function

        Public Shared Function InitializeObject(ByVal objObject As Object, ByVal objType As Type) As Object

            Dim intProperty As Integer

            ' get properties for type
            Dim objProperties As ArrayList = GetPropertyInfo(objType)
            For intProperty = 0 To objProperties.Count - 1

                ' initialize properties
                If DirectCast(objProperties(intProperty), PropertyInfo).CanWrite Then
                    DirectCast(objProperties(intProperty), PropertyInfo).SetValue(objObject, Null.SetNull(DirectCast(objProperties(intProperty), PropertyInfo)), Nothing)
                End If
            Next

            Return objObject

        End Function

        Public Shared Function Serialize(ByVal objObject As Object) As XmlDocument

            Dim objXmlSerializer As New XmlSerializer(objObject.[GetType]())

            Dim objStringBuilder As New StringBuilder()

            Dim objTextWriter As TextWriter = New StringWriter(objStringBuilder)

            objXmlSerializer.Serialize(objTextWriter, objObject)

            Dim objStringReader As New StringReader(objTextWriter.ToString())

            Dim objDataSet As New DataSet()

            objDataSet.ReadXml(objStringReader)

            Dim xmlSerializedObject As New XmlDocument()

            xmlSerializedObject.LoadXml(objDataSet.GetXml())

            Return xmlSerializedObject

        End Function

    End Class

End Namespace
