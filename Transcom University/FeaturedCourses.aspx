﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FeaturedCourses.aspx.cs" Inherits="FeaturedCourses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            var minIndex = 0,
            maxIndex,
            globalSelectedIndex,
            multiPage,
            NextDestination,
            PreviousDestination;

            $(document).ready(function () {
                multiPage = $find("<%= RadMultiPage1.ClientID %>");
                PreviousDestination = $find("<%= PreviousDestination.ClientID %>");
                NextDestination = $find("<%= NextDestination.ClientID %>");

                //Initialization of the global selected index of the RadMultiPage control
                globalSelectedIndex = multiPage.get_selectedIndex();
                maxIndex = document.getElementById("<%= hiddenMaxCount.ClientID %>").value;
            });

            function PrevClicked(sender, args) {
                globalSelectedIndex--;
                multiPage.set_selectedIndex(globalSelectedIndex);

                if (globalSelectedIndex == minIndex) {
                    sender.set_enabled(false);
                }

                if (!NextDestination.get_enabled()) {
                    NextDestination.set_enabled(true);
                }
            }

            function NextClicked(sender, args) {
                globalSelectedIndex++;
                multiPage.set_selectedIndex(globalSelectedIndex);

                if (globalSelectedIndex == maxIndex) {
                    sender.set_enabled(false);
                }

                if (!PreviousDestination.get_enabled()) {
                    PreviousDestination.set_enabled(true);
                }
            }
  
        </script>
    </telerik:RadCodeBlock>
    <div class="preview">
        <div class="exampleWrapper">
            <div class="buttonContainer">
                <telerik:RadButton ID="PreviousDestination" Skin="Default" CssClass="button buttonPrev"
                    ToolTip="Click to see previous destination" Width="35px" Height="300px" runat="server"
                    Enabled="false" ButtonType="LinkButton" AutoPostBack="false" OnClientClicked="PrevClicked">
                </telerik:RadButton>
            </div>
            <telerik:RadMultiPage ID="RadMultiPage1" CssClass="multipage" runat="server" SelectedIndex="0"
                ScrollBars="Auto">
            </telerik:RadMultiPage>
            <div class="buttonContainer">
                <telerik:RadButton ID="NextDestination" Skin="Default" CssClass="button buttonNext"
                    Width="35px" ToolTip="Click to see next destination" Height="300px" runat="server"
                    ButtonType="LinkButton" AutoPostBack="false" OnClientClicked="NextClicked">
                </telerik:RadButton>
            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hiddenMaxCount" Value="" runat="server" />
    </div>
</asp:Content>
