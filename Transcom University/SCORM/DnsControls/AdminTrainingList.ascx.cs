using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;

using DotNetSCORM.LearningAPI;
using Schema = DotNetSCORM.LearningAPI.Schema;


public partial class DNSControls_AdminTrainingList : DotNetSCORM.LearningAPI.DotNetSCORMControlBase
{

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get a ClientScriptManager reference from the Page class.
        ClientScriptManager cs = Page.ClientScript;

        // Load the javaScript support file
        if (!cs.IsClientScriptBlockRegistered("DotNetSCORMjsScript"))
        {
            cs.RegisterClientScriptInclude("DotNetSCORMjsScript", "dnscontrols.js");
        }

        // Insert the Variables to allow the js to find the child controls on this control
        if (!cs.IsClientScriptBlockRegistered("DotNetSCORMTrainingList"))
        {
            string controlScript;
            controlScript = "var dnsTrainingGridControlID = '" + TrainingGrid.ClientID + "';";
            controlScript = controlScript + "var dnsDeletePackagesLinkID = '" + DeletePackagesLink.ClientID + "';";
            cs.RegisterClientScriptBlock(this.GetType(), "DotNetSCORMTrainingList", "", true);
        }

        
        // set <currentUser> to information about the current user, and add the current user's list
        // of training to the TrainingGrid table; for best performance, both these requests are
        // done in  single call to the database
        LearningStoreJob job = LStore.CreateJob();
        this.RequestCurrentUserInfo(job);
        RequestMyTraining(job, null);
        ReadOnlyCollection<object> results = job.Execute();
        DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfoResults((DataTable)results[0]);
        GetMyTrainingResultsToHtml((DataTable)results[1], TrainingGrid);
        
        //TODO: Finsih updating GridVIew

        TrainingDataItemCtrl TrainingItems = new TrainingDataItemCtrl();
        List<TrainingDataItems> itemsArr = TrainingItems.List((DataTable)results[1]);
        
        AdminTrainingGrid.DataSource = itemsArr; // (DataTable)results[1];
        AdminTrainingGrid.DataBind();

        // if there is no training (i.e. if TrainingGrid contains only the header row), initially
        // hide TrainingGrid and show a message instructing the user to upload content
        if (TrainingGrid.Rows.Count == 1)
            TrainingPanel.Attributes.Add("style", "display: none");
        else
            NoTrainingMessage.Attributes.Add("style", "display: none");

        // since there is no initial selection, intially disable the "Delete Packages" link
        DeletePackagesLink.Attributes.Add("disabled", "true");


    }



}
