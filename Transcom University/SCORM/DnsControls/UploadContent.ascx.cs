using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web.UI;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.LearningComponents;
using Microsoft.LearningComponents.Storage;
using DotNetSCORM.LearningAPI;
using System.Data.SqlClient;
using Telerik.Web.UI;

using Schema = DotNetSCORM.LearningAPI.Schema;
//using ASP;

using ScormCourse;
using System.IO;
using System.Web.Hosting;

public partial class DNSControls_UploadContent : DotNetSCORMControlBase
{
    public Boolean UploadComplete = false;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    readonly ScormCourse.SCORM_UserControls_AddScormCourse _asc = new ScormCourse.SCORM_UserControls_AddScormCourse();

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    public static string CompletePath
    {
        get
        {
            var value = HttpContext.Current.Session["CompletePath"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["CompletePath"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    //public static IEnumerable<T> GetAllControlsOfType<T>(this Control parent) where T : Control
    //{
    //    var result = new List<T>();
    //    foreach (Control control in parent.Controls)
    //    {
    //        if (control is T)
    //        {
    //            result.Add((T)control);
    //        }
    //        if (control.HasControls())
    //        {
    //            result.AddRange(control.GetAllControlsOfType<T>());
    //        }
    //    }
    //    return result;
    //}

    protected void UploadPackageButton_OnClick(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid)
            {
                //add course form
                SCORM_UserControls_AddScormCourse asc1 = new SCORM_UserControls_AddScormCourse();
                RadMultiPage mpCourses = (RadMultiPage)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("mpCourses");

                Label AddButtonSenderID;
                RadComboBox cbSCOType;
                RadGrid gridPackage;

                if (mpCourses.SelectedIndex == 1)
                {
                    UserControl AddScormCourse = (UserControl)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse");
                    AddButtonSenderID = (Label)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
                    cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
                    gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
                }
                else
                {
                    RadGrid gridCourse = (RadGrid)((RadPageView)((RadPageView)UploadPackageButton.NamingContainer.NamingContainer.NamingContainer.NamingContainer.FindControl("pvCourse")).FindControl("pvCourseList")).FindControl("gridCourse");

                    //var allTextBoxesOnThePage = GetAllControlsOfType<GridView>();
                    //var allHyperLinksInControl = randomControl.GetAllControlsOfType<HyperLink>();

                    //SCORM_UserControls_AddScormCourse EditScormCourse = LogicHelper.FindControl<SCORM_UserControls_AddScormCourse>(gridCourse.Controls, "EditScormCourse");

                    ASP.scorm_usercontrols_addscormcourse_ascx EditScormCourse = LogicHelper.FindControl<ASP.scorm_usercontrols_addscormcourse_ascx>(gridCourse.Controls, "EditScormCourse");
                    //ASP.scorm_usercontrols_addscormcourse_ascx EditScormCourse = (ASP.scorm_usercontrols_addscormcourse_ascx)gridCourse.FindControl("EditScormCourse");
                    // ASP.scorm_usercontrols_addscormcourse_ascx EditScormCourse = LogicHelper.FindControl<ASP.scorm_usercontrols_addscormcourse_ascx>(gridCourse.Controls, "EditScormCourse");

                    AddButtonSenderID = (Label)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
                    cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
                    gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
                }

                _asc.CreatePackageDataTableColumns();

                foreach (GridDataItem row in gridPackage.Items)
                {
                    var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                    var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                    var ScoType = (string)row.GetDataKeyValue("ScoType");
                    var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
                    var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                    var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

                    DataRow dr = _asc.DtPackage1.NewRow();
                    dr["ScoPackageId"] = ScoPackageId;
                    dr["ScoTitle"] = ScoTitle;
                    dr["ScoType"] = ScoType;
                    dr["ScoTypeId"] = ScoTypeId;
                    dr["CourseTypeID"] = CourseTypeID;
                    dr["curriculumcourseID"] = curriculumcourseID;
                    _asc.DtPackage1.Rows.Add(dr);
                }

                //--MATT 06132015 - Added Learning Guide as "Other Resource" upload and in clause for better code readability
                if (cbSCOType.Text.In("Trainer Guide", "Additional Resources (Trainer)", "Additional Resources (Learner)", "Trainer Presentation", "Learning Guide"))
                {
                    //Raymark - Copy original other resources to Asia other resources
                    string completePath;
                    string completeAsiaPath;
                    string uniqName = null;

                    //Upload all the files to a permanent location
                    foreach (UploadedFile file in RadAsyncUpload1.UploadedFiles)
                    //if (UploadedPackageFile.HasFile)
                    {
                        string FolderPath = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"];
                        string AsiaFolderPath = string.Format(@"\\10.63.8.111\wwwroot\TranscomUniversityAP\SCORM\OtherResources\"); //Globe DC
                        //string AsiaFolderPath = string.Format(@"\\10.31.70.36\d$\TranscomUniversityAP\SCORM\OtherResources\"); //Q9VMDEVAPP06
                        //string AsiaFolderPath = string.Format(@"\\10.29.8.52\E$\Inetpub\University\"); //SUSL3PWEB01

                        //string targetFolder = Server.MapPath("/Scorm/OtherResources/");
                        //completePath = targetFolder + DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();

                        //Updload other resources to default path
                        uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
                        completePath = Server.MapPath(FolderPath + "\\" + uniqName);

                        file.SaveAs(completePath);

                        //Upload other resources to asia path
                        uniqName = DateTime.Now.ToString("yyyyMMddHHmmss") + file.GetName();
                        completeAsiaPath = string.Format(AsiaFolderPath + uniqName);

                        file.SaveAs(completeAsiaPath);
                    }
                    //=======================================================================
                    DataRow drPackage = _asc.DtPackage1.NewRow();
                    drPackage["ScoPackageId"] = 0;
                    drPackage["ScoTitle"] = uniqName; //RadAsyncUpload1.UploadedFiles[0].FileName;
                    //RadAsyncUpload1.UploadedFiles[0].FileName.Remove(RadAsyncUpload1.UploadedFiles[0].FileName.Length - 4);//remove extension
                    drPackage["ScoType"] = cbSCOType.SelectedItem.Text;
                    drPackage["ScoTypeId"] = cbSCOType.SelectedValue;
                    drPackage["CourseTypeID"] = 1; //1 - non scorm, 2- scorm
                    drPackage["curriculumcourseID"] = AddButtonSenderID.Text; //id of grid

                    _asc.DtPackage1.Rows.Add(drPackage);

                    gridPackage.DataSource = _asc.DtPackage1;
                    gridPackage.DataBind();
                }
                else
                //if (cbSCOType.Text != "Trainer Guide" && cbSCOType.Text != "Additional Resources (Trainer)" &&
                //    cbSCOType.Text != "Additional Resources (Learner)" && cbSCOType.Text != "Trainer Presentation") //not trainer resources
                {
                    DotNetSCORM.LearningAPI.LStoreUserInfo currentUser = GetCurrentUserInfo();
                    PackageItemIdentifier packageId;
                    //ValidationResults importLog;
                    //RadAsyncUpload1.FileFilters.

                    using (
                         PackageReader packageReader = PackageReader.Create(RadAsyncUpload1.UploadedFiles[0].InputStream)
                        //using (PackageReader packageReader = PackageReader.Create(UploadedPackageFile.FileContent)
                         )
                    {
                        AddPackageResult result = PStore.AddPackage(packageReader, new PackageEnforcement(false, false, false));
                        packageId = result.PackageId;
                        //importLog = result.Log;

                        //=======================================================================

                        //Raymark - Copy original package to Asia package
                        string fileToCopy = Server.MapPath(string.Format("~/Scorm/LearningContent/Package{0}/", packageId.GetKey()));
                        string directoryPath = string.Format(@"\\10.63.8.111\wwwroot\TranscomUniversityAP\SCORM\LearningContent\Package{0}\", packageId.GetKey()); //Globe DC
                        //string directoryPath = string.Format(@"\\10.31.70.36\d$\TranscomUniversityAP\SCORM\LearningContent\Package{0}\", packageId.GetKey()); //Q9VMDEVAPP06
                        //string directoryPath = string.Format(@"\\10.29.8.52\E$\Inetpub\University\SCORM\LearningContent\Package{0}\", packageId.GetKey()); //SUSL3PWEB01

                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        if (Directory.Exists(directoryPath))
                        {
                            //Copy folders and sub-folders
                            foreach (string subFolder in Directory.GetDirectories(fileToCopy, "*", SearchOption.AllDirectories))
                            {
                                Directory.CreateDirectory(subFolder.Replace(fileToCopy, directoryPath));
                            }

                            //Deep copy
                            foreach (string subFolder in Directory.GetDirectories(fileToCopy, "*", SearchOption.AllDirectories))
                            {
                                foreach (string filesInSubFolder1 in Directory.GetFiles(subFolder))
                                {
                                    string FileDest1 = subFolder.Replace(fileToCopy, directoryPath) + "\\" + Path.GetFileName(filesInSubFolder1);
                                    File.Copy(filesInSubFolder1, FileDest1, true);
                                }
                            }

                            //Copy remaining files
                            foreach (string files in Directory.GetFiles(fileToCopy))
                            {
                                File.Copy(files, Path.Combine(directoryPath, Path.GetFileName(files)), true);
                            }
                        }
                    }

                    LearningStoreJob job = LStore.CreateJob();
                    Dictionary<string, object> properties = new Dictionary<string, object>();
                    properties[Schema.PackageItem.Owner] = currentUser.Id;
                    properties[Schema.PackageItem.FileName] = RadAsyncUpload1.UploadedFiles[0].FileName;
                    //properties[Schema.PackageItem.FileName] = UploadedPackageFile.FileName;
                    properties[Schema.PackageItem.UploadDateTime] = DateTime.Now;

                    job.UpdateItem(packageId, properties);
                    job.Execute();
                    job = LStore.CreateJob();
                    RequestMyTraining(job, packageId);
                    job.Execute<DataTable>();

                    //=============================================================

                    //get package title
                    string PackageTitle;
                    Dbconn("Intranet");
                    _ocmd = new SqlCommand("pr_Scorm_Cor_GetPackageDetails", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                    _ocmd.Parameters.AddWithValue("@ScoPackageId", packageId.ToString().Replace("ItemType:PackageItem, Key:", ""));
                    _ocmd.Parameters.Add("@Title", SqlDbType.VarChar, -1);
                    _ocmd.Parameters["@Title"].Direction = ParameterDirection.Output;
                    _ocmd.ExecuteReader();
                    _oconn.Close();
                    PackageTitle = _ocmd.Parameters["@Title"].Value.ToString();

                    //=======================================================================

                    DataRow drPackage = _asc.DtPackage1.NewRow();
                    drPackage["ScoPackageId"] = packageId.ToString().Replace("ItemType:PackageItem, Key:", "");
                    drPackage["ScoTitle"] = PackageTitle;
                    //RadAsyncUpload1.UploadedFiles[0].FileName.Remove(RadAsyncUpload1.UploadedFiles[0].FileName.Length - 4);//remove extension
                    drPackage["ScoType"] = cbSCOType.SelectedItem.Text;
                    drPackage["ScoTypeId"] = cbSCOType.SelectedValue;
                    drPackage["CourseTypeID"] = 2; //1 - non scorm, 2- scorm
                    drPackage["curriculumcourseID"] = AddButtonSenderID.Text; //id of grid

                    _asc.DtPackage1.Rows.Add(drPackage);
                    gridPackage.DataSource = _asc.DtPackage1;
                    gridPackage.DataBind();
                    gridPackage.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                             string.Format("alert('Error Uploading SCORM Package: {0}');", ex.Message.Replace("'", "")), true);
        }
    }

    protected void UploadDone_Click(object sender, EventArgs e)
    {
        UploadComplete = true;
    }
}
