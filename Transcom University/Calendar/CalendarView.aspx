﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CalendarView.aspx.cs" Inherits="Calendar.CalendarCalendarView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(this).mouseleave(function () {
                $("div[id^='divenroll']").hide();
                $("div[id^='divdesc']").hide();
            });

            $('.calendar-date').mouseover(function () {
                var off = $(this).offset();
                var oo = $(this).find('.details').outerHeight();
                $(this).find('.details').css({
                    left: (off.left - 120) + 'px',
                    top: (off.top - oo) + 'px'
                });
                $(this).find('.details').show();
                $(this).find('.next-prev').show();
            });

            $('.calendar-date').mouseout(function () {
                $(this).find('.details').hide();
                $(this).find('.next-prev').hide();
            });

            $('.btnenrollnow').click(function () {
                var modal = $(this).attr('modal');
                $('#divenroll' + modal).show();
            });

            $('.btndescription').click(function () {
                var modal = $(this).attr('modal');
                $('#divdesc' + modal).show();
            });

            $('.gotonext').click(function () {
                var cc = $(this).attr('showobject');
                var hh = $(this).attr('hideme');
                $('#' + hh).hide();
                $('#' + cc).fadeIn();
            });

            $('.gotoprev').click(function () {
                var cc = $(this).attr('showobject');
                var hh = $(this).attr('hideme');
                $('#' + hh).hide();
                $('#' + cc).fadeIn();
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <div style="float: left; width: 300px; position: absolute">
            <asp:LinkButton runat="server" Text="< Calendar Menu" PostBackUrl="Index.aspx" CssClass="linkBtn" />
        </div>
        <table width="100%" style="padding-bottom: 50px">
            <tr>
                <td align="center">
                    <h2 id="elementh2" runat="server" />
                    <hr />
                    <div id="calendar" runat="server" class="calendar" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
