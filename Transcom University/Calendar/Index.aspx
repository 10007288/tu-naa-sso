﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Index.aspx.cs" Inherits="Calendar.CalendarIndex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#professional').change(function () {
                if ($(this).find('input[type=radio]').is(':checked')) {
                    $('#clientlist').hide();
                    $('#sitelist').hide();
                    $('#roomlist').hide();
                }
            });
            
            $('#client').change(function () {
                if ($(this).find('input[type=radio]').is(':checked')) {
                    $('#clientlist').show();
                    $('#sitelist').hide();
                    $('#roomlist').hide();
                }
            });

            $('#site').change(function () {
                if ($(this).find('input[type=radio]').is(':checked')) {
                    $('#clientlist').hide();
                    $('#sitelist').show();
                    $('#roomlist').hide();
                }
            });

            $('#room').change(function () {
                if ($(this).find('input[type=radio]').is(':checked')) {
                    $('#clientlist').hide();
                    $('#sitelist').hide();
                    $('#roomlist').show();
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <h2>
            Select Calendar</h2>
        <hr />
        <div style="padding: 0 0 20px 20px">
            <div id="professional">
                <asp:RadioButton ID="rbProfessional" Text="Professional Development" runat="server"
                    GroupName="ok" Checked="true" />
            </div>
            <div id="client">
                <asp:RadioButton ID="rbClient" Text="Program Specific" runat="server" GroupName="ok" />
            </div>
            <div id="site">
                <asp:RadioButton ID="rbSite" Text="Site Specific" runat="server" GroupName="ok" />
            </div>
            <div id="room">
                <asp:RadioButton ID="rbRoom" Text="Room Specific" runat="server" GroupName="ok" />
            </div>
            <div id="clientlist" style="display: none;">
                <div class="display-label">
                    <div>
                        <label>
                            Select...</label></div>
                    <asp:ListBox runat="server" ID="lstClients" SelectionMode="Single" Width="320" Height="170" />
                </div>
            </div>
            <div id="sitelist" style="display: none;">
                <div class="display-label">
                    <div>
                        <label>
                            Select...</label></div>
                    <asp:ListBox runat="server" ID="lstSites" SelectionMode="Single" Width="320" Height="170" />
                </div>
            </div>
            <div id="roomlist" style="display: none;">
                <div class="display-label">
                    <div>
                        <label>
                            Select...</label></div>
                    <asp:ListBox runat="server" ID="lstRooms" SelectionMode="Single" Width="320" Height="170" />
                </div>
            </div>
            <p>
                <telerik:RadButton ID="btnView" Text="View" runat="server" OnClick="btnView_Click"
                    ValidationGroup="view" />
            </p>
        </div>
    </div>
</asp:Content>
