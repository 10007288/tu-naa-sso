﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class dialogs_Explorer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Object.Equals(Session["SkinChooserSkin"], null)) FileExplorer1.Skin = (string)Session["SkinChooserSkin"];
        FileExplorer1.Configuration.SearchPatterns = new string[] { "*.html" };
 
    }
}