﻿#region .DLLs

using System;
using System.Web.UI;
using Telerik.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using NuComm.Security.Encryption;

#endregion

public partial class MyClass : Page
{
    #region VARIABLES

    private MasterPage _master;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    private DbHelper _db;

    //Functions
    public DataTable ConvertToDataTable<T>(IList<T> data)
    {
        var properties = TypeDescriptor.GetProperties(typeof(T));

        var table = new DataTable();

        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

        foreach (T item in data)
        {
            var row = table.NewRow();

            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;

            table.Rows.Add(row);
        }

        return table;
    }

    //DataSets
    private DataSet GetTests(int classId, string testType)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetTests(classId, testType);
        return ds;
    }

    private DataSet GetCert(int classId)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetCert(classId);
        return ds;
    }

    //DataTables
    private DataTable DtCert
    {
        get
        {
            return ViewState["DtCert"] as DataTable;
        }
        set
        {
            ViewState["DtCert"] = value;
        }
    }

    private DataTable DtCertTemp
    {
        get
        {
            return ViewState["DtCertTemp"] as DataTable;
        }
        set
        {
            ViewState["DtCertTemp"] = value;
        }
    }

    private DataTable Dt
    {
        get
        {
            return ViewState["dt"] as DataTable;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }

    private DataTable DtTemp
    {
        get
        {
            return ViewState["DtTemp"] as DataTable;
        }
        set
        {
            ViewState["DtTemp"] = value;
        }
    }

    private DataTable DtParticipant
    {
        get
        {
            return ViewState["dtParticipant"] as DataTable;
        }
        set
        {
            ViewState["dtParticipant"] = value;
        }
    }

    private DataTable DtToAssignTemp
    {
        get
        {
            return ViewState["DtToAssignTemp"] as DataTable;
        }
        set
        {
            ViewState["DtToAssignTemp"] = value;
        }

    }

    private DataTable DtToAssign
    {
        get
        {
            return ViewState["dtToAssign"] as DataTable;
        }
        set
        {
            ViewState["dtToAssign"] = value;
        }
    }

    private DataTable DtTUAssign
    {
        get
        {
            return ViewState["DtTUAssign"] as DataTable;
        }
        set
        {
            ViewState["DtTUAssign"] = value;
        }
    }

    //ViewStates
    private string CurrentWindow
    {
        get
        {
            return ViewState["CurrentWindow"] as string;
        }
        set
        {
            ViewState["CurrentWindow"] = value;
        }
    }

    //Sessions
    public static int ClassId
    {
        get
        {
            var value = HttpContext.Current.Session["ClassIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["ClassIdSession"] = value;
        }
    }

    private string EditClassId
    {
        get
        {
            return ViewState["EditClassID"] as string;
        }
        set
        {
            ViewState["EditClassID"] = value;
        }

    }

    public static int CtrPrerequisite
    {
        get
        {
            var value = HttpContext.Current.Session["ctrPrerequisite"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ctrPrerequisite"] = value; }
    }


    #endregion


    #region "Export to Excel"

    protected void btnExportViewParticipants_Click(object sender, EventArgs e)
    {
        gridViewParticipants.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridViewParticipants.ExportSettings.IgnorePaging = true;
        gridViewParticipants.ExportSettings.ExportOnlyData = true;
        gridViewParticipants.ExportSettings.OpenInNewWindow = true;
        gridViewParticipants.ExportSettings.FileName = "Participants_" + DateTime.Now;
        gridViewParticipants.MasterTableView.ExportToExcel();
    }

    protected void btnExportMainViewParticipant_Click(object sender, EventArgs e)
    {
        gridParticipants.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridParticipants.ExportSettings.IgnorePaging = true;
        gridParticipants.ExportSettings.ExportOnlyData = true;
        gridParticipants.ExportSettings.OpenInNewWindow = true;
        gridParticipants.ExportSettings.FileName = "Participants_" + DateTime.Now;
        gridParticipants.MasterTableView.ExportToExcel();
    }

    protected void btnExportClass_Click(object sender, EventArgs e)
    {
        //gridClass.MasterTableView.GetColumn("EditCommandColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("ParticipantColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("Non-TUTestsColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("TUTestsColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("TUCourseColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("CertificateColumn").Visible = false;
        //gridClass.MasterTableView.GetColumn("ParticipantColumn").Visible = false;

        gridClass.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridClass.ExportSettings.IgnorePaging = true;
        gridClass.ExportSettings.ExportOnlyData = true;
        gridClass.ExportSettings.OpenInNewWindow = true;
        gridClass.ExportSettings.FileName = "Class_" + DateTime.Now;
        gridClass.MasterTableView.ExportToExcel();
    }

    protected void btnExportNonTUTest_Click(object sender, EventArgs e)
    {

        grdEditNonTUTests.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdEditNonTUTests.ExportSettings.IgnorePaging = true;
        grdEditNonTUTests.ExportSettings.ExportOnlyData = true;
        grdEditNonTUTests.ExportSettings.OpenInNewWindow = true;
        grdEditNonTUTests.ExportSettings.FileName = "Non-TU Test_" + DateTime.Now;
        grdEditNonTUTests.MasterTableView.ExportToExcel();
    }

    protected void btnExportTUTest_Click(object sender, EventArgs e)
    {

        rgEditAssignedTests.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        rgEditAssignedTests.ExportSettings.IgnorePaging = true;
        rgEditAssignedTests.ExportSettings.ExportOnlyData = true;
        rgEditAssignedTests.ExportSettings.OpenInNewWindow = true;
        rgEditAssignedTests.ExportSettings.FileName = "TU Test_" + DateTime.Now;
        rgEditAssignedTests.MasterTableView.ExportToExcel();
    }

    protected void btnExportCourse_Click(object sender, EventArgs e)
    {

        rgEditAssignedCourses.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        rgEditAssignedCourses.ExportSettings.IgnorePaging = true;
        rgEditAssignedCourses.ExportSettings.ExportOnlyData = true;
        rgEditAssignedCourses.ExportSettings.OpenInNewWindow = true;
        rgEditAssignedCourses.ExportSettings.FileName = "Course_" + DateTime.Now;
        rgEditAssignedCourses.MasterTableView.ExportToExcel();
    }


    protected void btnExportCertification_Click(object sender, EventArgs e)
    {

        grdEditCert.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        grdEditCert.ExportSettings.IgnorePaging = true;
        grdEditCert.ExportSettings.ExportOnlyData = true;
        grdEditCert.ExportSettings.OpenInNewWindow = true;
        grdEditCert.ExportSettings.FileName = "Certification_" + DateTime.Now;
        grdEditCert.MasterTableView.ExportToExcel();
    }


    protected void btnExportAttendance_Click(object sender, EventArgs e)
    {

        gridAttendance.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridAttendance.ExportSettings.IgnorePaging = true;
        gridAttendance.ExportSettings.ExportOnlyData = true;
        gridAttendance.ExportSettings.OpenInNewWindow = true;
        gridAttendance.ExportSettings.FileName = "Attendance_" + DateTime.Now;
        gridAttendance.MasterTableView.ExportToExcel();
    }


    protected void btnExportTestScore_Click(object sender, EventArgs e)
    {

        gridTestParticipants.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridTestParticipants.ExportSettings.IgnorePaging = true;
        gridTestParticipants.ExportSettings.ExportOnlyData = true;
        gridTestParticipants.ExportSettings.OpenInNewWindow = true;
        gridTestParticipants.ExportSettings.FileName = "Test Score_" + DateTime.Now;
        gridTestParticipants.MasterTableView.ExportToExcel();
    }

    #endregion

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "AddAssignCourse")
        {
            AssignCourseAdd();
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Page.IsPostBack)
        {
            if (!Roles.IsUserInRole("Admin") && !Roles.IsUserInRole("Non-APAC Admin") && !Roles.IsUserInRole("Trainer"))
            {
                Response.Redirect("NoAccess.aspx");
            }

            gridClass.MasterTableView.FilterExpression = "([classStatus] = 'Active' )";
            CtrPrerequisite = 0;
            dsClassID.SelectParameters["TrainerCim"].DefaultValue = Roles.IsUserInRole("Admin") ? "0" : Context.User.Identity.Name;
            dpAttendanceForm.SelectedDate = DateTime.Now.Date;
            dtJournalDate.SelectedDate = DateTime.Now.Date;

        }
    }

    protected void MainTab_Click(object sender, RadTabStripEventArgs e)
    {
        if (tabStrip1.SelectedIndex == 0)
        {
            tsClasss.SelectedIndex = 0;
            mpClass.SelectedIndex = 0;
        }
        else if (tabStrip1.SelectedIndex == 1)
        {
            cmbClassID.DataBind();
            tsParticipant.SelectedIndex = 0;
            mpParticipant.SelectedIndex = 0;
        }
        else if (tabStrip1.SelectedIndex == 2)
        {
            cbAttendanceClassID.DataBind();
        }
        else if (tabStrip1.SelectedIndex == 3)
        {
            cbLoadClass.DataBind();
        }
        else if (tabStrip1.SelectedIndex == 4)
        {
            cbClassIDJournal.DataBind();
        }
    }

    protected void tsClass_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (tsClasss.SelectedIndex == 1)
        {
            divClassExport.Visible = false;

            if (cbCategoryAssign.Items.Count == 0)
            {
                RefreshComboBoxesOnCategorySubcategory();
                RefreshComboBoxesTUTestCategory();
            }

            if (Dt != null)
            {
                Dt.Clear();
            }

            foreach (GridDataItem row in rgAssignedTests.Items)
            {
                var lblTestName = (Label)row.FindControl("lblTestName");
                var dr = Dt.NewRow();
                dr["TestName"] = lblTestName.Text;
                Dt.Rows.Add(dr);
            }

            rgAssignedTests.Rebind();

            if (DtToAssign != null)
            {
                DtToAssign.Clear();
            }

            if (DtTUAssign != null)
            {
                DtTUAssign.Clear();
            }

            foreach (GridDataItem row in rgAssignedCourses.Items)
            {
                var courseAssignmentId = (int)row.GetDataKeyValue("CourseAssignmentID");
                var courseId = (Int64)row.GetDataKeyValue("CourseID");
                var title = (string)row.GetDataKeyValue("Title");
                var lblDueDate = (Label)row.FindControl("lblDueDate");

                DataRow dr = DtToAssign.NewRow();

                dr["CourseAssignmentID"] = courseAssignmentId;
                dr["CourseID"] = courseId;
                dr["DueDate"] = lblDueDate.Text;
                dr["Title"] = title;

                DtToAssign.Rows.Add(dr);
            }


            if (DtToAssign == null)
            {
                DtToAssign = ConvertToDataTable(DataHelper.GetAssignedCourses(2, 0));
            }

            rgAssignedCourses.Rebind();


            foreach (GridDataItem row in rgAddAssignedTUTest.Items)
            {
                var courseAssignmentId = (int)row.GetDataKeyValue("CourseAssignmentID");
                var courseId = (Int64)row.GetDataKeyValue("CourseID");
                var title = (string)row.GetDataKeyValue("Title");
                var lblDueDate = (Label)row.FindControl("lblDueDate");

                DataRow dr = DtTUAssign.NewRow();

                dr["CourseAssignmentID"] = courseAssignmentId;
                dr["CourseID"] = courseId;
                dr["DueDate"] = lblDueDate.Text;
                dr["Title"] = title;

                DtTUAssign.Rows.Add(dr);
            }

            if (DtTUAssign == null)
            {
                DtTUAssign = ConvertToDataTable(DataHelper.GetAssignedCourses(4, 0));
            }

            rgAddAssignedTUTest.Rebind();
        }
        else
        {
            divClassExport.Visible = true;
        }
    }

    #region .CATEGORY SUBCATEGORY COMBOs

    protected bool ValidateControls()
    {
        var retVal = false;

        if (cbCategoryAssign.Text != string.Empty && cbCategoryAssign.Text != string.Empty && ddtSubcategoryAssign.SelectedValue != string.Empty)
        {
            retVal = true;
            btnAssign.Visible = true;
        }
        else
        {
            btnAssign.Visible = false;
        }

        return retVal;
    }

    protected bool ValidateEditControls()
    {
        var retVal = false;

        if (cbEditCategoryAssign.Text != string.Empty && cbEditCategoryAssign.Text != string.Empty && ddtEditSubcategoryAssign.SelectedValue != string.Empty)
        {
            retVal = true;
            btnEditAssign.Visible = true;
        }
        else
        {
            btnEditAssign.Visible = false;
        }

        return retVal;
    }

    protected void RefreshComboBoxesOnCategorySubcategory()
    {
        PopulateComboBoxCategory(cbCategoryAssign, "Admin", false);
    }

    protected void RefreshComboBoxesTUTestCategory()
    {
        PopulateComboBoxCategory(cbAddTUTestCategory, "Admin", true);
    }

    protected void RefreshComboBoxesOnCategorySubcategoryEdit()
    {
        PopulateComboBoxCategory(cbEditCategoryAssign, "Admin", false);
    }

    protected void PopulateComboBoxCategory(RadComboBox combo, string accessMode, bool isTUTest)
    {
        combo.Text = "";

        if (isTUTest == false)
        {
            var ds = DataHelper.GetCategories(accessMode);
            combo.DataSource = ds;
        }
        else
        {
            combo.DataSource = GetTUTestCategories();
        }
        combo.DataBind();

    }

    protected void CbCategoryAssignSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategoryAssign, ddtSubcategoryAssign, false);
        ValidateControls();
        ddtSubcategoryAssign.Enabled = true;
    }

    protected void CbEditCategoryAssignSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbEditCategoryAssign, ddtEditSubcategoryAssign, false);
        ValidateEditControls();
        ddtEditSubcategoryAssign.Enabled = true;
    }




    #endregion

    #region CLASS

    private void ClearFields()
    {
        ddTraineeSite.ClearSelection();
        cbClassName.ClearSelection();
        cbClassName.Text = string.Empty;
        ddTrainingType.ClearSelection();
        ddlLocType.ClearSelection();
        ddTrainingHours.ClearSelection();
        txtRTPTandim.Text = string.Empty;
        txtTrainingValNo.Text = string.Empty;
        txtLoginReqNo.Text = string.Empty;
        dpStartDate.Clear();
        RadStartDate.Clear();
        dpOrientationDate.Clear();
        RadClient.ClearSelection();
        dpEndDate.Clear();
        txtWaveNumber.Text = string.Empty;
        txtOtherAttrition.Text = string.Empty;
        txtDay1NoShow.Text = string.Empty;
        txtStartingHeadCount.Text = string.Empty;

        cbAddTUTestCategory.ClearSelection();
        cbCategoryAssign.ClearSelection();

        rdtAddTUTestCategory.Entries.Clear();
        rdtAddTUTestCategory.Enabled = false;

        ddtSubcategoryAssign.Entries.Clear();
        ddtSubcategoryAssign.Enabled = false;

        Dt.Clear();
        rgAssignedTests.DataSource = Dt;
        rgAssignedTests.DataBind();

        DtToAssign.Clear();
        rgAssignedCourses.DataSource = DtToAssign;
        rgAssignedCourses.DataBind();

        DtTUAssign.Clear();
        rgAddAssignedTUTest.DataSource = DtTUAssign;
        rgAddAssignedTUTest.DataBind();

        btnAssign.Visible = false;
        btnSelectTUTest.Visible = false;

        lbUnassignedCourses.Items.Clear();
        lbAssignedCourses.Items.Clear();


    }

    protected void GridClassRefreshCombos()
    {

        // SqlDataSource2.SelectCommand = SqlDataSource2.SelectCommand + " WHERE " + RadGrid1.MasterTableView.FilterExpression.ToString();

        // SqlDataSource3.SelectCommand = SqlDataSource3.SelectCommand + " WHERE " + RadGrid1.MasterTableView.FilterExpression.ToString();

        // SqlDataSource4.SelectCommand = SqlDataSource4.SelectCommand + " WHERE " + RadGrid1.MasterTableView.FilterExpression.ToString();

        // SqlDataSource6.SelectCommand = SqlDataSource6.SelectCommand + " WHERE " + RadGrid1.MasterTableView.FilterExpression.ToString();

        gridClass.MasterTableView.Rebind();
    }

    private DataSet GetClass()
    {
        _db = new DbHelper("Intranet");

        var ds = _db.GetClass(Context.User.Identity.Name);
        if (Roles.IsUserInRole("Admin"))
        {
            ds = _db.GetClass("0");
        }

        return ds;
    }

    private DataSet GetTUTestCategories()
    {
        _db = new DbHelper("NuskillCheck");

        var ds = _db.GetTuTestCategories();

        return ds;
    }

    private DataSet GetTUTestSubCategories(int? campaignid)
    {
        _db = new DbHelper("NuskillCheck");

        var ds = _db.GetTuTestSubCategories(campaignid);

        return ds;
    }

    #region windowViewTUTest

    protected void RefreshComboBoxesOnCategorySubcategoryTest()
    {
        PopulateComboBoxCategory(cbTestCategoryAssign, "Admin", true);
    }

    protected void btnViewTUTest_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;
        var eventId = item.GetDataKeyValue("ClassID");

        EditClassId = eventId.ToString();

        RefreshComboBoxesOnCategorySubcategoryTest();

        ddtTestSubcategoryAssign.Enabled = false;
        ddtTestSubcategoryAssign.DataSource = null;
        ddtTestSubcategoryAssign.DataBind();


        DtToAssign = ConvertToDataTable(DataHelper.GetAssignedCourses(4, Convert.ToInt32(EditClassId)));

        DtToAssignTemp = DtToAssign;

        if (DtToAssign != null)
        {
            rgEditAssignedTests.DataSource = DtToAssign;//DataHelper.getAssignedCourses(2, 0);
        }
        else
        {
            rgEditAssignedTests.DataSource = DataHelper.GetAssignedCourses(4, Convert.ToInt32(EditClassId));
        }

        rgEditAssignedTests.DataBind();
        btnSelectTest.Visible = false;

        lblTUTestClassID.Text = "Class ID: " + EditClassId;
    }

    protected bool validateTestControls()
    {
        var retVal = false;

        if (cbTestCategoryAssign.Text != string.Empty && cbTestCategoryAssign.Text != string.Empty && ddtTestSubcategoryAssign.SelectedValue != string.Empty)
        {
            retVal = true;
            btnSelectTest.Visible = true;
        }
        else
        {
            btnSelectTest.Visible = false;
        }

        return retVal;
    }

    protected bool validateTUTestControls()
    {
        var retVal = false;

        if (cbAddTUTestCategory.Text != string.Empty && rdtAddTUTestCategory.SelectedValue != string.Empty)
        {
            retVal = true;
            btnSelectTUTest.Visible = true;
        }
        else
        {
            btnSelectTUTest.Visible = false;
        }

        return retVal;
    }

    protected void setTestEditAssignToSource()
    {
        lbUnassignedCourses.DataSource = DataHelper.GetUnassignedTests(4, Convert.ToInt32(EditClassId), Convert.ToInt32(cbTestCategoryAssign.SelectedValue), Convert.ToInt32(ddtTestSubcategoryAssign.SelectedValue), "");

        btnSelectTest.Visible = true;
        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();

    }

    protected void setTUTestEditAssignToSource()
    {
        string CourseIDs = "";

        foreach (GridDataItem row in rgAddAssignedTUTest.Items)
        {
            var courseId = (Int64)row.GetDataKeyValue("CourseID");
            CourseIDs += "," + courseId.ToString();
        }
        if (CourseIDs != string.Empty)
        {
            CourseIDs = CourseIDs.Remove(0, 1);
        }

        lbUnassignedCourses.DataSource = DataHelper.GetUnassignedTests(4, 0, Convert.ToInt32(cbAddTUTestCategory.SelectedValue), Convert.ToInt32(rdtAddTUTestCategory.SelectedValue), CourseIDs);

        btnSelectTUTest.Visible = true;
        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();
    }

    protected void ddtTestSubcategoryAssign_EntryRemoved(object sender, DropDownTreeEntryEventArgs e)
    {
        btnSelectTest.Visible = false;
    }

    protected void rdtAddTUTestCategory_EntryRemoved(object sender, DropDownTreeEntryEventArgs e)
    {
        btnSelectTUTest.Visible = false;
    }

    protected void cbTestCategoryAssignSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbTestCategoryAssign, ddtTestSubcategoryAssign, true);
        validateTestControls();
        ddtTestSubcategoryAssign.Enabled = true;
    }

    protected void cbAddTUTestCategoryAssignSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbAddTUTestCategory, rdtAddTUTestCategory, true);
        validateTUTestControls();
        rdtAddTUTestCategory.Enabled = true;
    }

    protected void ddtTestSubcategoryAssign_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (validateTestControls())
        {
            setTestEditAssignToSource();
        }
    }

    protected void rdtAddTUTestCategory_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (validateTUTestControls())
        {
            setTUTestEditAssignToSource();
        }
    }

    protected void btnSelectTest_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CurrentWindow = "editTest";

            btnAssignCourseAdd.Visible = false;
            btnAssignTestAdd.Visible = true;
            lbAssignedCourses.Items.Clear();
            AssignCourseWindow.Title = "Select TU Test/s";

            lbUnassignedCourses.EmptyMessage = "No Unassigned TU Test";
            lbAssignedCourses.EmptyMessage = "No TU Test Selected";



            //var tbllbAssignedCourses = (System.Web.UI.HtmlControls.HtmlTable)item.FindControl("tbllbAssignedCourses");
            var tbllbAssignedCourses1 = (System.Web.UI.HtmlControls.HtmlTable)lbAssignedCourses.Controls[0].FindControl("tbllbAssignedCourses1");
            var td1 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses1.FindControl("td1");
            td1.Width = "100%";

            var lblCourseName = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblCourseName");
            var lblPrerequisite = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblPrerequisite");
            var lblDueDate = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblDueDate");

            lblDueDate.Visible = false;
            lblPrerequisite.Visible = false;
            lblCourseName.Text = "TU Test Name";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "OpenAssignForm();", true);

        }
    }

    protected void btnSelectTUTest_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            btnAssignCourseAdd.Visible = false;
            btnAssignTestAdd.Visible = true;
            lbAssignedCourses.Items.Clear();
            CurrentWindow = "addTUTest";
            AssignCourseWindow.Title = "Select TU Test/s";
            lbUnassignedCourses.EmptyMessage = "No Unassigned TU Test";
            lbAssignedCourses.EmptyMessage = "No TU Test Selected";
            var lblCourseName = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblCourseName");
            var lblPrerequisite = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblPrerequisite");
            var lblDueDate = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblDueDate");

            var tbllbAssignedCourses1 = (System.Web.UI.HtmlControls.HtmlTable)lbAssignedCourses.Controls[0].FindControl("tbllbAssignedCourses1");
            var td1 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses1.FindControl("td1");
            td1.Width = "100%";

            lblDueDate.Visible = false;
            lblPrerequisite.Visible = false;
            lblCourseName.Text = "TU Test Name";
            setTUTestEditAssignToSource();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "OpenAssignForm();", true);
        }
    }

    protected void rgEditAssignedTests_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var CourseID = (Int64)deletedItem.GetDataKeyValue("CourseID");

                var rows = DtToAssign.Select("CourseID = '" + CourseID + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch
                {
                }
            }
        }

        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var CourseID = (Int64)editedItem.GetDataKeyValue("CourseID");
                var dpTestDueDateEdit = (RadDatePicker)editedItem.FindControl("dpTestDueDateEdit");

                DataRow[] rows = DtToAssign.Select("CourseID = '" + CourseID + "'");
                rows[0]["DueDate"] = dpTestDueDateEdit.SelectedDate;

            }
        }


    }

    protected void rgEditAssignedTests_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

        if (DtToAssign != null)
        {
            rgEditAssignedTests.DataSource = DtToAssign;//DataHelper.getAssignedCourses(2, 0);
        }
        else
        {
            rgEditAssignedTests.DataSource = DataHelper.GetAssignedCourses(4, Convert.ToInt32(EditClassId));
        }
    }

    protected void rgAddAssignedTUTest_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var CourseID = (Int64)deletedItem.GetDataKeyValue("CourseID");

                var rows = DtTUAssign.Select("CourseID = '" + CourseID + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch
                {
                }
            }
        }

        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var CourseID = (Int64)editedItem.GetDataKeyValue("CourseID");
                var dpTestDueDateEdit = (RadDatePicker)editedItem.FindControl("dpDueDateEdit");

                DataRow[] rows = DtTUAssign.Select("CourseID = '" + CourseID + "'");
                rows[0]["DueDate"] = dpTestDueDateEdit.SelectedDate;

            }
        }


    }

    protected void rgAddAssignedTUTest_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

        if (DtTUAssign != null)
        {
            rgAddAssignedTUTest.DataSource = DtTUAssign;//DataHelper.getAssignedCourses(2, 0);
        }
        else
        {
            rgAddAssignedTUTest.DataSource = DataHelper.GetAssignedCourses(4, 0);
        }
    }

    protected void btnUpdateTest_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            //delete and update
            for (int i = 0; i < DtToAssignTemp.Rows.Count; i++)
            {
                var rows = DtToAssign.Select("CourseAssignmentID = '" + DtToAssignTemp.Rows[i]["CourseAssignmentID"] + "'");

                if (rows.Length == 0) //if data from DtToAssignTemp does not exist in DtToAssign
                {
                    DataHelper.DeleteCourseAssignment(Convert.ToInt32(DtToAssignTemp.Rows[i]["CourseAssignmentID"]), Convert.ToInt32(User.Identity.Name));
                }
                else //update
                {
                    DataHelper.UpdateCourseAssignment(Convert.ToDateTime(DtToAssign.Rows[i]["DueDate"]), Convert.ToInt32(DtToAssignTemp.Rows[i]["CourseAssignmentID"]), Convert.ToInt32(User.Identity.Name));
                }
            }

            var dtAdd = new DataTable();
            dtAdd.Columns.Add("CourseID", typeof(int));
            dtAdd.Columns.Add("DueDate", typeof(DateTime));

            //add
            for (var i = 0; i < DtToAssign.Rows.Count; i++)
            {
                var rows = DtToAssignTemp.Select("CourseID = '" + DtToAssign.Rows[i]["CourseID"] + "'");

                if (rows.Length == 0)
                {
                    DataRow dr = dtAdd.NewRow();
                    dr["CourseID"] = DtToAssign.Rows[i]["CourseID"];
                    dr["DueDate"] = DtToAssign.Rows[i]["DueDate"];
                    dtAdd.Rows.Add(dr);

                }
            }

            DataHelper.BulkAddAssignCourse(4, Convert.ToInt32(EditClassId), Convert.ToInt32(User.Identity.Name), dtAdd);

            btnSelectTest.Visible = false;
            cbLoadTest.DataBind();

            _master.AlertMessage(true, "The test/s has been successfully assigned.");
            ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowViewTest('1');", true);
        }
        catch
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }
    protected void btnUpdateTestCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowViewTest('0');", true);
    }
    #endregion

    #region Assign Course Window

    protected void btnAssign_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            btnAssignCourseAdd.Visible = true;
            btnAssignTestAdd.Visible = false;
            lbAssignedCourses.Items.Clear();
            CurrentWindow = "addCourse";
            AssignCourseWindow.Title = "Select Course/s";
            lbUnassignedCourses.EmptyMessage = "No Unassigned Course";
            lbAssignedCourses.EmptyMessage = "No Course Selected";
            var lblCourseName = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblCourseName");
            var lblPrerequisite = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblPrerequisite");
            var lblDueDate = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblDueDate");

            var tbllbAssignedCourses1 = (System.Web.UI.HtmlControls.HtmlTable)lbAssignedCourses.Controls[0].FindControl("tbllbAssignedCourses1");
            var td1 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses1.FindControl("td1");
            td1.Width = "60%";

            lblDueDate.Visible = true;
            lblPrerequisite.Visible = true;
            lblCourseName.Text = "Course Name";
            SetAssignToSource();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "OpenAssignForm();", true);
        }
    }

    protected void btnEditAssign_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            btnAssignCourseAdd.Visible = true;
            btnAssignTestAdd.Visible = false;
            CurrentWindow = "editCourse";
            AssignCourseWindow.Title = "Select Course/s";
            lbUnassignedCourses.EmptyMessage = "No Unassigned Course";
            lbAssignedCourses.Items.Clear();
            lbAssignedCourses.EmptyMessage = "No Course Selected";
            var lblCourseName = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblCourseName");
            var lblPrerequisite = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblPrerequisite");
            var lblDueDate = (Label)lbAssignedCourses.Controls[0].Controls[0].FindControl("lblDueDate");
            var tbllbAssignedCourses1 = (System.Web.UI.HtmlControls.HtmlTable)lbAssignedCourses.Controls[0].FindControl("tbllbAssignedCourses1");
            var td1 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses1.FindControl("td1");
            td1.Width = "60%";

            lblDueDate.Visible = true;
            lblPrerequisite.Visible = true;
            lblCourseName.Text = "Course Name";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                         "OpenAssignForm();", true);


        }
    }

    public void PopulateDropdownTreeSubcategories(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1, bool isTUTest)
    {
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue) ? Convert.ToInt32(cbCategory1.SelectedValue) : (int?)null;

        if (isTUTest == false)
        {
            var ds1 = DataHelper.GetSubCategoriesByCategoryId(categoryId, "Admin");
            ddtSubcategory1.DataSource = ds1;
        }
        else
        {

            ddtSubcategory1.DataSource = GetTUTestSubCategories(categoryId);

        }

        ddtSubcategory1.DataBind();
    }

    protected void rgAssignedCourses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (DtToAssign != null)
        {
            rgAssignedCourses.DataSource = DtToAssign;
        }
        else
        {
            rgAssignedCourses.DataSource = DataHelper.GetAssignedCourses(2, 0);
        }
    }

    protected void rgAssignedCourses_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var courseId = (Int64)deletedItem.GetDataKeyValue("CourseID");

                var rows = DtToAssign.Select("CourseID = '" + courseId + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var courseId = (Int64)editedItem.GetDataKeyValue("CourseID");
                var dpDueDateEdit = (RadDatePicker)editedItem.FindControl("dpDueDateEdit");

                DataRow[] rows = DtToAssign.Select("CourseID = '" + courseId + "'");
                rows[0]["DueDate"] = dpDueDateEdit.SelectedDate;
            }
        }
    }

    protected void rgEditAssignedCourses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

        if (DtToAssign != null)
        {
            rgEditAssignedCourses.DataSource = DtToAssign;
        }
        else
        {
            rgEditAssignedCourses.DataSource = DataHelper.GetAssignedCourses(2, Convert.ToInt32(EditClassId));
        }
    }

    protected void rgEditAssignedCourses_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var courseId = (Int64)deletedItem.GetDataKeyValue("CourseID");
                var rows = DtToAssign.Select("CourseID = '" + courseId + "'");

                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var courseId = (Int64)editedItem.GetDataKeyValue("CourseID");
                var dpDueDateEdit = (RadDatePicker)editedItem.FindControl("dpDueDateEdit");

                DataRow[] rows = DtToAssign.Select("CourseID = '" + courseId + "'");
                rows[0]["DueDate"] = dpDueDateEdit.SelectedDate;
            }
        }
    }

    protected void SetAssignToSource()
    {
        lbUnassignedCourses.DataSource = DataHelper.GetUnassignedCourses(2, 0,
                                                                         Convert.ToInt32(cbCategoryAssign.SelectedValue),
                                                                         Convert.ToInt32(
                                                                             ddtSubcategoryAssign.SelectedValue));

        btnAssign.Visible = true;
        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();
    }

    protected void SetEditAssignToSource()
    {
        lbUnassignedCourses.DataSource = DataHelper.GetUnassignedCourses(2, Convert.ToInt32(EditClassId), Convert.ToInt32(cbEditCategoryAssign.SelectedValue), Convert.ToInt32(ddtEditSubcategoryAssign.SelectedValue));

        btnEditAssign.Visible = true;
        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();
    }

    protected void lbUnassignedCourses_ItemDataBound(object sender, RadListBoxItemEventArgs e)
    {
        e.Item.Text = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title;
        e.Item.Value = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).CourseID.ToString();
        e.Item.Attributes.Add("Title", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title);
        e.Item.Attributes.Add("Prerequisite", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Prerequisite);
        e.Item.DataBind();
    }


    protected void lbUnassignedCourses_Transferred(object sender, RadListBoxTransferredEventArgs e)
    {

        foreach (RadListBoxItem item in lbAssignedCourses.Items)
        {

            var tbllbAssignedCourses2 = (System.Web.UI.HtmlControls.HtmlTable)item.FindControl("tbllbAssignedCourses2");

            System.Web.UI.HtmlControls.HtmlTableCell tdValue2 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses2.FindControl("td2");
            System.Web.UI.HtmlControls.HtmlTableCell tdValue3 = (System.Web.UI.HtmlControls.HtmlTableCell)tbllbAssignedCourses2.FindControl("td3");

            if (CurrentWindow == "editCourse" || CurrentWindow == "addCourse")
            {

                tdValue2.Visible = true;
                tdValue3.Visible = true;

            }
            else if (CurrentWindow == "editTest" || CurrentWindow == "addTUTest")
            {

                tdValue2.Visible = false;
                tdValue3.Visible = false;
            }
        }



        foreach (RadListBoxItem item in e.Items)
        {
            item.DataBind();
            if (item.Attributes["Prerequisite"] == "Yes")
            {
                if (e.DestinationListBox.ID == "lbAssignedCourses")
                {
                    CtrPrerequisite++;
                }
                else
                {
                    CtrPrerequisite--;
                }
            }
        }

        lbUnassignedCourses.SelectedIndex = -1;
        ctrPrereq.Value = CtrPrerequisite.ToString();
    }

    protected void lbUnassignedCourses_Inserted(object sender, RadListBoxEventArgs e)
    {
        lbUnassignedCourses.SortItems();
    }

    protected void btnAssignTestAdd_Click(object sender, EventArgs e)
    {
        AssignCourseAdd();
    }

    protected void AssignCourseAdd()
    {


        if (CurrentWindow == "editCourse" || CurrentWindow == "addCourse")
        {
            foreach (RadListBoxItem item in lbAssignedCourses.Items)
            {

                var dr = DtToAssign.NewRow();
                var dpCourseAssigned = (RadDatePicker)item.FindControl("dpCourseAssigned");
                var lblCourseAssigned = (Label)item.FindControl("lblCourseAssigned");

                //dpCourseAssigned.Visible = true;
                //lblCourseAssigned.Visible = true;

                dr["CourseAssignmentID"] = 0;
                dr["CourseID"] = item.Value;
                dr["DueDate"] = dpCourseAssigned.SelectedDate;
                dr["Title"] = lblCourseAssigned.Text;

                DtToAssign.Rows.Add(dr);

            }
        }
        else if (CurrentWindow == "editTest")
        {
            foreach (RadListBoxItem item in lbAssignedCourses.Items)
            {

                var dr = DtToAssign.NewRow();
                var dpCourseAssigned = (RadDatePicker)item.FindControl("dpCourseAssigned");
                var lblCourseAssigned = (Label)item.FindControl("lblCourseAssigned");

                //dpCourseAssigned.Visible = true;
                //lblCourseAssigned.Visible = true;

                dr["CourseAssignmentID"] = 0;
                dr["CourseID"] = item.Value;
                dr["DueDate"] = dpCourseAssigned.SelectedDate;
                dr["Title"] = lblCourseAssigned.Text;

                DtToAssign.Rows.Add(dr);

            }
        }
        else if (CurrentWindow == "addTUTest")
        {
            foreach (RadListBoxItem item in lbAssignedCourses.Items)
            {
                var dr = DtTUAssign.NewRow();
                var dpCourseAssigned = (RadDatePicker)item.FindControl("dpCourseAssigned");
                var lblCourseAssigned = (Label)item.FindControl("lblCourseAssigned");

                //dpCourseAssigned.Visible = false;
                // lblCourseAssigned.Visible = false;

                dr["CourseAssignmentID"] = 0;
                dr["CourseID"] = item.Value;
                dr["DueDate"] = dpCourseAssigned.SelectedDate;
                dr["Title"] = lblCourseAssigned.Text;

                DtTUAssign.Rows.Add(dr);
            }
        }

        if (CurrentWindow == "editCourse")
        {
            rgEditAssignedCourses.Rebind();
        }
        else if (CurrentWindow == "addCourse")
        {
            rgAssignedCourses.Rebind();
        }
        else if (CurrentWindow == "editTest")
        {
            rgEditAssignedTests.Rebind();
        }
        else if (CurrentWindow == "addTUTest")
        {
            rgAddAssignedTUTest.Rebind();
        }
        CtrPrerequisite = 0;
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseAssignForm();", true);
    }

    protected void btnAssignCourseCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseAssignForm();", true);
    }

    protected void ddtSubcategoryAssign_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (ValidateControls())
        {
            SetAssignToSource();
        }
    }

    protected void ddtSubcategoryAssign_EntryRemoved(object sender, DropDownTreeEntryEventArgs e)
    {
        btnAssign.Visible = false;
    }

    protected void dtEditSubcategoryAssign_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (ValidateEditControls())
        {
            SetEditAssignToSource();
        }
    }

    protected void dtEditSubcategoryAssign_EntryRemoved(object sender, DropDownTreeEntryEventArgs e)
    {
        btnEditAssign.Visible = false;
    }

    protected void btnUpdateCourse_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            //delete and update
            for (int i = 0; i < DtToAssignTemp.Rows.Count; i++)
            {
                var rows = DtToAssign.Select("CourseAssignmentID = '" + DtToAssignTemp.Rows[i]["CourseAssignmentID"] + "'");

                if (rows.Length == 0) //if data from DtToAssignTemp does not exist in DtToAssign
                {
                    DataHelper.DeleteCourseAssignment(Convert.ToInt32(DtToAssignTemp.Rows[i]["CourseAssignmentID"]), Convert.ToInt32(User.Identity.Name));
                }
                else //update
                {
                    DataHelper.UpdateCourseAssignment(Convert.ToDateTime(DtToAssign.Rows[i]["DueDate"]), Convert.ToInt32(DtToAssignTemp.Rows[i]["CourseAssignmentID"]), Convert.ToInt32(User.Identity.Name));
                }

            }
            DataTable DtAdd;

            DtAdd = new DataTable();
            DtAdd.Columns.Add("CourseID", typeof(int));
            DtAdd.Columns.Add("DueDate", typeof(DateTime));

            //add
            for (int i = 0; i < DtToAssign.Rows.Count; i++)
            {
                var rows = DtToAssignTemp.Select("CourseID = '" + DtToAssign.Rows[i]["CourseID"] + "'");

                if (rows.Length == 0)
                {
                    DataRow dr = DtAdd.NewRow();
                    dr["CourseID"] = DtToAssign.Rows[i]["CourseID"];
                    dr["DueDate"] = DtToAssign.Rows[i]["DueDate"];
                    DtAdd.Rows.Add(dr);
                }
            }


            if (DtAdd != null)
            {
                DataHelper.BulkAddAssignCourse(2, Convert.ToInt32(EditClassId), Convert.ToInt32(User.Identity.Name), DtAdd);
            }
        }
        catch (Exception)
        {

        }

        _master.AlertMessage(true, "The course/s has been successfully assigned.");
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewCourse('1');", true);

        btnEditAssign.Visible = false;
    }
    protected void btnUpdateCourseCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewCourse('0');", true);
    }
    #endregion

    protected void gridClass_PreRender(object sender, EventArgs e)
    {

        if (gridClass.MasterTableView.FilterExpression != string.Empty)
        {
            GridClassRefreshCombos();
        }
    }

    private void CreateDataTableColumns()
    {
        Dt = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "TestTypeID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        Dt.Columns.Add(column);
        Dt.Columns.Add("TestName", typeof(String));

        DtTemp = Dt;
    }

    private void CertificationDataTableColumns()
    {
        DtCert = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "CertificationID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtCert.Columns.Add(column);
        DtCert.Columns.Add("classid", typeof(int));
        DtCert.Columns.Add("clientname", typeof(String));
        DtCert.Columns.Add("trainingtype", typeof(String));
        DtCert.Columns.Add("classname", typeof(String));

        DtCert.Columns.Add("clientid", typeof(int));
        DtCert.Columns.Add("classnameid", typeof(int));
        DtCert.Columns.Add("trainingtypeid", typeof(int));

        DtCertTemp = DtCert;
    }

    protected void btnTestEditUpdate_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            foreach (DataRow row in DtTemp.Rows) //update to 1
            {
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_TestType", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@ClassID", EditClassId.ToString());
                _ocmd.Parameters.AddWithValue("@TestName", row["TestName"].ToString());
                _ocmd.Parameters.AddWithValue("@TestTypeID", row["TestTypeID"].ToString());
                _ocmd.Parameters.AddWithValue("@HideFromList", "1");
                _ocmd.ExecuteReader();
                _oconn.Close();
            }

            foreach (DataRow row in Dt.Rows) //add and update
            {
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_TestType", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@ClassID", EditClassId);
                _ocmd.Parameters.AddWithValue("@TestName", row["TestName"].ToString());
                _ocmd.Parameters.AddWithValue("@TestTypeID", row["TestTypeID"].ToString());

                _ocmd.ExecuteReader();
                _oconn.Close();
            }

            _master.AlertMessage(true, "Congratulations! Record successfully saved.");
            ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseAssignNonTUForm('1');", true);
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void btnTestEditCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseAssignNonTUForm('0');", true);
    }

    protected void btnUpdateCert_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            foreach (DataRow row in DtCertTemp.Rows) //update to 1
            {
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_Certification", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@CertificationID", row["CertificationID"].ToString());
                _ocmd.Parameters.AddWithValue("@ClassID", EditClassId);
                _ocmd.Parameters.AddWithValue("@HideFromList", "1");
                _ocmd.ExecuteReader();
                _oconn.Close();
            }

            foreach (DataRow row in DtCert.Rows) //add and update
            {
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_Certification", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@CertificationID", row["CertificationID"].ToString());
                _ocmd.Parameters.AddWithValue("@ClassID", EditClassId);
                _ocmd.Parameters.AddWithValue("@ClientID", row["ClientID"].ToString());
                _ocmd.Parameters.AddWithValue("@TrainingTypeID", row["TrainingTypeID"].ToString());
                _ocmd.Parameters.AddWithValue("@ClassNameID", row["ClassNameID"].ToString());
                _ocmd.ExecuteReader();
                _oconn.Close();
            }

            _master.AlertMessage(true, "Congratulations! Record successfully saved.");
            ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewCertificate('1');", true);
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void btnUpdateCertCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewCertificate('0');", true);
    }

    protected void rgAssignedTests_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Dt == null)
        {
            CreateDataTableColumns();
        }

        rgAssignedTests.DataSource = Dt;
    }

    protected void rgAssignedTests_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var TestTypeID = (int)deletedItem.GetDataKeyValue("TestTypeID");

                var rows = Dt.Select("TestTypeID = '" + TestTypeID + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var testTypeId = (int)editedItem.GetDataKeyValue("TestTypeID");
                var txtEditTestName = (RadTextBox)editedItem.FindControl("txtEditTestName");

                DataRow[] rows = Dt.Select("TestTypeID = '" + testTypeId + "'");
                rows[0]["TestName"] = txtEditTestName.Text;

            }
        }
    }

    protected void grdEditNonTUTests_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (Dt == null)
        {
            CreateDataTableColumns();
        }

        grdEditNonTUTests.DataSource = Dt;
    }

    protected void grdEditCert_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (DtCert == null)
        {
            CertificationDataTableColumns();
        }

        grdEditCert.DataSource = DtCert;
    }

    protected void grdEditNonTUTests_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var testTypeId = (int)deletedItem.GetDataKeyValue("TestTypeID");

                var rows = Dt.Select("TestTypeID = '" + testTypeId + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var testTypeId = (int)editedItem.GetDataKeyValue("TestTypeID");
                var txtEditTestName = (RadTextBox)editedItem.FindControl("txtEditTestName");

                DataRow[] rows = Dt.Select("TestTypeID = '" + testTypeId + "'");
                rows[0]["TestName"] = txtEditTestName.Text;
            }
        }
    }

    protected void grdEditCert_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var CertificationID = (int)deletedItem.GetDataKeyValue("CertificationID");

                var rows = DtCert.Select("CertificationID = '" + CertificationID + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }
    }

    protected void btnEditCert_Click(object sender, EventArgs e)
    {
        _master = Master;
        if (Page.IsValid)
        {
            int a = 0;

            DtCert.Clear();

            foreach (GridDataItem row in grdEditCert.Items)
            {
                var clientId = (int)row.GetDataKeyValue("ClientID");
                var trainingTypeId = (int)row.GetDataKeyValue("TrainingTypeID");
                var classNameId = (int)row.GetDataKeyValue("ClassNameID");
                var certificationId = (int)row.GetDataKeyValue("CertificationID");

                var lblClient = (Label)row.FindControl("lblclientname");
                var lblTrainingType = (Label)row.FindControl("lbltrainingtype");
                var lblClassName = (Label)row.FindControl("lblclassname");

                DataRow dr = DtCert.NewRow();
                dr["CertificationID"] = certificationId;
                dr["ClassID"] = EditClassId;
                dr["ClientID"] = clientId;
                dr["TrainingTypeID"] = trainingTypeId;
                dr["ClassNameID"] = classNameId;

                dr["ClientName"] = lblClient.Text;
                dr["TrainingType"] = lblTrainingType.Text;
                dr["ClassName"] = lblClassName.Text;

                DtCert.Rows.Add(dr);
            }

            foreach (DataRow row in DtCert.Rows)
            {
                if (cbEditCert.Text == row["ClientName"].ToString() &&
                    ddeditTrainingTypeCert.Text == row["TrainingType"].ToString() &&
                    cbeditCertName.Text == row["ClassName"].ToString())
                {
                    a += 1;
                }
            }

            if (a == 0)
            {
                DataRow dr1 = DtCert.NewRow();
                //dr1["CertificationID"] = "0";
                dr1["ClassID"] = EditClassId;
                dr1["ClientID"] = cbEditCert.SelectedValue;
                dr1["TrainingTypeID"] = ddeditTrainingTypeCert.SelectedValue;
                dr1["ClassNameID"] = cbeditCertName.SelectedValue;

                dr1["ClientName"] = cbEditCert.Text;
                dr1["TrainingType"] = ddeditTrainingTypeCert.Text;
                dr1["ClassName"] = cbeditCertName.Text;
                DtCert.Rows.Add(dr1);

                grdEditCert.Rebind();
            }
            else
            {
                _master.AlertMessage(false, "The record already exists.");
            }

            cbeditCertName.Text = "";
            cbeditCertName.ClearSelection();

        }
    }

    protected void btnAddTest_Click(object sender, EventArgs e)
    {
        _master = Master;
        if (Page.IsValid)
        {
            int a = 0;

            Dt.Clear();

            foreach (GridDataItem row in rgAssignedTests.Items)
            {
                var lblTestName = (Label)row.FindControl("lblTestName");
                DataRow dr = Dt.NewRow();
                dr["TestName"] = lblTestName.Text;
                Dt.Rows.Add(dr);
            }

            foreach (DataRow row in Dt.Rows)
            {
                if (txtTestName.Text == row["TestName"].ToString())
                {
                    a += 1;
                }
            }

            if (a == 0)
            {
                DataRow dr = Dt.NewRow();
                dr["TestName"] = txtTestName.Text;
                txtTestName.Text = string.Empty;
                Dt.Rows.Add(dr);

                rgAssignedTests.Rebind();
            }
            else
            {
                _master.AlertMessage(false, "The record already exists.");
            }
        }
    }

    protected void btnTestEdit_Click(object sender, EventArgs e)
    {
        _master = Master;
        if (Page.IsValid)
        {
            var a = 0;

            foreach (DataRow row in Dt.Rows)
            {
                if (txtEditTestName.Text == row["TestName"].ToString())
                {
                    a += 1;
                }
            }

            if (a == 0)
            {
                DataRow dr = Dt.NewRow();
                dr["TestName"] = txtEditTestName.Text;
                txtEditTestName.Text = string.Empty;
                Dt.Rows.Add(dr);
                grdEditNonTUTests.Rebind();
            }
            else
            {
                _master.AlertMessage(false, "The record already exists.");
            }
        }
    }

    protected void btnViewNonTUTest_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;
        var eventId = item.GetDataKeyValue("ClassID");

        EditClassId = eventId.ToString();

        txtEditTestName.Text = string.Empty;
        Dt.Clear();
        DtTemp.Clear();

        DataTable dt1 = GetTests(Convert.ToInt32(eventId), "1").Tables["getExternalUsersCourseHistory"];

        if (dt1.Rows.Count == 0)
        {
            CreateDataTableColumns();
        }
        else
        {
            foreach (DataRow row in dt1.Rows)
            {
                DataRow dr = Dt.NewRow();
                dr["TestName"] = row["TestName"].ToString();
                dr["TestTypeID"] = row["TestTypeID"].ToString();
                Dt.Rows.Add(dr);
            }

            foreach (DataRow row in dt1.Rows)
            {
                DataRow dr = DtTemp.NewRow();
                dr["TestName"] = row["TestName"].ToString();
                dr["TestTypeID"] = row["TestTypeID"].ToString();
                DtTemp.Rows.Add(dr);
            }
        }

        grdEditNonTUTests.Rebind();

        lblNonTUTestClassID.Text = "Class ID: " + EditClassId;


    }

    protected void btnViewCertificate_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;
        var eventId = item.GetDataKeyValue("ClassID");

        EditClassId = eventId.ToString();

        DtCert.Clear();
        DtCertTemp.Clear();



        DataTable dtCert1 = GetCert(Convert.ToInt32(eventId)).Tables["getCertDetails"];

        if (dtCert1.Rows.Count == 0)
        {
            CertificationDataTableColumns();
        }
        else
        {
            foreach (DataRow row in dtCert1.Rows)
            {
                DataRow dr = DtCert.NewRow();
                dr["CertificationID"] = row["CertificationID"].ToString();
                dr["ClassID"] = row["ClassID"].ToString();
                dr["ClientID"] = row["ClientID"].ToString();
                dr["TrainingTypeID"] = row["TrainingTypeID"].ToString();
                dr["ClassNameID"] = row["ClassNameID"].ToString();
                dr["ClientName"] = row["ClientName"].ToString();
                dr["TrainingType"] = row["TrainingType"].ToString();
                dr["ClassName"] = row["ClassName"].ToString();
                DtCert.Rows.Add(dr);
            }

            foreach (DataRow row in dtCert1.Rows)
            {
                DataRow dr = DtCertTemp.NewRow();
                dr["CertificationID"] = row["CertificationID"].ToString();
                dr["ClassID"] = row["ClassID"].ToString();
                dr["ClientID"] = row["ClientID"].ToString();
                dr["TrainingTypeID"] = row["TrainingTypeID"].ToString();
                dr["ClassNameID"] = row["ClassNameID"].ToString();
                dr["ClientName"] = row["ClientName"].ToString();
                dr["TrainingType"] = row["TrainingType"].ToString();
                dr["ClassName"] = row["ClassName"].ToString();
                DtCertTemp.Rows.Add(dr);
            }
        }

        grdEditCert.Rebind();

        cbEditCert.ClearSelection();
        ddeditTrainingTypeCert.ClearSelection();
        ddeditTrainingTypeCert.Enabled = false;
        cbeditCertName.Enabled = false;
        lblCertificationClassID.Text = "Class ID: " + EditClassId;
    }

    protected void btnViewTUCourse_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;
        var eventId = item.GetDataKeyValue("ClassID");

        EditClassId = eventId.ToString();

        RefreshComboBoxesOnCategorySubcategoryEdit();

        ddtEditSubcategoryAssign.Enabled = false;
        ddtEditSubcategoryAssign.DataSource = null;
        ddtEditSubcategoryAssign.DataBind();

        btnEditAssign.Visible = false;

        DtToAssign = ConvertToDataTable(DataHelper.GetAssignedCourses(2, Convert.ToInt32(EditClassId)));

        DtToAssignTemp = DtToAssign;

        if (DtToAssign != null)
        {
            rgEditAssignedCourses.DataSource = DtToAssign;
        }
        else
        {
            rgEditAssignedCourses.DataSource = DataHelper.GetAssignedCourses(2, Convert.ToInt32(EditClassId));
        }

        rgEditAssignedCourses.DataBind();

        lblTUCourseClassID.Text = "Class ID: " + EditClassId;
    }

    protected void btnAddClass_Click(object sender, EventArgs e)
    {
        _master = Master;
        if (Page.IsValid)
        {
            try
            {
                var classId = "";

                //add class details
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_ClassInfo", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                _ocmd.Parameters.AddWithValue("@Command", "N");
                _ocmd.Parameters.AddWithValue("@TrainerCIM", User.Identity.Name);
                _ocmd.Parameters.AddWithValue("@TraineeSiteID", ddTraineeSite.SelectedValue);
                _ocmd.Parameters.AddWithValue("@TrainingTypeID", ddTrainingType.SelectedValue);
                _ocmd.Parameters.AddWithValue("@LocationTypeID", ddlLocType.SelectedValue);
                _ocmd.Parameters.AddWithValue("@TrainingHoursID", ddTrainingHours.SelectedValue);
                _ocmd.Parameters.AddWithValue("@RTP_Tandim", txtRTPTandim.Text);
                _ocmd.Parameters.AddWithValue("@TrainingValidationNo", txtTrainingValNo.Text);
                _ocmd.Parameters.AddWithValue("@LoginReqNo", txtLoginReqNo.Text);
                _ocmd.Parameters.AddWithValue("@StartDate", dpStartDate.SelectedDate);
                _ocmd.Parameters.AddWithValue("@ProductionStartDate", RadStartDate.SelectedDate);
                _ocmd.Parameters.AddWithValue("@ClassNameID", cbClassName.SelectedValue);
                _ocmd.Parameters.AddWithValue("@ClientID", RadClient.SelectedValue);
                _ocmd.Parameters.AddWithValue("@ClassStatus", "A");
                _ocmd.Parameters.AddWithValue("@ClosedDate", dpEndDate.SelectedDate);
                _ocmd.Parameters.AddWithValue("@WaveNumber", txtWaveNumber.Text);
                _ocmd.Parameters.AddWithValue("@OtherAttrition", txtOtherAttrition.Text);
                _ocmd.Parameters.AddWithValue("@Day1NoShow", txtDay1NoShow.Text);
                _ocmd.Parameters.AddWithValue("@StartingHeadCount", txtStartingHeadCount.Text);
                _ocmd.Parameters.AddWithValue("@OrientationDate", dpOrientationDate.SelectedDate);

                var oDataReader = _ocmd.ExecuteReader();

                if (oDataReader.Read())
                {
                    classId = oDataReader["ClassID"].ToString();
                }
                _oconn.Close();

                //add test
                try
                {
                    var dtAddTUTest = new DataTable();
                    dtAddTUTest.Columns.Add("CourseID", typeof(int));
                    dtAddTUTest.Columns.Add("DueDate", typeof(DateTime));

                    foreach (GridDataItem row in rgAddAssignedTUTest.Items)
                    {
                        var courseId = (Int64)row.GetDataKeyValue("CourseID");

                        var llblDueDate = (Label)row.FindControl("lblDueDate");

                        DataRow dr = dtAddTUTest.NewRow();
                        dr["CourseID"] = courseId;
                        dr["DueDate"] = llblDueDate.Text;

                        dtAddTUTest.Rows.Add(dr);
                    }

                    DataHelper.BulkAddAssignCourse(4, Convert.ToInt32(classId), Convert.ToInt32(User.Identity.Name),
                                                   dtAddTUTest);
                }
                catch (Exception)
                {
                }

                //add non TU Test
                try
                {
                    foreach (DataRow row in Dt.Rows)
                    {
                        Dbconn("Intranet");
                        _ocmd = new SqlCommand("pr_Infinity_Sav_TestType", _oconn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                        _ocmd.Parameters.AddWithValue("@ClassID", classId);
                        _ocmd.Parameters.AddWithValue("@TestName", row["TestName"].ToString());
                        _ocmd.ExecuteReader();
                        _oconn.Close();
                    }
                }
                catch (Exception)
                {
                }

                //add course
                try
                {
                    var dtAddCourse = new DataTable();
                    dtAddCourse.Columns.Add("CourseID", typeof(int));
                    dtAddCourse.Columns.Add("DueDate", typeof(DateTime));

                    foreach (GridDataItem row in rgAssignedCourses.Items)
                    {
                        var courseId = (Int64)row.GetDataKeyValue("CourseID");

                        var llblDueDate = (Label)row.FindControl("lblDueDate");

                        DataRow dr = dtAddCourse.NewRow();
                        dr["CourseID"] = courseId;
                        dr["DueDate"] = llblDueDate.Text;

                        dtAddCourse.Rows.Add(dr);
                    }

                    DataHelper.BulkAddAssignCourse(2, Convert.ToInt32(classId), Convert.ToInt32(User.Identity.Name),
                                                   dtAddCourse);
                }
                catch (Exception)
                {
                }

                gridClass.Rebind();
                ClearFields();

                tsClasss.SelectedIndex = 0;
                mpClass.SelectedIndex = 0;

                _master.AlertMessage(true, "Congratulations! Record successfully saved.");
                txtWaveNumber.Focus();
            }
            catch (Exception)
            {
                _master.AlertMessage(false, "Error. Please try again.");
            }
        }
    }

    protected void gridClass_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {


        gridClass.DataSource = GetClass();
    }

    protected void gridClass_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                var editedItem = e.Item as GridEditableItem;

                if (editedItem != null)
                {
                    //update profile
                    Dbconn("Intranet");
                    _ocmd = new SqlCommand("pr_Infinity_Sav_ClassInfo", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    var traineeSite = (RadComboBox)editedItem.FindControl("ddTraineeSite");
                    var trainingType = (RadComboBox)editedItem.FindControl("editTrainingType");
                    var locType = (RadComboBox)editedItem.FindControl("ddlLocType");
                    var trainingHours = (RadComboBox)editedItem.FindControl("ddTrainingHours");
                    var rtpTandim = (RadNumericTextBox)editedItem.FindControl("txtRTPTandim");
                    var trainingValNo = (RadNumericTextBox)editedItem.FindControl("txtTrainingValNo");

                    var loginReqNo = (RadNumericTextBox)editedItem.FindControl("txtLoginReqNo");
                    var startDate = (RadDatePicker)editedItem.FindControl("dpStartDate");
                    var prodStartDate = (RadDatePicker)editedItem.FindControl("RadStartDate");
                    var client = (RadComboBox)editedItem.FindControl("editClient");

                    var closeDate = (RadDatePicker)editedItem.FindControl("dpEndDate");
                    var waveNumber = (RadNumericTextBox)editedItem.FindControl("txtWaveNumber");
                    var otherAttrition = (RadNumericTextBox)editedItem.FindControl("txtOtherAttrition");
                    var day1NoShow = (RadNumericTextBox)editedItem.FindControl("txtDay1NoShow");
                    var startingHeadCount = (RadNumericTextBox)editedItem.FindControl("txtStartingHeadCount");

                    var orientationDate = (RadDatePicker)editedItem.FindControl("dpOrientationDate");
                    var classId = editedItem.GetDataKeyValue("ClassID").ToString();

                    var classNameId = (RadComboBox)editedItem.FindControl("cbeditClassName");

                    _ocmd.Parameters.AddWithValue("@Command", "E");
                    _ocmd.Parameters.AddWithValue("@TrainerCIM", User.Identity.Name);
                    _ocmd.Parameters.AddWithValue("@TraineeSiteID", traineeSite.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@TrainingTypeID", trainingType.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@LocationTypeID", locType.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@TrainingHoursID", trainingHours.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@RTP_Tandim", rtpTandim.Text);
                    _ocmd.Parameters.AddWithValue("@TrainingValidationNo", trainingValNo.Text);
                    _ocmd.Parameters.AddWithValue("@LoginReqNo", loginReqNo.Text);
                    _ocmd.Parameters.AddWithValue("@StartDate", startDate.SelectedDate);
                    _ocmd.Parameters.AddWithValue("@ProductionStartDate", prodStartDate.SelectedDate);
                    _ocmd.Parameters.AddWithValue("@ClassNameID", classNameId.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@ClientID", client.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@ClassStatus", "A");
                    _ocmd.Parameters.AddWithValue("@ClosedDate", closeDate.SelectedDate);
                    _ocmd.Parameters.AddWithValue("@WaveNumber", waveNumber.Text);
                    _ocmd.Parameters.AddWithValue("@OtherAttrition", otherAttrition.Text);
                    _ocmd.Parameters.AddWithValue("@Day1NoShow", day1NoShow.Text);
                    _ocmd.Parameters.AddWithValue("@StartingHeadCount", startingHeadCount.Text);
                    _ocmd.Parameters.AddWithValue("@OrientationDate", orientationDate.SelectedDate);
                    _ocmd.Parameters.AddWithValue("@ClassID", classId);

                    _ocmd.ExecuteReader();
                    _oconn.Close();

                    gridClass.Rebind();

                    _master = Master;
                    _master.AlertMessage(true, "The Class details has been updated.");
                }
            }
        }
    }

    protected void gridClass_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var ClientID = item.GetDataKeyValue("ClientID").ToString();
                var trainingTypeId = item.GetDataKeyValue("TrainingTypeID").ToString();
                var classNameId = item.GetDataKeyValue("ClassNameID").ToString();
                var listModels = (RadComboBox)item.FindControl("cbeditClassName");

                var editTrainingType = (RadComboBox)item.FindControl("editTrainingType");
                dsTrainingType.SelectParameters["ClientID"].DefaultValue = ClientID;
                editTrainingType.DataBind();

                editTrainingType.SelectedValue = trainingTypeId;

                var dsModelsByManufacturer = (SqlDataSource)item.FindControl("dsEditClassNames");
                dsModelsByManufacturer.SelectParameters["ClientID"].DefaultValue = ClientID;
                dsModelsByManufacturer.SelectParameters["TrainingTypeID"].DefaultValue = trainingTypeId;

                listModels.DataSource = dsModelsByManufacturer;
                listModels.DataBind();

                listModels.SelectedValue = classNameId;
            }
        }
    }

    protected void EditQualificationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        var editQualificationType = sender as RadComboBox;

        if (editQualificationType != null)
        {
            var editedItem = editQualificationType.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var editQualificationDesc = editedItem.FindControl("RadQualificationDesc") as RadComboBox;
                var dsDesc = editedItem.FindControl("dsQualificationDesc") as SqlDataSource;

                dsDesc.SelectParameters["QualificationTypeID"].DefaultValue = editQualificationType.SelectedValue;

                if (editQualificationDesc != null)
                {
                    editQualificationDesc.ClearSelection();
                    editQualificationDesc.DataSource = dsDesc;
                    editQualificationDesc.DataBind();

                    editQualificationDesc.Focus();
                }
            }
        }
    }

    protected void addClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ddTrainingType.Text = "";
        ddTrainingType.ClearSelection();
        dsTrainingType.SelectParameters["ClientID"].DefaultValue = RadClient.SelectedValue;
        dsTrainingType.DataBind();

        cbClassName.Text = "";
        cbClassName.ClearSelection();
        cbClassName.DataBind();
    }

    protected void cbEditCert_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ddeditTrainingTypeCert.Text = "";
        ddeditTrainingTypeCert.ClearSelection();

        dsTrainingType.SelectParameters["ClientID"].DefaultValue = cbEditCert.SelectedValue;

        cbeditCertName.Text = "";
        cbeditCertName.ClearSelection();
        cbeditCertName.DataBind();

        ddeditTrainingTypeCert.Enabled = true;
    }

    protected void addTrainingType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cbClassName.Text = "";
        cbClassName.ClearSelection();
        cbClassName.DataBind();
    }

    protected void ddeditTrainingTypeCert_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

        cbeditCertName.Text = "";
        cbeditCertName.ClearSelection();

        dsEditClassNames.SelectParameters["ClientID"].DefaultValue = cbEditCert.SelectedValue;
        dsEditClassNames.SelectParameters["TrainingTypeID"].DefaultValue = ddeditTrainingTypeCert.SelectedValue;

        cbeditCertName.DataBind();
        cbeditCertName.Enabled = true;
    }

    protected void cbeditCertName_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        btnEditCert.Visible = true;
    }

    protected void editClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var editClient = sender as RadComboBox;

        if (editClient != null)
        {
            var editedItem = editClient.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var editClassName = editedItem.FindControl("cbeditClassName") as RadComboBox;
                var editTrainingType = editedItem.FindControl("editTrainingType") as RadComboBox;

                var dsDesc = editedItem.FindControl("dsEditClassNames") as SqlDataSource;

                dsTrainingType.SelectParameters["ClientID"].DefaultValue = editClient.SelectedValue;
                editTrainingType.Text = string.Empty;
                editTrainingType.DataBind();

                dsDesc.SelectParameters["ClientID"].DefaultValue = editClient.SelectedValue;
                dsDesc.SelectParameters["TrainingTypeID"].DefaultValue = editClient.SelectedValue;


                if (editTrainingType != null)
                {
                    editClassName.Text = string.Empty;
                    editClassName.ClearSelection();
                    editClassName.DataSource = dsDesc;
                    editClassName.DataBind();
                    editClassName.Focus();
                }
            }
        }
    }

    protected void editTrainingType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var editTrainingType = sender as RadComboBox;
        if (editTrainingType != null)
        {
            var editedItem = editTrainingType.NamingContainer as GridEditableItem;

            if (editedItem != null)
            {
                var editClassName = editedItem.FindControl("cbeditClassName") as RadComboBox;
                var editClient = editedItem.FindControl("editClient") as RadComboBox;

                var dsDesc = editedItem.FindControl("dsEditClassNames") as SqlDataSource;

                dsDesc.SelectParameters["ClientID"].DefaultValue = editClient.SelectedValue;
                dsDesc.SelectParameters["TrainingTypeID"].DefaultValue = editTrainingType.SelectedValue;

                if (editTrainingType != null)
                {

                    editClassName.Text = string.Empty;
                    editClassName.ClearSelection();
                    editClassName.DataSource = dsDesc;
                    editClassName.DataBind();
                    editClassName.Focus();
                }
            }
        }
    }

    protected void btnParticipant_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var classId = item.GetDataKeyValue("ClassID").ToString();
        ClassId = Convert.ToInt32(classId);

        lblClassId.Text = "Class ID: " + ClassId;
        gridViewParticipants.CurrentPageIndex = 0;
        gridViewParticipants.Rebind();

    }

    #endregion

    #region PARTICIPANTS

    protected void tsParticipant_TabClick(object sender, RadTabStripEventArgs e)
    {
        divExportParticipant.Visible = tsParticipant.SelectedIndex == 0;
    }

    private void CreateParticipantDataTableColumns()
    {
        DtParticipant = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ParticipantID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtParticipant.Columns.Add(column);
        DtParticipant.Columns.Add("CimNumber", typeof(int));
        DtParticipant.Columns.Add("ParticipantName", typeof(String));
        DtParticipant.Columns.Add("Site", typeof(String));
        //DtParticipantTemp = DtParticipant;
    }

    protected void rgAssignedParticipant_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (DtParticipant == null)
        {
            CreateParticipantDataTableColumns();
        }

        rgAssignedParticipant.DataSource = DtParticipant;
    }

    protected void rgAssignedParticipant_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var ParticipantID = (int)deletedItem.GetDataKeyValue("ParticipantID");

                var rows = DtParticipant.Select("ParticipantID = '" + ParticipantID + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch (Exception)
                {
                }
            }
        }

    }

    private DataSet GetParticipants(int classId, string status)
    {
        _db = new DbHelper("Intranet");

        if (Roles.IsUserInRole("Admin"))
        {
            var ds = _db.GetParticipants(classId, status, "0", 0);
            return ds;
        }
        else
        {
            var ds = _db.GetParticipants(classId, status, Context.User.Identity.Name, 0);
            return ds;
        }

    }

    private void ClearFields_Participants()
    {
        cmbClassID.ClearSelection();
        dpDateJoined.Clear();
        txtCimNumber.Text = string.Empty;
        cbInactiveReason.ClearSelection();
        cbInactiveDesc.ClearSelection();
        radioStatus.SelectedIndex = 0;
        cbInactiveReason.Enabled = false;
        cbInactiveDesc.Enabled = false;
        rfvReason.Enabled = false;
        rfvDesc.Enabled = false;
        txtAddComments.Content = string.Empty;
        chkAddCertified.Checked = false;
        txtAddDescOthers.Text = string.Empty;
        txtAddDescOthers.Visible = false;
    }

    protected void btnAddParticipants_Click(object sender, EventArgs e)
    {
        _master = Master;

        try
        {
            if (Page.IsValid)
            {
                string cimnumbers = "";

                foreach (GridDataItem row in rgAssignedParticipant.Items)
                {
                    var cimnumber = (int)row.GetDataKeyValue("CimNumber");
                    cimnumbers += "," + cimnumber;
                }

                if (rgAssignedParticipant.Items.Count > 0)
                {
                    //update profile
                    Dbconn("Intranet");
                    _ocmd = new SqlCommand("pr_Infinity_Sav_Participant", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    _ocmd.Parameters.AddWithValue("@Command", "N");

                    if (chkAddCertified.Checked)
                    {
                        _ocmd.Parameters.AddWithValue("@CertificationStatus", "C");
                    }

                    _ocmd.Parameters.AddWithValue("@ClassID", cmbClassID.SelectedItem.Text);
                    _ocmd.Parameters.AddWithValue("@DateJoined", dpDateJoined.SelectedDate);
                    _ocmd.Parameters.AddWithValue("@Comments", txtAddComments.Content);
                    _ocmd.Parameters.AddWithValue("@MultipleCIMs", cimnumbers);
                    _ocmd.Parameters.AddWithValue("@ParticipantStatus", radioStatus.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@Reason", cbInactiveReason.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@Description", cbInactiveDesc.SelectedValue);
                    _ocmd.Parameters.AddWithValue("@OtherDescription", txtAddDescOthers.Text);

                    _ocmd.ExecuteReader();
                    _oconn.Close();

                    DtParticipant.Clear();
                    rgAssignedParticipant.DataSource = DtParticipant;
                    rgAssignedParticipant.DataBind();

                    gridParticipants.Rebind();

                    ClearFields_Participants();

                    tsParticipant.SelectedIndex = 0;
                    mpParticipant.SelectedIndex = 0;

                    _master.AlertMessage(true, "Record successfully saved.");
                }
                else
                {
                    _master.AlertMessage(false, "Please add participant to the list.");
                }
            }
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void radioStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (radioStatus.SelectedIndex == 0)
        {
            cbInactiveDesc.Enabled = false;
            cbInactiveReason.Enabled = false;

            cbInactiveReason.ClearSelection();
            cbInactiveReason.ClearSelection();

            rfvReason.Enabled = false;
            rfvDesc.Enabled = false;
        }
        else
        {
            cbInactiveDesc.Enabled = true;
            cbInactiveReason.Enabled = true;

            rfvReason.Enabled = true;
            rfvDesc.Enabled = true;
        }
    }

    protected void btnGetCIMInfo_Click(object sender, EventArgs e)
    {
        _master = Master;

        try
        {
            if (Page.IsValid) //if cimnumber and classid has value
            {
                int a = 0;

                foreach (GridDataItem row in rgAssignedParticipant.Items)
                {
                    var cimnumber = (int)row.GetDataKeyValue("CimNumber");

                    if (txtCimNumber.Text == cimnumber.ToString())
                    {
                        a += 1;
                    }
                }

                if (a == 0) // CIM is not yet on the list
                {
                    Dbconn("Intranet");
                    _ocmd = new SqlCommand("pr_Lkp_GetCIMInfo", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    _ocmd.Parameters.AddWithValue("@CIMNo", txtCimNumber.Text);
                    var oDataReader = _ocmd.ExecuteReader();

                    if (oDataReader.Read()) //check if CIM is valid
                    {
                        if (cmbClassID.SelectedValue == oDataReader["ClassID"].ToString()) //check if CIM is existing already on classid
                        {
                            _master.AlertMessage(false, "Participant already exists in Class ID " + cmbClassID.SelectedValue);
                            txtCimNumber.Focus();
                        }
                        else
                        {
                            DataRow dr = DtParticipant.NewRow();
                            dr["CimNumber"] = txtCimNumber.Text;
                            dr["ParticipantName"] = oDataReader["CIMName"].ToString();
                            dr["Site"] = oDataReader["SiteDesc"].ToString();
                            DtParticipant.Rows.Add(dr);
                            rgAssignedParticipant.Rebind();
                            txtCimNumber.Text = string.Empty;
                        }
                    }
                    else
                    {
                        _master.AlertMessage(false, "Invalid CIM No.");
                        txtCimNumber.Focus();
                    }

                    oDataReader.Close();
                    _oconn.Close();
                }
                else
                {
                    _master.AlertMessage(false, "CIM No. already added to the list.");
                    txtCimNumber.Focus();
                }
            }
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void gridParticipants_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        gridParticipants.DataSource = GetParticipants(Convert.ToInt32(ClassId), "L");
    }

    protected void gridParticipants_ItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;

        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                try
                {
                    var editedItem = e.Item as GridEditableItem;

                    if (editedItem != null)
                    {
                        //update profile
                        Dbconn("Intranet");
                        _ocmd = new SqlCommand("pr_Infinity_Sav_Participant", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        var classId = (RadComboBox)editedItem.FindControl("cmbClassID");
                        var dateJoined = (RadDatePicker)editedItem.FindControl("dpDateJoined");
                        var cimNumber = (RadNumericTextBox)editedItem.FindControl("txtCimNumber");
                        var status = (RadioButtonList)editedItem.FindControl("radioStatus");
                        var inactiveReason = (RadComboBox)editedItem.FindControl("cbeditInactiveReason");
                        var inactiveDesc = (RadComboBox)editedItem.FindControl("cbeditInactiveDesc");
                        var comments = (RadEditor)editedItem.FindControl("txteditComments");
                        var participantId = editedItem.GetDataKeyValue("ParticipantID").ToString();
                        var othersDesc = (RadTextBox)editedItem.FindControl("txtEditDescOthers");
                        var chkeditCertified = (CheckBox)editedItem.FindControl("chkeditCertified");

                        _ocmd.Parameters.AddWithValue("@Command", "E");

                        _ocmd.Parameters.AddWithValue("@ClassID", classId.SelectedItem.Text);
                        _ocmd.Parameters.AddWithValue("@DateJoined", dateJoined.SelectedDate);
                        _ocmd.Parameters.AddWithValue("@Comments", comments.Content);
                        _ocmd.Parameters.AddWithValue("@ParticipantCIM", cimNumber.Text);
                        _ocmd.Parameters.AddWithValue("@ParticipantStatus", status.SelectedValue);
                        _ocmd.Parameters.AddWithValue("@Reason", inactiveReason.SelectedValue);
                        _ocmd.Parameters.AddWithValue("@Description", inactiveDesc.SelectedValue);
                        _ocmd.Parameters.AddWithValue("@ParticipantID", participantId);

                        if (othersDesc.Visible)
                        {
                            _ocmd.Parameters.AddWithValue("@OtherDescription", othersDesc.Text);
                        }

                        if (chkeditCertified.Checked)
                        {
                            _ocmd.Parameters.AddWithValue("@CertificationStatus", "C");
                        }

                        _ocmd.ExecuteReader();
                        _oconn.Close();

                        gridClass.Rebind();

                        _master.AlertMessage(true, "Congratulations! Record successfully updated.");
                    }

                }
                catch (Exception)
                {
                    _master.AlertMessage(false, "Error. Please try Again");
                }
            }
        }
    }

    protected void gridParticipants_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var reasonId = item.GetDataKeyValue("ReasonID").ToString();
                var reasonDescriptionId = item.GetDataKeyValue("DescriptionID").ToString();
                var certificationStatus = item.GetDataKeyValue("CertificationStatus").ToString();

                var dsParticipantDesc = (SqlDataSource)item.FindControl("dsParticipantDesc");
                var radioStatus1 = (RadioButtonList)item.FindControl("radioStatus");
                var editReason = (RadComboBox)item.FindControl("cbeditInactiveReason");
                var editDesc = (RadComboBox)item.FindControl("cbeditInactiveDesc");
                var editrfvReason = (RequiredFieldValidator)item.FindControl("rfvReason");
                var editrfvDesc = (RequiredFieldValidator)item.FindControl("rfvDesc");
                var editDescOthers = (RadTextBox)item.FindControl("txtEditDescOthers");
                var dseditOtherDesc = (SqlDataSource)item.FindControl("dsParticipantOtherDesc");
                var editRequiredFieldValidator15 = (RequiredFieldValidator)item.FindControl("RequiredFieldValidator15");

                var chkeditCertified = (CheckBox)item.FindControl("chkeditCertified");

                if (certificationStatus == "Certified")
                {
                    chkeditCertified.Checked = true;
                }

                if (radioStatus1.SelectedIndex == 0)
                {
                    editDesc.Enabled = false;
                    editReason.Enabled = false;

                    editrfvReason.Enabled = false;
                    editrfvDesc.Enabled = false;

                    editReason.ClearSelection();
                    editDesc.ClearSelection();
                }
                else
                {
                    editDesc.Enabled = true;
                    editReason.Enabled = true;

                    editrfvReason.Enabled = true;
                    editrfvDesc.Enabled = true;

                    dsParticipantDesc.SelectParameters["reasonid"].DefaultValue = reasonId;
                    editDesc.ClearSelection();
                    editDesc.DataSource = dsParticipantDesc;
                    editDesc.DataBind();
                    editDesc.SelectedValue = reasonDescriptionId;

                    var dv = (DataView)dseditOtherDesc.Select(DataSourceSelectArguments.Empty);
                    if (dv != null)
                    {
                        var allowOthers = (int)dv.Table.Rows[0][0];

                        if (allowOthers > 0)
                        {
                            editDescOthers.Visible = false;
                            editDescOthers.Text = string.Empty;
                            editRequiredFieldValidator15.Enabled = false;
                        }
                        else
                        {
                            editDescOthers.Visible = true;
                            editRequiredFieldValidator15.Enabled = true;
                        }
                    }
                }
            }
        }
    }

    protected void cbeditInactiveReason_SelectedIndexChanged(object sender, EventArgs e)
    {
        var editInactiveReason = sender as RadComboBox;

        if (editInactiveReason != null)
        {
            var editedItem = editInactiveReason.NamingContainer as GridEditableItem;

            if (editedItem != null)
            {
                var editReason = editedItem.FindControl("cbeditInactiveReason") as RadComboBox;
                var editDesc = editedItem.FindControl("cbeditInactiveDesc") as RadComboBox;
                var dsParticipantDesc = editedItem.FindControl("dsParticipantDesc") as SqlDataSource;

                dsParticipantDesc.SelectParameters["Reasonid"].DefaultValue = editReason.SelectedValue;

                editDesc.ClearSelection();
                editDesc.DataSource = dsParticipantDesc;
                editDesc.DataBind();
                editDesc.Focus();
            }
        }
    }

    protected void cbeditInactiveDesc_SelectedIndexChanged(object sender, EventArgs e)
    {
        var editInactiveDesc = sender as RadComboBox;

        if (editInactiveDesc != null)
        {
            var editedItem = editInactiveDesc.NamingContainer as GridEditableItem;

            if (editedItem != null)
            {
                var editdsParticipantOtherDesc = editedItem.FindControl("dsParticipantOtherDesc") as SqlDataSource;
                var txtEditDescOthers = editedItem.FindControl("txtEditDescOthers") as RadTextBox;
                var editRequiredFieldValidator15 = editedItem.FindControl("RequiredFieldValidator15") as RequiredFieldValidator;

                var dv = (DataView)editdsParticipantOtherDesc.Select(DataSourceSelectArguments.Empty);
                var allowOthers = (int)dv.Table.Rows[0][0];

                if (allowOthers > 0)
                {
                    txtEditDescOthers.Visible = false;
                    txtEditDescOthers.Text = string.Empty;
                    editRequiredFieldValidator15.Enabled = false;
                }
                else
                {
                    txtEditDescOthers.Visible = true;
                    editRequiredFieldValidator15.Enabled = true;
                }
            }
        }
    }

    protected void cbInactiveDesc_SelectedIndexChanged(object sender, EventArgs e)
    {
        var dv = (DataView)dsParticipantOtherDesc.Select(DataSourceSelectArguments.Empty);
        if (dv != null)
        {
            var allowOthers = (int)dv.Table.Rows[0][0];
            if (allowOthers > 0)
            {
                txtAddDescOthers.Visible = false;
                txtAddDescOthers.Text = string.Empty;
                RequiredFieldValidator15.Enabled = false;
            }
            else
            {
                txtAddDescOthers.Visible = true;
                RequiredFieldValidator15.Enabled = true;
            }
        }
    }

    protected void radioeditStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        var editStatus = sender as RadioButtonList;

        if (editStatus != null)
        {
            var editedItem = editStatus.NamingContainer as GridEditableItem;

            if (editedItem != null)
            {
                var editReason = editedItem.FindControl("cbeditInactiveReason") as RadComboBox;
                var editDesc = editedItem.FindControl("cbeditInactiveDesc") as RadComboBox;
                var editrfvReason = editedItem.FindControl("rfvReason") as RequiredFieldValidator;
                var editrfvDesc = editedItem.FindControl("rfvDesc") as RequiredFieldValidator;

                if (editStatus.SelectedIndex == 0)
                {
                    editDesc.Enabled = false;
                    editReason.Enabled = false;

                    editrfvReason.Enabled = false;
                    editrfvDesc.Enabled = false;

                    editReason.ClearSelection();
                    editDesc.ClearSelection();
                }
                else
                {
                    editDesc.Enabled = true;
                    editReason.Enabled = true;

                    editrfvReason.Enabled = true;
                    editrfvDesc.Enabled = true;
                }
            }
        }
    }

    //protected void btneditGetCIMInfo_Click(object sender, EventArgs e)
    //{
    //    var btneditGetCIMInfo = sender as LinkButton;

    //    if (btneditGetCIMInfo != null)
    //    {
    //        var editedItem = btneditGetCIMInfo.NamingContainer as GridEditableItem;

    //        if (editedItem != null)
    //        {
    //            var edittxtCimNumber = editedItem.FindControl("txtCimNumber") as RadNumericTextBox;
    //            var edittxtName = editedItem.FindControl("txtName") as RadTextBox;
    //            var edittxtSite = editedItem.FindControl("txtSite") as RadTextBox;

    //            if (edittxtCimNumber.Text.Trim() == "")
    //            {
    //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
    //                 "alert('Please enter CIM No.');", true);
    //                edittxtCimNumber.Focus();
    //            }
    //            else
    //            {
    //                Dbconn("Intranet");
    //                _ocmd = new SqlCommand("pr_Lkp_GetCIMInfo", _oconn)
    //                {
    //                    CommandType = CommandType.StoredProcedure
    //                };

    //                _ocmd.Parameters.AddWithValue("@CIMNo", edittxtCimNumber.Text);
    //                var oDataReader = _ocmd.ExecuteReader();

    //                if (oDataReader.Read())
    //                {
    //                    edittxtName.Text = oDataReader["CIMName"].ToString();
    //                    edittxtSite.Text = oDataReader["SiteDesc"].ToString();
    //                }
    //                else
    //                {
    //                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
    //                    "alert('Invalid CIM No. Please change the CIM No.');", true);
    //                    edittxtName.Text = string.Empty;
    //                    edittxtSite.Text = string.Empty;
    //                    edittxtCimNumber.Focus();
    //                }
    //                oDataReader.Close();
    //                _oconn.Close();
    //            }
    //        }
    //    }
    //}

    //NEW JAY

    protected void gridViewParticipants_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        gridViewParticipants.DataSource = GetParticipants(Convert.ToInt32(ClassId), "-");
    }

    protected void gridViewParticipants_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var gridItem = e.Item as GridDataItem;
            var name = gridItem.GetDataKeyValue("ParticipantName").ToString();
            gridItem.ToolTip = name;
        }
    }

    protected void btnEditParticipants_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;

        if (btn != null)
        {
            var editedItem = btn.NamingContainer as GridEditableItem;

            if (editedItem != null)
            {
                var cimNo = editedItem.GetDataKeyValue("cimnumber").ToString();
                var classId = editedItem.GetDataKeyValue("ClassID").ToString();

                var ds = DataHelper.GetClassParticipant(Convert.ToInt32(classId), Convert.ToInt32(cimNo)).FirstOrDefault();
                if (ds != null)
                {
                    lblParticipantId_editParticipant.Text = ds.ParticipantID.ToString();

                    lblCimNo_editParticipant.Text = ds.cimnumber.ToString();
                    lblName_editParticipant.Text = ds.ParticipantName;
                    lblSite_editParticipant.Text = ds.ParticipantSite;
                    cbClassId_editParticipant.SelectedValue = ds.ClassID.ToString();
                    dpDateJoined_editParticipant.SelectedDate = Convert.ToDateTime(ds.DateJoined);
                    rblStatus_editParticipant.SelectedValue = ds.ParticipantStatus;
                    cbReason_editParticipant.SelectedValue = ds.ReasonID.ToString();
                    txtDescOther_editParticipant.Text = ds.OtherDescription;

                    if (ds.ParticipantStatus == "Active")
                    {
                        cbReason_editParticipant.Text = "";
                        cbReason_editParticipant.ClearSelection();
                        cbReason_editParticipant.Enabled = false;
                        rfvReason_editParticipant.Enabled = false;

                        cbDesc_editParticipant.Text = "";
                        cbDesc_editParticipant.ClearSelection();
                        cbDesc_editParticipant.Enabled = false;
                        rfvDesc_editParticipant.Enabled = false;

                        txtDescOther_editParticipant.Visible = false;
                        rfvDescOther_editParticipant.Enabled = false;
                    }
                    else
                    {
                        cbDesc_editParticipant.Enabled = true;
                        cbReason_editParticipant.Enabled = true;

                        rfvReason_editParticipant.Enabled = true;
                        rfvDesc_editParticipant.Enabled = true;

                        dsParticipantDesc.SelectParameters["reasonid"].DefaultValue = ds.ReasonID.ToString();

                        cbDesc_editParticipant.ClearSelection();
                        cbDesc_editParticipant.DataSource = dsParticipantDesc;
                        cbDesc_editParticipant.DataBind();
                        cbDesc_editParticipant.SelectedValue = ds.DescriptionID.ToString();

                        var dv = (DataView)dsDescOther_editParticipant.Select(DataSourceSelectArguments.Empty);

                        if (dv != null)
                        {
                            var allowOthers = (int)dv.Table.Rows[0][0];

                            if (allowOthers > 0)
                            {
                                txtDescOther_editParticipant.Visible = false;
                                rfvDescOther_editParticipant.Enabled = false;
                            }
                            else
                            {
                                txtDescOther_editParticipant.Visible = true;
                                rfvDescOther_editParticipant.Enabled = true;
                            }
                        }
                    }

                    chkCertified_editParticipant.Checked = ds.CertificationStatus == "Certified";

                    txtComment_editParticipant.Content = ds.Comments;
                }
            }
        }
    }

    protected void rblStatusEditParticipant_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblStatus_editParticipant.SelectedIndex == 0)
        {
            cbDesc_editParticipant.Enabled = false;
            cbReason_editParticipant.Enabled = false;

            rfvReason_editParticipant.Enabled = false;
            rfvDesc_editParticipant.Enabled = false;

            cbReason_editParticipant.ClearSelection();
            cbDesc_editParticipant.ClearSelection();
            txtDescOther_editParticipant.Visible = false;
            txtDescOther_editParticipant.Text = string.Empty;
        }
        else
        {
            cbDesc_editParticipant.Enabled = true;
            cbReason_editParticipant.Enabled = true;

            rfvReason_editParticipant.Enabled = true;
            rfvDesc_editParticipant.Enabled = true;
        }
    }

    protected void cbReasonEditParticipant_SelectedIndexChanged(object sender, EventArgs e)
    {
        dsDesc_editParticipant.SelectParameters["Reasonid"].DefaultValue = cbReason_editParticipant.SelectedValue;

        cbDesc_editParticipant.ClearSelection();
        cbDesc_editParticipant.DataSource = dsDesc_editParticipant;
        cbDesc_editParticipant.DataBind();
        cbDesc_editParticipant.Focus();
    }

    protected void cbDescEditParticipant_SelectedIndexChanged(object sender, EventArgs e)
    {
        var dv = (DataView)dsDescOther_editParticipant.Select(DataSourceSelectArguments.Empty);

        if (dv != null)
        {
            var allowOthers = (int)dv.Table.Rows[0][0];
            if (allowOthers > 0)
            {
                txtDescOther_editParticipant.Visible = false;
                txtDescOther_editParticipant.Text = string.Empty;
                rfvDescOther_editParticipant.Enabled = false;
            }
            else
            {
                txtDescOther_editParticipant.Visible = true;
                rfvDescOther_editParticipant.Enabled = true;
            }
        }
    }

    protected void btnUpdateParticipant_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            _master = Master;
            var descOther = txtDescOther_editParticipant.Visible ? txtDescOther_editParticipant.Text : "";
            var certStat = chkCertified_editParticipant.Checked ? Convert.ToChar("C") : Convert.ToChar("N");
            var stat = rblStatus_editParticipant.SelectedValue == "Active" ? Convert.ToChar("A") : Convert.ToChar("I");
            var reason = !string.IsNullOrEmpty(cbReason_editParticipant.SelectedValue)
                             ? Convert.ToInt32(cbReason_editParticipant.SelectedValue)
                             : (int?)null;
            var desc = !string.IsNullOrEmpty(cbDesc_editParticipant.SelectedValue)
                           ? Convert.ToInt32(cbDesc_editParticipant.SelectedValue)
                           : (int?)null;

            var retVal = DataHelper.UpdateParticipant(Convert.ToChar("E"),
                                                      Convert.ToInt32(lblParticipantId_editParticipant.Text), null,
                                                      Convert.ToInt32(cbClassId_editParticipant.SelectedValue),
                                                      dpDateJoined_editParticipant.SelectedDate,
                                                      txtComment_editParticipant.Content,
                                                      Convert.ToInt32(lblCimNo_editParticipant.Text),
                                                      stat, certStat, reason, desc, null, descOther);
            switch (retVal)
            {
                case true:
                    _master.AlertMessage(true, "The Participant details successfully updated.");
                    ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowEditParticipant('1');", true);
                    gridViewParticipants.Rebind();
                    gridParticipants.Rebind();
                    break;
                case false:
                    _master.AlertMessage(false, "Request failed. Please try again.");
                    break;
            }
        }
    }

    protected void btnUpdateParticipantCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowEditParticipant('0');", true);
    }

    #endregion

    #region ATTENDANCE

    private static string GetComboBoxSelectedItems(RadComboBox combo)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                sb.Append(item.Value + ",");
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    private void ClearForm_UpdateAttendance()
    {
        cbUpdateClassAttendance.ClearSelection();
        cbUpdateClassAttendance.Text = string.Empty;
        dpUpdateAttendanceFrom.Clear();
        dpUpdateAttendanceTo.Clear();
        cbUpdateAttendanceStatus.ClearSelection();
        cbUpdateAttendanceStatus.Text = string.Empty;
        txtUpdateTraininigHours.Text = string.Empty;
        radioUpdateApply.SelectedIndex = 0;
        cbUpdateAttendanceParticipants.Enabled = false;
        cbUpdateAttendanceParticipants.ClearSelection();
        cbUpdateAttendanceParticipants.ClearCheckedItems();
        cbUpdateAttendanceParticipants.Text = string.Empty;
        rfvUpdateParticipants.Enabled = false;
    }

    private void ClearForm_RemoveAttendance()
    {
        cbAttendanceRemove.ClearSelection();
        cbAttendanceRemove.Text = string.Empty;
        dpRemoveDateFrom.Clear();
        dpRemoveDateTo.Clear();

        rbRemoveAttendanceApply.SelectedIndex = 0;
        cbRemoveParticipants.Enabled = false;
        cbRemoveParticipants.ClearSelection();
        cbRemoveParticipants.ClearCheckedItems();
        cbRemoveParticipants.Text = string.Empty;
        rfvRemoveParticipants.Enabled = false;
    }

    private DataSet GetAttendance(int? classId, DateTime classDate)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetAttendance(classId, classDate);
        return ds;
    }

    protected void gridAttendance_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var classId = string.IsNullOrEmpty(cbAttendanceClassID.SelectedValue) ? 0 : Convert.ToInt32(cbAttendanceClassID.SelectedValue);
        gridAttendance.DataSource = GetAttendance(classId, Convert.ToDateTime(dpAttendanceForm.SelectedDate));
    }

    protected void gridAttendance_ItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                try
                {
                    var editedItem = e.Item as GridEditableItem;

                    if (editedItem != null)
                    {
                        //update profile
                        Dbconn("Intranet");
                        _ocmd = new SqlCommand("pr_Infinity_Sav_Attendance", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        var classId = editedItem.GetDataKeyValue("ClassID").ToString();
                        var classDate = editedItem.GetDataKeyValue("ClassDate").ToString();
                        var attendanceStatusId = (RadComboBox)editedItem.FindControl("cbAttendanceStatus");
                        var trainingHours = (RadNumericTextBox)editedItem.FindControl("txtTrainingHours");
                        var participantId = editedItem.GetDataKeyValue("ParticipantID").ToString();

                        _ocmd.Parameters.AddWithValue("@ClassID", classId);
                        _ocmd.Parameters.AddWithValue("@DateFrom", classDate);
                        _ocmd.Parameters.AddWithValue("@DateTo", "");
                        _ocmd.Parameters.AddWithValue("@AttendanceStatusID", attendanceStatusId.SelectedValue);
                        _ocmd.Parameters.AddWithValue("@TrainingHours", trainingHours.Text);
                        _ocmd.Parameters.AddWithValue("@ParticipantIDs", participantId);
                        _ocmd.Parameters.AddWithValue("@SaveType", "Individual");

                        _ocmd.ExecuteReader();
                        _oconn.Close();

                        gridClass.Rebind();

                        _master.AlertMessage(true, "Congratulations! Record successfully updated.");
                    }

                }
                catch (Exception)
                {
                    _master.AlertMessage(false, "Error. Please try again.");
                }
            }
        }
    }

    protected void btnViewAttendance_Click(object sender, EventArgs e)
    {
        gridAttendance.CurrentPageIndex = 0;
        gridAttendance.Rebind();
    }

    protected void radioUpdateApply_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (radioUpdateApply.SelectedIndex == 0)
        {
            cbUpdateAttendanceParticipants.Text = string.Empty;
            cbUpdateAttendanceParticipants.Enabled = false;
            cbUpdateAttendanceParticipants.ClearSelection();
            cbUpdateAttendanceParticipants.ClearCheckedItems();
            rfvUpdateParticipants.Enabled = false;
        }
        else
        {
            rfvUpdateParticipants.Enabled = true;
            cbUpdateAttendanceParticipants.Enabled = true;
            cbUpdateAttendanceParticipants.ClearSelection();
            dsActiveParticipants.SelectParameters["ClassID"].DefaultValue = cbUpdateClassAttendance.SelectedIndex == -1 ? "0" : cbUpdateClassAttendance.SelectedItem.Text;
            cbUpdateAttendanceParticipants.DataSource = dsActiveParticipants;
            cbUpdateAttendanceParticipants.DataBind();
            cbUpdateAttendanceParticipants.Focus();
        }

        windowUpdateAttendance.VisibleOnPageLoad = true;
    }

    protected void rbRemoveAttendanceApply_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbRemoveAttendanceApply.SelectedIndex == 0)
        {
            cbRemoveParticipants.Text = string.Empty;
            cbRemoveParticipants.Enabled = false;
            cbRemoveParticipants.ClearSelection();
            cbRemoveParticipants.ClearCheckedItems();
            rfvRemoveParticipants.Enabled = false;
        }
        else
        {
            rfvRemoveParticipants.Enabled = true;
            cbRemoveParticipants.Enabled = true;
            cbRemoveParticipants.ClearSelection();
            dsActiveParticipants.SelectParameters["ClassID"].DefaultValue = cbAttendanceRemove.SelectedIndex == -1 ? "0" : cbAttendanceRemove.SelectedItem.Text;
            cbRemoveParticipants.DataSource = dsActiveParticipants;
            cbRemoveParticipants.DataBind();
            cbRemoveParticipants.Focus();

            windowRemoveAttendance.VisibleOnPageLoad = true;
        }
    }

    protected void btnUpdateAttendance_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            if (Page.IsValid)
            {
                var participants = GetComboBoxSelectedItems(cbUpdateAttendanceParticipants);

                //update profile
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_Attendance", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@ClassID", cbUpdateClassAttendance.SelectedItem.Text);
                _ocmd.Parameters.AddWithValue("@DateFrom", dpUpdateAttendanceFrom.SelectedDate);
                _ocmd.Parameters.AddWithValue("@DateTo", dpUpdateAttendanceTo.SelectedDate);
                _ocmd.Parameters.AddWithValue("@AttendanceStatusID", cbUpdateAttendanceStatus.SelectedValue);
                _ocmd.Parameters.AddWithValue("@TrainingHours", txtUpdateTraininigHours.Text);

                if (radioUpdateApply.SelectedIndex == 0)
                {
                    _ocmd.Parameters.AddWithValue("@ParticipantIDs", "");
                    _ocmd.Parameters.AddWithValue("@SaveType", "All");
                }
                else
                {
                    _ocmd.Parameters.AddWithValue("@ParticipantIDs", participants);
                    _ocmd.Parameters.AddWithValue("@SaveType", "Multiple");
                }

                _ocmd.ExecuteReader();
                _oconn.Close();

                gridAttendance.Rebind();
                ClearForm_UpdateAttendance();

                _master.AlertMessage(true, "Congratulations! Record successfully updated.");
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowUpdateAttendance('1');", true);
            }
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }
    protected void btnUpdateAttendanceCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowUpdateAttendance('0');", true);
    }

    protected void btnResetAttendance_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            if (Page.IsValid)
            {
                var participants = GetComboBoxSelectedItems(cbRemoveParticipants);

                //update profile
                Dbconn("Intranet");
                _ocmd = new SqlCommand("pr_Infinity_Sav_Attendance", _oconn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                _ocmd.Parameters.AddWithValue("@ClassID", cbAttendanceRemove.SelectedItem.Text);
                _ocmd.Parameters.AddWithValue("@DateFrom", dpRemoveDateFrom.SelectedDate);
                _ocmd.Parameters.AddWithValue("@DateTo", dpRemoveDateTo.SelectedDate);

                if (rbRemoveAttendanceApply.SelectedIndex == 0)
                {
                    _ocmd.Parameters.AddWithValue("@ParticipantIDs", "");
                    _ocmd.Parameters.AddWithValue("@SaveType", "RemoveAll");
                }
                else
                {
                    _ocmd.Parameters.AddWithValue("@ParticipantIDs", participants);
                    _ocmd.Parameters.AddWithValue("@SaveType", "RemoveMultiple");
                }

                _ocmd.ExecuteReader();
                _oconn.Close();

                gridAttendance.Rebind();
                ClearForm_RemoveAttendance();

                _master.AlertMessage(true, "Congratulations! Record successfully removed.");
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowRemoveAttendance('1');", true);
            }
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void btnResetAttendanceCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_windowRemoveAttendance('0');", true);
    }

    #endregion

    #region TEST SCORES

    private DataSet GetTestScores(int classId, int testTypeId, int status)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetTestScore(classId, testTypeId, status);
        return ds;
    }

    protected void rbTestType_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbLoadTest.Enabled = true;
        cbLoadTest.ClearSelection();

        gridTestParticipants.DataSource = GetTestScores(0, 0, 0);
        gridTestParticipants.CurrentPageIndex = 0;
        gridTestParticipants.DataBind();

        if (cbLoadClass.SelectedIndex == -1 || cbTestType.SelectedIndex == -1)
        {
            dsTestsTaken.SelectParameters["ClassID"].DefaultValue = "0";
            dsTestsTaken.SelectParameters["TestType"].DefaultValue = "0";
        }
        else
        {
            dsTestsTaken.SelectParameters["ClassID"].DefaultValue = cbLoadClass.SelectedValue;
            dsTestsTaken.SelectParameters["TestType"].DefaultValue = cbTestType.SelectedIndex.ToString();
        }
        cbLoadTest.DataSource = dsTestsTaken;
        cbLoadTest.DataBind();
        cbLoadTest.Focus();
    }

    protected void cbLoadTest_SelectedIndexChanged(object sender, EventArgs e)
    {
        gridTestParticipants.CurrentPageIndex = 0;
        gridTestParticipants.Rebind();
    }

    protected void gridTestParticipants_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        gridTestParticipants.DataSource = GetTestScores(cbLoadClass.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbLoadClass.SelectedValue),
            cbLoadTest.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbLoadTest.SelectedValue), cbTestType.SelectedIndex);
    }

    protected void gridTestParticipants_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var item = (GridDataItem)e.Item;
            var editButton = (ImageButton)item["EditCommandColumn"].Controls[0];

            editButton.Visible = cbTestType.SelectedIndex != 0;
        }
        else if (e.Item.IsInEditMode)
        {
            if (!(e.Item is IGridInsertItem))
            {
                var item = (GridEditableItem)e.Item;

                if (!(e.Item is IGridInsertItem))
                {
                    var testStatus = item.GetDataKeyValue("TestStatus") != null ? item.GetDataKeyValue("TestStatus").ToString() : null;
                    var cbTestStatus = (RadComboBox)item.FindControl("cbTestStatus");
                    cbTestStatus.SelectedValue = testStatus;
                }
            }
        }
    }

    protected void gridTestParticipants_ItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                try
                {
                    var editedItem = e.Item as GridEditableItem;

                    if (editedItem != null)
                    {
                        //update profile
                        Dbconn("Intranet");
                        _ocmd = new SqlCommand("pr_Infinity_Sav_TestScore", _oconn)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        var testScoresId = editedItem.GetDataKeyValue("TestScoresID").ToString();
                        var participantId = editedItem.GetDataKeyValue("ParticipantID").ToString();

                        var dpTestDate = (RadDatePicker)editedItem.FindControl("dpTestDate");
                        var txteditTestScore = (RadNumericTextBox)editedItem.FindControl("txteditTestScore");
                        var cbTestStatus = (RadComboBox)editedItem.FindControl("cbTestStatus");
                        bool? testStatus;
                        if (cbTestStatus.SelectedValue != string.Empty)
                        {
                            testStatus = Convert.ToBoolean(cbTestStatus.SelectedValue);
                        }
                        else
                        {
                            testStatus = null;
                        }

                        _ocmd.Parameters.AddWithValue("@TestScoresID", testScoresId);
                        _ocmd.Parameters.AddWithValue("@ParticipantID", participantId);
                        _ocmd.Parameters.AddWithValue("@TestDate", dpTestDate.SelectedDate);
                        _ocmd.Parameters.AddWithValue("@TestTypeID", cbLoadTest.SelectedValue);
                        _ocmd.Parameters.AddWithValue("@Score", txteditTestScore.Text);
                        _ocmd.Parameters.AddWithValue("@TestStatus", testStatus);

                        _ocmd.ExecuteReader();
                        _oconn.Close();

                        gridTestParticipants.Rebind();

                        _master.AlertMessage(true, "Congratulations! Record successfully updated.");
                    }

                }
                catch (Exception)
                {
                    _master.AlertMessage(false, "Error. Please try again.");
                }
            }
        }
    }

    #endregion

    #region JOURNAL

    private DataSet GetJournals(int classId, string dateFrom)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetJournal(classId, dateFrom);
        return ds;
    }

    protected void cbClassIDJournal_SelectedIndexChanged(object sender, EventArgs e)
    {
        gridJournals.Rebind();
    }

    protected void gridJournals_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var classId = string.IsNullOrEmpty(cbClassIDJournal.SelectedValue) ? 0 : Convert.ToInt32(cbClassIDJournal.SelectedValue);
        gridJournals.DataSource = GetJournals(classId, dtJournalDate.SelectedDate.ToString());
    }

    protected void gridJournals_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridEditableItem;

            var journalDate = dataItem.GetDataKeyValue("JournalDate").ToString();
            var successes = dataItem.GetDataKeyValue("Successes").ToString();
            var commonChallenges = dataItem.GetDataKeyValue("CommonChallenges").ToString();
            var debriefTopics = dataItem.GetDataKeyValue("DebriefTopics").ToString();
            var actionPlans = dataItem.GetDataKeyValue("ActionPlans").ToString();

            var btnSuccesses0 = (LinkButton)dataItem.FindControl("btnSuccesses0");
            var btnSuccesses1 = (LinkButton)dataItem.FindControl("btnSuccesses1");

            var btnCommonChallenges0 = (LinkButton)dataItem.FindControl("btnCommonChallenges0");
            var btnCommonChallenges1 = (LinkButton)dataItem.FindControl("btnCommonChallenges1");

            var btnDebriefTopics0 = (LinkButton)dataItem.FindControl("btnDebriefTopics0");
            var btnDebriefTopics1 = (LinkButton)dataItem.FindControl("btnDebriefTopics1");

            var btnActionPlans0 = (LinkButton)dataItem.FindControl("btnActionPlans0");
            var btnActionPlans1 = (LinkButton)dataItem.FindControl("btnActionPlans1");

            dataItem.ToolTip = journalDate;

            if (successes == "0")
            {
                btnSuccesses0.Visible = true;
                btnSuccesses1.Visible = false;
            }
            else
            {
                btnSuccesses0.Visible = false;
                btnSuccesses1.Visible = true;
            }


            if (commonChallenges == "0")
            {
                btnCommonChallenges0.Visible = true;
                btnCommonChallenges1.Visible = false;
            }
            else
            {
                btnCommonChallenges0.Visible = false;
                btnCommonChallenges1.Visible = true;
            }

            if (debriefTopics == "0")
            {
                btnDebriefTopics0.Visible = true;
                btnDebriefTopics1.Visible = false;
            }
            else
            {
                btnDebriefTopics0.Visible = false;
                btnDebriefTopics1.Visible = true;
            }

            if (actionPlans == "0")
            {
                btnActionPlans0.Visible = true;
                btnActionPlans1.Visible = false;
            }
            else
            {
                btnActionPlans0.Visible = false;
                btnActionPlans1.Visible = true;
            }
        }
    }

    protected void btnLoadJournals_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var journalDate = item.GetDataKeyValue("JournalDate");

        var successesContent = item.GetDataKeyValue("Successes_Content");
        var commonChallengesContent = item.GetDataKeyValue("CommonChallenges_Content");
        var debriefTopicsContent = item.GetDataKeyValue("DebriefTopics_Content");
        var actionPlansContent = item.GetDataKeyValue("ActionPlans_Content");

        txteditJournalDate.Text = journalDate.ToString();
        txteditJournalCategory.Text = string.Empty;

        txtJournalContent.Content = string.Empty;

        switch (btn.ID)
        {
            case "btnSuccesses1":
            case "btnSuccesses0":
                txteditJournalCategory.Text = "Successes";
                txtJournalContent.Content = successesContent.ToString();
                break;
            case "btnCommonChallenges1":
            case "btnCommonChallenges0":
                txteditJournalCategory.Text = "Common Challenges";
                txtJournalContent.Content = commonChallengesContent.ToString();
                break;
            case "btnDebriefTopics1":
            case "btnDebriefTopics0":
                txteditJournalCategory.Text = "Debrief Topics";
                txtJournalContent.Content = debriefTopicsContent.ToString();
                break;
            case "btnActionPlans1":
            case "btnActionPlans0":
                txteditJournalCategory.Text = "Action Plans";
                txtJournalContent.Content = actionPlansContent.ToString();
                break;
        }
    }

    protected void btnUpdateJournal_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            Dbconn("Intranet");

            _ocmd = new SqlCommand("pr_Infinity_Sav_Journal", _oconn)
            {
                CommandType = CommandType.StoredProcedure
            };
            _ocmd.Parameters.AddWithValue("@ClassID", cbClassIDJournal.SelectedValue);

            switch (txteditJournalCategory.Text)
            {
                case "Successes":
                    _ocmd.Parameters.AddWithValue("@CategoryID", "1");
                    break;
                case "Common Challenges":
                    _ocmd.Parameters.AddWithValue("@CategoryID", "2");
                    break;
                case "Debrief Topics":
                    _ocmd.Parameters.AddWithValue("@CategoryID", "3");
                    break;
                case "Action Plans":
                    _ocmd.Parameters.AddWithValue("@CategoryID", "4");
                    break;
            }

            _ocmd.Parameters.AddWithValue("@JournalDate", txteditJournalDate.Text);

            _ocmd.Parameters.AddWithValue("@JournalEntry", txtJournalContent.Content);
            _ocmd.ExecuteReader();
            _oconn.Close();

            gridJournals.Rebind();

            _master.AlertMessage(true, "Congratulations! Record successfully updated.");
            ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewJournal('1');", true);
        }
        catch (Exception)
        {
            _master.AlertMessage(false, "Error. Please try again.");
        }
    }

    protected void btnUpdateJournalCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseViewJournal('0');", true);

    }

    #endregion
}

