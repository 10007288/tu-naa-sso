﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="true" CodeFile="MyClass.aspx.cs" Title="Transcom University - My Class Page"
    Inherits="MyClass" %>

<%@ MasterType VirtualPath="MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">

            var from;

            function OnClientSelectedIndexChanged(sender, eventArgs) {

                eventArgs.get_item().set_checked(eventArgs.get_item().get_selected());

            }
            function OnClientItemChecked(sender, eventArgs) {

                eventArgs.get_item().set_selected(eventArgs.get_item().get_checked());


                var lstBoxControl;
                lstBoxControl = $find(sender.get_id());
                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                    }
                }
            }
            function OnTransferring(sender, eventArgs) {

                var lstBoxControl;
                lstBoxControl = $find(sender.get_id());

                var chkctl = document.getElementById("<%=chkAll.ClientID%>");
                chkctl.checked = false;

                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                        items.getItem(i).set_checked(false)
                    }
                }
            }
            function chk(sender, eventArgs) {

                var chkctl = document.getElementById("<%=chkAll.ClientID%>").checked;
                var lstBoxControl;
                lstBoxControl = $find("<%=lbUnassignedCourses.ClientID %>");
                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {
                    items.getItem(i).set_selected(chkctl);
                }
            }
            //----------------------------------------------------
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportViewParticipants") >= 0
                 || args.get_eventTarget().indexOf("btnExportMainViewParticipant") >= 0
                 || args.get_eventTarget().indexOf("btnExportClass") >= 0
                || args.get_eventTarget().indexOf("btnExportNonTUTest") >= 0
                || args.get_eventTarget().indexOf("btnExportTUTest") >= 0
               || args.get_eventTarget().indexOf("btnExportCourse") >= 0
               || args.get_eventTarget().indexOf("btnExportCertification") >= 0
                || args.get_eventTarget().indexOf("btnExportAttendance") >= 0
                 || args.get_eventTarget().indexOf("btnExportTestScore") >= 0
                 ) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                var scrollLeftOffset;
                var scrollTopOffset;

                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            var popUp;
            function PopUpShowing(sender, eventArgs) {
                popUp = eventArgs.get_popUp();
                var gridWidth = sender.get_element().offsetWidth;
                var gridHeight = sender.get_element().offsetHeight;
                var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
                var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
                popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
                popUp.style.top = ((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
            }
            function triggerValidation() {
                Page_ClientValidate();
            }
            function OnClientEntryAdding(sender, eventArgs) {
                setTimeout(function () { sender.closeDropDown(); }, 10);
            }

            //close radwindow
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);
                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }

            function View_Participants(sender, args) {
                setTimeout(function () { $find("<%=windowViewParticipants.ClientID %>").show(); }, 800);

            }
            function View_NonTUTests(sender, args) {
                //setTimeout(function () { $find("<%=windowViewNonTUTests.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowViewNonTUTests");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function View_Journal(sender, args) {
                // setTimeout(function () { $find("<%=windowViewJournal.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowViewJournal");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function OpenAssignForm(sender, args) {
                setTimeout(function () { $find("<%=AssignCourseWindow.ClientID %>").show(); }, 800);


            }
            function View_Courses(sender, args) {
                //setTimeout(function () { $find("<%=windowViewTUCourse.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowViewTUCourse");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function View_Certificates(sender, args) {
                //setTimeout(function () { $find("<%=windowViewCertification.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowViewCertification");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }

            function View_Tests(sender, args) {
                //setTimeout(function () { $find("<%=windowViewTUTest.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowViewTUTest");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }

            //AssignCourse
            function CloseAssignForm(sender, args) {

                var window = $find('<%=AssignCourseWindow.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            //AssignCourse
            function CloseAssignNonTUForm(from1) {
                from = from1;
                var window = $find('<%=windowViewNonTUTests.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }

            //View Course
            function CloseViewCourse(from1) {
                from = from1;
                var window = $find('<%=windowViewTUCourse.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            //View Test
            function Close_windowViewTest(from1) {
                from = from1;
                var window = $find('<%=windowViewTUTest.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            //View Certificate
            function CloseViewCertificate(from1) {
                from = from1;
                var window = $find('<%=windowViewCertification.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            //View Journal
            function CloseViewJournal(from1) {
                from = from1;
                var window = $find('<%=windowViewJournal.ClientID %>');

                setTimeout(function () {
                    window.close();
                }, 100);
            }

            //JAY NEW
            function View_windowUpdateAttendance(sender, args) {
                //setTimeout(function () { $find("<%=windowUpdateAttendance.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowUpdateAttendance");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function Close_windowUpdateAttendance(from1) {
                from = from1;
                var window = $find('<%=windowUpdateAttendance.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            function View_windowRemoveAttendance(sender, args) {
                //setTimeout(function () { $find("<%=windowRemoveAttendance.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowRemoveAttendance");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function Close_windowRemoveAttendance(from1) {
                from = from1;
                var window = $find('<%=windowRemoveAttendance.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            function View_windowEditParticipant(from1) {
                from = from1;
                //setTimeout(function () { $find("<%=windowEditParticipant.ClientID %>").show(); }, 800);
                var oWindow = window.radopen(null, "windowEditParticipant");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function Close_windowEditParticipant(from1) {
                from = from1;
                var window = $find('<%=windowEditParticipant.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);

            }


            function GetCoursesWithPrerequisite() {

                lstBoxControl = $find($("[id$=lbAssignedCourses]").attr("id"));
                var courseIDs = ''
                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {
                    courseIDs = courseIDs + items.getItem(i).get_value() + ' ';
                }

                //                alert(courseIDs);
                if (courseIDs.trim() == '') {
                    return true;
                }

                $.ajax({
                    type: "POST",
                    url: "ServiceContainer.aspx/GetCoursesWithPrerequisite",
                    data: '{courseIds: "' + courseIDs + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function OnSuccess(response) {
                        var msg = response.d.MSG;
                        if (msg.trim() != '') {

                            return radconfirm(text, callbackFn, 350, 180, null, "Course with prerequisite");
                        }
                        else {
                            $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                        }
                    },
                    error: function (response) {
                        alert('Error occured cannot take exam.');
                    }
                });
            }
            function chkprerequisite() {


                if ($("input:hidden[id$=ctrPrereq]").val() != "0") {
                    var text = "Course/s selected have prerequisites.</br>Do you want to continue?";
                    return radconfirm(text, callbackFn, 350, 180, null, "Course with Prerequisite");
                }
                else {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function callbackFn(args) {
                if (args == true) {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function confirmCallBackFn(arg) {
                if (arg == true) {
                    chkprerequisite();
                }
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Panel2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdEditNonTUTests" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridJournals">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="gridJournals" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtJournalContent" />
                    <telerik:AjaxUpdatedControl ControlID="txteditJournalDate" />
                    <telerik:AjaxUpdatedControl ControlID="txteditJournalCategory" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateJournal">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridJournals" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Panel5">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel5" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Panel6">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel6" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR EDIT ASSIGN COURSE--%>
            <telerik:AjaxSetting AjaxControlID="btnEditAssign">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel5" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR EDIT ASSIGN TEST--%>
            <telerik:AjaxSetting AjaxControlID="Panel7">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel7" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSelectTest">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel7" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR EDIT Journal--%>
            <telerik:AjaxSetting AjaxControlID="Panel8">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="gridJournals" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtJournalContent" />
                    <telerik:AjaxUpdatedControl ControlID="btnUpdateJournalCancel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="btnSuccesses0">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSuccesses1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCommonChallenges0">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCommonChallenges1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDebriefTopics0">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDebriefTopics1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnActionPlans0">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnActionPlans1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="windowViewJournal" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel8" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--END EDIT ASSIGN COURSE--%>
            <telerik:AjaxSetting AjaxControlID="tsCoursesOnCareerPath">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="mpCoursesOnCareerPath" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddCareerCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tsCoursesOnCareerPath" />
                    <telerik:AjaxUpdatedControl ControlID="mpCoursesOnCareerPath" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCategory_onEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="hidCategoryId" />
                    <telerik:AjaxUpdatedControl ControlID="ddtParentSubcategory_onEdit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridClass">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridClass" />
                    <telerik:AjaxUpdatedControl ControlID="gridViewParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="lblClassId" />
                    <telerik:AjaxUpdatedControl ControlID="grdEditNonTUTests" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdEditNonTUTests">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdEditNonTUTests" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridAttendance">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAttendance" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddClass">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="btnAddClass" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnTestEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdEditNonTUTests" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="grdEditNonTUTests">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="grdEditNonTUTests" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAssign">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel4" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSelectTUTest">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel4" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR ASSIGN COURSE--%>
            <telerik:AjaxSetting AjaxControlID="lbUnassignedCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbAssignTo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="rgAssignedCourses" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END ASSIGN COURSE--%>
            <%--JAY NEW--%>
            <%--ATTENDANCE--%>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbUpdateClassAttendance">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radioUpdateApply">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateAttendance">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAttendance" />
                    <telerik:AjaxUpdatedControl ControlID="updateAttendanceFields" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbAttendanceRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rbRemoveAttendanceApply">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnResetAttendance">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAttendance" />
                    <telerik:AjaxUpdatedControl ControlID="removeAttendanceFields" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--CLASS TAB--%>
            <telerik:AjaxSetting AjaxControlID="rblStatus_editParticipant">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbReason_editParticipant">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbDesc_editParticipant">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridViewParticipants">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridViewParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="tblEditParticipant" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridParticipants">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="tblEditParticipant" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateParticipant">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridViewParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="gridParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--JAY NEW - END--%>
            <telerik:AjaxSetting AjaxControlID="btnAddParticipants">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGetCIMInfo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateTest">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridTestParticipants">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridTestParticipants" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAssignCourseAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnTestEditUpdate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditCert">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddTest">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <%--GRID CLASS WINDOWS--%>
            <telerik:RadWindow ID="windowViewParticipants" runat="server" Width="800px" Title="Participants"
                Height="555px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div style="vertical-align: top">
                        <table width="100%" style="padding: 4px">
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblClassId" Text="Class Id: " />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportViewParticipants" CssClass="linkBtn"
                                            OnClick="btnExportViewParticipants_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Participant records to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="gridViewParticipants" AutoGenerateColumns="False"
                            GridLines="None" AllowFilteringByColumn="True" EnableLinqExpressions="False"
                            CssClass="RadGrid2_detailtable" Skin="Metro" AllowPaging="True" PageSize="15"
                            AllowSorting="True" OnNeedDataSource="gridViewParticipants_NeedDataSource" OnItemDataBound="gridViewParticipants_ItemDataBound"
                            BorderStyle="None">
                            <GroupingSettings CaseSensitive="False" />
                            <FilterItemStyle HorizontalAlign="Center" />
                            <MasterTableView ShowHeader="True" AutoGenerateColumns="False" EditMode="PopUp" NoMasterRecordsText="Participants list is empty."
                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                DataKeyNames="ParticipantName,cimnumber,ClassID" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                GridLines="None">
                                <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btnEditParticipants" CssClass="linkBtn" OnClientClick="View_windowEditParticipant('1')"
                                                OnClick="btnEditParticipants_Click" ToolTip="Edit"><img style="border:0; padding-left: 3px" alt="" src="Images/editpen2.png"/></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn UniqueName="ParticipantName" HeaderText="Participant Name"
                                        DataField="ParticipantName" AllowFiltering="True" FilterControlWidth="150" AutoPostBackOnFilter="True"
                                        ShowFilterIcon="False" CurrentFilterFunction="Contains" />
                                    <telerik:GridBoundColumn UniqueName="ParticipantSite" HeaderText="Site" DataField="ParticipantSite"
                                        AllowFiltering="True" FilterControlWidth="100" AutoPostBackOnFilter="True" ShowFilterIcon="False"
                                        CurrentFilterFunction="Contains" />
                                    <telerik:GridBoundColumn UniqueName="DateJoined" HeaderText="Date Joined" DataField="DateJoined"
                                        AllowFiltering="false" HeaderStyle-Width="100px" />
                                    <telerik:GridBoundColumn UniqueName="ParticipantStatus" HeaderText="Status" DataField="ParticipantStatus"
                                        AllowFiltering="false" />
                                    <telerik:GridBoundColumn UniqueName="CertificationStatus" HeaderText="Certification Status"
                                        DataField="CertificationStatus" AllowFiltering="False" />
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowEditParticipant" runat="server" Width="850px" Title="Edit Participant"
                Height="547px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="tblEditParticipant" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:ValidationSummary ID="vs_EditParticipant" ShowMessageBox="False" ShowSummary="True"
                                    DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                    runat="server" ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblCimNo_editParticipant" Text="-" Font-Bold="True"
                                    Visible="False" />
                                <asp:Label runat="server" ID="lblParticipantId_editParticipant" Text="-" Font-Bold="True"
                                    Visible="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblName_editParticipant" Text="-" Font-Bold="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Site
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="lblSite_editParticipant" Text="-" Font-Bold="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Class ID"
                                    ControlToValidate="cbClassId_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" />
                                Class ID
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbClassId_editParticipant" runat="server" EmptyMessage="- class id -"
                                    Width="100" MaxHeight="300" DataSourceID="dsClassID1" DataValueField="ClassID"
                                    DataTextField="ClassID" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Date Joined"
                                    ControlToValidate="dpDateJoined_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" />
                                Date Joined
                            </td>
                            <td class="tblfield">
                                <telerik:RadDatePicker ID="dpDateJoined_editParticipant" runat="server" EnableTyping="False"
                                    ShowPopupOnFocus="True" Width="140">
                                    <DateInput ID="DateInput16" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Status"
                                    ControlToValidate="rblStatus_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    CssClass="displayerror" ValidationGroup="editParticipant" EnableClientScript="False"
                                    Text="*" />
                                Status
                            </td>
                            <td class="tblfield">
                                <asp:RadioButtonList ID="rblStatus_editParticipant" runat="server" AutoPostBack="true"
                                    RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio" OnSelectedIndexChanged="rblStatusEditParticipant_SelectedIndexChanged"
                                    Font-Size="8pt">
                                    <asp:ListItem Value="Active" Text="Active" Selected="true" />
                                    <asp:ListItem Value="Inactive" Text="Inactive" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ID="rfvReason_editParticipant" ErrorMessage="Reason"
                                    ControlToValidate="cbReason_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" Enabled="false" />
                                Reason
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbReason_editParticipant" runat="server" EmptyMessage="- reason -"
                                    DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="id" AutoPostBack="true"
                                    DataTextField="details" DataSourceID="dsParticipantReason" Enabled="false" OnSelectedIndexChanged="cbReasonEditParticipant_SelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ID="rfvDesc_editParticipant" ErrorMessage="Description"
                                    ControlToValidate="cbDesc_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" Enabled="false" />
                                Description
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbDesc_editParticipant" runat="server" EmptyMessage="- description -"
                                    DropDownAutoWidth="Enabled" MaxHeight="300" Enabled="false" DataValueField="id"
                                    AutoPostBack="true" DataTextField="details" OnSelectedIndexChanged="cbDescEditParticipant_SelectedIndexChanged" />
                                <asp:SqlDataSource ID="dsDesc_editParticipant" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                    SelectCommand="pr_Infinity_Lkp_ParticipantReason" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="0" Name="ReasonID" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <telerik:RadTextBox ID="txtDescOther_editParticipant" runat="server" EmptyMessage="- other description -"
                                    MaxLength="255" Width="250px" Visible="false" SelectionOnFocus="SelectAll" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescOther_editParticipant" ErrorMessage="Other Description"
                                    ControlToValidate="txtDescOther_editParticipant" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="editParticipant" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" Enabled="false" />
                                <asp:SqlDataSource ID="dsDescOther_editParticipant" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                    SelectCommand="select count(*) FROM tbl_Infinity_lkp_ParticipantDesc where [descid] = @DescID and allowothers = 0">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="cbDesc_editParticipant" DefaultValue="0" Name="DescID"
                                            PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Certified?
                            </td>
                            <td class="tblfield">
                                <asp:CheckBox runat="server" ID="chkCertified_editParticipant" Text="Yes" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Comments
                            </td>
                            <td class="tblfield">
                                <telerik:RadEditor ID="txtComment_editParticipant" runat="server" EnableResize="false"
                                    ToolsFile="~/App_Data/Editor_LimitedTools.xml" AllowScripts="false" Width="570px"
                                    Height="200px" EmptyMessage="- course description -" BorderColor="LightGray"
                                    BorderStyle="Solid" BorderWidth="1px">
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                    </CssFiles>
                                </telerik:RadEditor>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnUpdateParticipant" Text="Update" CssClass="linkBtn"
                                    ValidationGroup="editParticipant" OnClick="btnUpdateParticipant_Click" />
                                |
                                <asp:LinkButton runat="server" ID="btnUpdateParticipantCancel" Text="Cancel" CssClass="linkBtn"
                                    CausesValidation="False" OnClick="btnUpdateParticipantCancel_Click" />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewNonTUTests" runat="server" Width="650px" Title="Non-TU Tests"
                Height="480px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server" BackColor="White" CssClass="childPanel">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblNonTUTestClassID" Text="Class ID: " />
                            <hr />
                        </div>
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0 10px 8px 10px">
                            <tr>
                                <td>
                                    <label>
                                        Test Name:
                                    </label>
                                    <telerik:RadTextBox ID="txtEditTestName" runat="server" EmptyMessage="- test name -"
                                        MaxLength="200" />
                                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Test Name" ControlToValidate="txtEditTestName"
                                        SetFocusOnError="True" Display="Dynamic" ValidationGroup="editTest" CssClass="displayerror"
                                        EnableClientScript="False" Text="*" />
                                    <asp:LinkButton runat="server" ID="btnTestEdit" Text="Add to List >" ValidationGroup="editTest"
                                        CssClass="linkBtn" OnClick="btnTestEdit_Click" Font-Size="8.5" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportNonTUTest" CssClass="linkBtn" OnClick="btnExportNonTUTest_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Non-TU Test list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="grdEditNonTUTests" AutoGenerateColumns="False"
                            AllowPaging="true" GridLines="None" CssClass="RadGrid2_detailtable" Skin="Metro"
                            OnItemCommand="grdEditNonTUTests_ItemCommand" OnNeedDataSource="grdEditNonTUTests_NeedDataSource"
                            BorderStyle="None" PageSize="10" Height="320">
                            <FilterItemStyle HorizontalAlign="Center" />
                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                            <MasterTableView ShowHeader="true" DataKeyNames="TestTypeID" AutoGenerateColumns="False"
                                EnableNoRecordsTemplate="True" NoMasterRecordsText="No test added." ShowHeadersWhenNoRecords="True"
                                TableLayout="Auto" EditMode="InPlace" BorderWidth="0" BorderStyle="None" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                        HeaderStyle-Width="70" EditImageUrl="Images/editpen2.png">
                                    </telerik:GridEditCommandColumn>
                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridTemplateColumn UniqueName="TestName" HeaderText="Test Name" DataField="TestName"
                                        SortExpression="TestName" EditFormHeaderTextFormat="Test Name" ShowFilterIcon="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTestName" Text='<%# Eval("TestName") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadTextBox ID="txtEditTestName" runat="server" EmptyMessage="- test name -"
                                                Text='<%# Bind("TestName") %>' MaxLength="200" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <div style="padding-left: 10px">
                            <br />
                            <asp:LinkButton runat="server" ID="btnTestEditUpdate" Text="Update" CssClass="linkBtn"
                                OnClick="btnTestEditUpdate_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnTestEditCancel" Text="Cancel" CssClass="linkBtn"
                                CausesValidation="False" OnClick="btnTestEditCancel_Click" />
                            <br />
                            <br />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewTUTest" runat="server" Width="800px" Height="480"
                Title="TU Tests" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel7" runat="server" BackColor="White" CssClass="childPanel">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblTUTestClassID" Text="Class ID: " />
                            <hr />
                        </div>
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0 10px 8px 10px">
                            <tr>
                                <td>
                                    <label>
                                        Test Categories:
                                    </label>
                                    <telerik:RadComboBox ID="cbTestCategoryAssign" DataTextField="Campaign" DataValueField="CampaignID"
                                        runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="cbTestCategoryAssignSelectedIndexChanged"
                                        CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                    <telerik:RadDropDownTree runat="server" ID="ddtTestSubcategoryAssign" DefaultMessage="- select subcategory -"
                                        DefaultValue="0" DataTextField="Campaign" DataValueField="CampaignID" DataFieldParentID="ParentCampaignId"
                                        TextMode="FullPath" AutoPostBack="True" OnClientEntryAdding="OnClientEntryAdding"
                                        Skin="Default" OnEntryAdded="ddtTestSubcategoryAssign_EntryAdded" OnEntryRemoved="ddtTestSubcategoryAssign_EntryRemoved">
                                        <DropDownSettings Width="400px" Height="250px" />
                                    </telerik:RadDropDownTree>
                                    <asp:LinkButton ID="btnSelectTest" runat="server" Text="Choose Test/s" ToolTip="Assign"
                                        CssClass="linkBtn" OnClick="btnSelectTest_Click" ValidationGroup="vgAssign" Visible="false"
                                        Font-Size="8.5" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom" align="right">
                                        <asp:LinkButton runat="server" ID="btnExportTUTest" CssClass="linkBtn" OnClick="btnExportTUTest_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export TU Test list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="rgEditAssignedTests" AutoGenerateColumns="False"
                            AllowPaging="True" GridLines="None" CssClass="RadGrid2_detailtable" Skin="Metro"
                            OnItemCommand="rgEditAssignedTests_ItemCommand" OnNeedDataSource="rgEditAssignedTests_NeedDataSource"
                            BorderStyle="None" PageSize="10" Height="320">
                            <FilterItemStyle HorizontalAlign="Center" />
                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EnableNoRecordsTemplate="True"
                                NoMasterRecordsText="No assigned test." ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                EditMode="InPlace" DataKeyNames="CourseID,CourseAssignmentID" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                GridLines="None">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                        AllowFiltering="False" ReadOnly="True">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                        Visible="false" SortExpression="DueDate" EditFormHeaderTextFormat="Due Date">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTestDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadDatePicker ID="dpTestDueDateEdit" runat="server" EnableTyping="False"
                                                SelectedDate='<%# Bind("DueDate") %>' ShowPopupOnFocus="True" Width="140">
                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                            </telerik:RadDatePicker>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <div style="padding-left: 10px">
                            <br />
                            <asp:LinkButton runat="server" ID="btnUpdateTest" Text="Update" CssClass="linkBtn"
                                OnClick="btnUpdateTest_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnUpdateTestCancel" Text="Cancel" CssClass="linkBtn"
                                CausesValidation="False" OnClick="btnUpdateTestCancel_Click" />
                            <br />
                            <br />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewTUCourse" runat="server" Width="800" Height="480"
                Title="Courses" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel5" runat="server" BackColor="White" CssClass="childPanel">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblTUCourseClassID" Text="Class ID: " />
                            <hr />
                        </div>
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0 10px 8px 10px">
                            <tr>
                                <td>
                                    <label>
                                        Assignment Filters:
                                    </label>
                                    <telerik:RadComboBox ID="cbEditCategoryAssign" DataTextField="Category" DataValueField="CategoryID"
                                        runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbEditCategoryAssignSelectedIndexChanged"
                                        CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                    <telerik:RadDropDownTree runat="server" ID="ddtEditSubcategoryAssign" DefaultMessage="- select subcategory -"
                                        DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                        DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" AutoPostBack="True"
                                        OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="dtEditSubcategoryAssign_EntryAdded"
                                        OnEntryRemoved="dtEditSubcategoryAssign_EntryRemoved">
                                        <DropDownSettings Width="400px" Height="250px" />
                                    </telerik:RadDropDownTree>
                                    <asp:LinkButton ID="btnEditAssign" runat="server" Text="Choose Course/s" ToolTip="Assign"
                                        CssClass="linkBtn" OnClick="btnEditAssign_Click" ValidationGroup="vgAssign" Visible="false"
                                        Font-Size="8.5" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom" align="right">
                                        <asp:LinkButton runat="server" ID="btnExportCourse" CssClass="linkBtn" OnClick="btnExportCourse_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Course list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="rgEditAssignedCourses" AutoGenerateColumns="False"
                            AllowPaging="True" GridLines="None" CssClass="RadGrid2_detailtable" Skin="Metro"
                            OnItemCommand="rgEditAssignedCourses_ItemCommand" OnNeedDataSource="rgEditAssignedCourses_NeedDataSource"
                            BorderStyle="None" PageSize="10" Height="320">
                            <FilterItemStyle HorizontalAlign="Center" />
                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EnableNoRecordsTemplate="True"
                                NoMasterRecordsText="No assigned course" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                EditMode="InPlace" DataKeyNames="CourseID,CourseAssignmentID" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                        HeaderStyle-Width="70" EditImageUrl="Images/editpen2.png">
                                    </telerik:GridEditCommandColumn>
                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                        AllowFiltering="False" ReadOnly="True">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                        SortExpression="DueDate" EditFormHeaderTextFormat="Due Date">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadDatePicker ID="dpDueDateEdit" runat="server" EnableTyping="False" SelectedDate='<%# Bind("DueDate") %>'
                                                ShowPopupOnFocus="True" Width="140">
                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                            </telerik:RadDatePicker>
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <div style="padding-left: 10px">
                            <br />
                            <asp:LinkButton runat="server" ID="btnUpdateCourse" Text="Update" CssClass="linkBtn"
                                OnClick="btnUpdateCourse_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnUpdateCourseCancel" Text="Cancel" CssClass="linkBtn"
                                CausesValidation="False" OnClick="btnUpdateCourseCancel_Click" />
                            <br />
                            <br />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewCertification" runat="server" Title="Certification"
                Width="800" Height="480" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel6" runat="server">
                        <div style="padding: 4px" align="center">
                            <asp:Label runat="server" ID="lblCertificationClassID" Text="Class ID: " />
                            <hr />
                        </div>
                        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0 10px 8px 10px">
                            <tr>
                                <td>
                                    <label>
                                        Select:
                                    </label>
                                    <telerik:RadComboBox ID="cbEditCert" runat="server" EmptyMessage="- program -" DataTextField="Client"
                                        DataValueField="ClientID" DataSourceID="dsTrainingClient" Width="100" Height="300"
                                        AutoPostBack="true" OnSelectedIndexChanged="cbEditCert_SelectedIndexChanged" />
                                    <telerik:RadComboBox ID="ddeditTrainingTypeCert" runat="server" EmptyMessage="- training type -"
                                        DropDownAutoWidth="Enabled" Width="120" MaxHeight="300" DataSourceID="dsTrainingTYpe"
                                        DataValueField="TrainingTypeID" DataTextField="TrainingType" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddeditTrainingTypeCert_SelectedIndexChanged" Enabled="false" />
                                    <telerik:RadComboBox ID="cbeditCertName" runat="server" EmptyMessage="- certification -"
                                        DropDownAutoWidth="Enabled" Width="120" MaxHeight="300px" DataTextField="ClassName"
                                        DataValueField="ID" DataSourceID="dsEditClassNames" EnableViewState="False" OnSelectedIndexChanged="cbeditCertName_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Certification"
                                        ControlToValidate="cbeditCertName" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                        ValidationGroup="editCertification" Text="*" EnableClientScript="False" />
                                    <asp:LinkButton runat="server" ID="btnEditCert" Text="Add to List >" CssClass="linkBtn"
                                        OnClick="btnEditCert_Click" Font-Size="8.5" ValidationGroup="editCertification" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom" align="right">
                                        <asp:LinkButton runat="server" ID="btnExportCertification" CssClass="linkBtn" OnClick="btnExportCertification_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Certificate list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="grdEditCert" AutoGenerateColumns="False" AllowPaging="True"
                            GridLines="None" CssClass="RadGrid2_detailtable" Skin="Metro" OnItemCommand="grdEditCert_ItemCommand"
                            OnNeedDataSource="grdEditCert_NeedDataSource" BorderStyle="None" PageSize="10"
                            Height="320">
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                            <FilterItemStyle HorizontalAlign="Center" />
                            <MasterTableView ShowHeader="true" DataKeyNames="CertificationID,ClassID,ClientID,TrainingTypeID,ClassNameID"
                                AutoGenerateColumns="False" EnableNoRecordsTemplate="True" NoMasterRecordsText="No certification added."
                                ShowHeadersWhenNoRecords="True" TableLayout="Auto" EditMode="InPlace" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="gridHeaderWindow" />
                                <Columns>
                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                        ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                    </telerik:GridButtonColumn>
                                    <telerik:GridTemplateColumn UniqueName="clientname" HeaderText="Program" DataField="clientname"
                                        SortExpression="clientname" EditFormHeaderTextFormat="Program" ShowFilterIcon="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblclientname" Text='<%# Eval("clientname") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="trainingtype" HeaderText="Training Type"
                                        DataField="trainingtype" SortExpression="trainingtype" EditFormHeaderTextFormat="Training Type"
                                        ShowFilterIcon="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbltrainingtype" Text='<%# Eval("trainingtype") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="classname" HeaderText="Certification" DataField="classname"
                                        SortExpression="classname" EditFormHeaderTextFormat="Client" ShowFilterIcon="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblclassname" Text='<%# Eval("classname") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <div style="padding-left: 10px">
                            <br />
                            <asp:LinkButton runat="server" ID="btnUpdateCert" Text="Update" CssClass="linkBtn"
                                OnClick="btnUpdateCert_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnUpdateCertCancel" Text="Cancel" CssClass="linkBtn"
                                CausesValidation="False" OnClick="btnUpdateCertCancel_Click" />
                            <br />
                            <br />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <%--END GRID CLASS WINDOWS--%>
            <telerik:RadWindow ID="AssignCourseWindow" runat="server" Title="Select Course/s"
                Width="700px" Height="530px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                ReloadOnShow="true" VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel4" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnAssignCourseAdd">
                        <div class="insidecontainer2">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="background: #e4e4e4; height: 23px; border-style: solid; border-color: #C6C7D2;
                                            border-width: 1px 1px 0 1px">
                                            <asp:CheckBox runat="server" ID="chkAll" Text="All" Checked="false" OnClick="JavaScript:chk();"
                                                CssClass="lbChk" />
                                        </div>
                                        <telerik:RadListBox ID="lbUnassignedCourses" runat="server" Height="200px" Width="100%"
                                            EmptyMessage="No Unassigned Course" TransferToID="lbAssignedCourses" CheckBoxes="true"
                                            AllowTransfer="true" AutoPostBackOnTransfer="true" TransferMode="Move" SelectionMode="Multiple"
                                            OnItemDataBound="lbUnassignedCourses_ItemDataBound" OnTransferred="lbUnassignedCourses_Transferred"
                                            OnInserted="lbUnassignedCourses_Inserted" OnClientItemChecked="OnClientItemChecked"
                                            OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" OnClientTransferring="OnTransferring"
                                            DataKeyField="Prerequisite">
                                            <ButtonSettings Position="Bottom" HorizontalAlign="Center" RenderButtonText="true" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourseUnassigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadListBox ID="lbAssignedCourses" runat="server" Height="200px" Width="100%"
                                            AutoPostBackOnTransfer="true" EmptyMessage="No Course Selected" CheckBoxes="false">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                            <HeaderTemplate>
                                                <table width="100%" id="tbllbAssignedCourses1" runat="server">
                                                    <tr>
                                                        <td width="60%" align="center" id="td1" runat="server">
                                                            <asp:Label ID="lblCourseName" runat="server" Text="Course Name" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                        <td width="25%" align="center" id="td2" runat="server">
                                                            <asp:Label ID="lblDueDate" runat="server" Text="Due Date" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                        <td width="15%" align="left" id="td3" runat="server">
                                                            <asp:Label ID="lblPrerequisite" runat="server" Text="Prerequisite?" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%" runat="server" id="tbllbAssignedCourses2">
                                                    <tr>
                                                        <td width="60%" align="center" id="td1" runat="server">
                                                            <asp:Label ID="lblCourseAssigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                                        </td>
                                                        <td width="30%" align="center" id="td2" runat="server">
                                                            <telerik:RadDatePicker ID="dpCourseAssigned" runat="server" EnableTyping="False"
                                                                SelectedDate='<%# DateTime.Today.AddMonths(1) %>' ShowPopupOnFocus="True" Width="140">
                                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td width="10%" runat="server" align="center" id="td3">
                                                            <asp:Label ID="lblAssignedPrerequisite" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Prerequisite\"]")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:LinkButton runat="server" ID="btnAssignCourseAdd" Text="Add" CssClass="linkBtn"
                                            OnClientClick="radconfirm('Are you sure you want to add this collection?', confirmCallBackFn, 350, 180, null, 'Close Window'); return false;" />
                                        <asp:LinkButton runat="server" ID="btnAssignTestAdd" Text="Add" CssClass="linkBtn"
                                            OnClick="btnAssignTestAdd_Click" OnClientClick="radconfirm('Are you sure you want to add this collection?', confirmCallBackFn, 350, 180, null, 'Add Collection'); return false;" />
                                        |
                                        <asp:LinkButton runat="server" ID="btnAssignCourseCancel" Text="Cancel" CssClass="linkBtn"
                                            CausesValidation="False" OnClick="btnAssignCourseCancel_Click" OnClientClick="radconfirm('Are you sure you want to close this window?', confirmCallBackFn, 350, 180, null, 'Close Window'); return false;" />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowViewJournal" runat="server" Width="840px" Title="Update Journal"
                Height="555px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel8" runat="server" BackColor="White" CssClass="childPanel">
                        <table width="100%" class="tabledefault">
                            <tr>
                                <td class="tblLabel">
                                </td>
                                <td class="tblfield">
                                </td>
                            </tr>
                            <tr>
                                <td class="tblLabel">
                                    Journal Date
                                </td>
                                <td class="tblfield">
                                    <asp:Label ID="txteditJournalDate" Text="Date" runat="server" Font-Bold="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tblLabel">
                                    Journal Category
                                </td>
                                <td class="tblfield">
                                    <asp:Label ID="txteditJournalCategory" Text="Category" runat="server" Font-Bold="True" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tblLabel">
                                    Journal Content
                                </td>
                                <td class="tblfield">
                                    <telerik:RadEditor ID="txtJournalContent" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                        AllowScripts="false" Height="400px" Width="570px" EmptyMessage="- course description -"
                                        BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <CssFiles>
                                            <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                        </CssFiles>
                                    </telerik:RadEditor>
                                </td>
                            </tr>
                            <tr>
                                <td class="tblLabel">
                                </td>
                                <td class="tblfield">
                                    <br />
                                    <asp:LinkButton runat="server" ID="btnUpdateJournal" Text="Update" CssClass="linkBtn"
                                        OnClick="btnUpdateJournal_Click" />
                                    |
                                    <asp:LinkButton runat="server" ID="btnUpdateJournalCancel" Text="Cancel" CssClass="linkBtn"
                                        OnClick="btnUpdateJournalCancel_Click" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowUpdateAttendance" runat="server" Width="550px" Title="Batch Update"
                Height="312px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="updateAttendanceFields" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Class ID" ControlToValidate="cbUpdateClassAttendance"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="addAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Class ID
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbUpdateClassAttendance" runat="server" EmptyMessage="- class id -"
                                    Width="100" MaxHeight="300" DataSourceID="dsClassID" DataValueField="ClassID"
                                    DataTextField="ClassID" OnSelectedIndexChanged="radioUpdateApply_SelectedIndexChanged"
                                    AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="For Date From" ControlToValidate="dpUpdateAttendanceFrom"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="addAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Date From
                            </td>
                            <td class="tblfield">
                                <telerik:RadDatePicker ID="dpUpdateAttendanceFrom" runat="server" EnableTyping="False"
                                    ShowPopupOnFocus="True" Width="140">
                                    <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="For Date To" ControlToValidate="dpUpdateAttendanceTo"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="addAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Date To
                            </td>
                            <td class="tblfield">
                                <telerik:RadDatePicker ID="dpUpdateAttendanceTo" runat="server" EnableTyping="False"
                                    ShowPopupOnFocus="True" Width="140">
                                    <DateInput ID="DateInput4" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Attendance Status" ControlToValidate="cbUpdateAttendanceStatus"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="addAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Attendance Status
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbUpdateAttendanceStatus" runat="server" EmptyMessage="- attendance status -"
                                    DataSourceID="dsAttendanceStatus" DataValueField="AttendanceStatusID" DataTextField="AttendanceStatus"
                                    DropDownAutoWidth="Enabled" MaxHeight="300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Training Hours" ControlToValidate="txtUpdateTraininigHours"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="addAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Training Hours
                            </td>
                            <td class="tblfield">
                                <telerik:RadNumericTextBox ID="txtUpdateTraininigHours" runat="server" EmptyMessage="- training hours -"
                                    MinValue="0" MaxValue="99999999" MaxLength="8" SelectionOnFocus="SelectAll">
                                    <NumberFormat GroupSeparator="" />
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:RadioButtonList ID="radioUpdateApply" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow" CssClass="radio" Font-Size="8pt" OnSelectedIndexChanged="radioUpdateApply_SelectedIndexChanged">
                                    <asp:ListItem Value="ApplyToAll" Text="Apply to All" Selected="true" />
                                    <asp:ListItem Value="ApplyTo" Text="Apply To" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator ID="rfvUpdateParticipants" runat="server" ErrorMessage="Participants"
                                    ControlToValidate="cbUpdateAttendanceParticipants" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="addAttendance" CssClass="displayerror" EnableClientScript="False"
                                    Text="select participant *" Enabled="false" />
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbUpdateAttendanceParticipants" runat="server" EmptyMessage="- select participant -"
                                    DataValueField="ParticipantID" DataTextField="ParticipantName" DropDownAutoWidth="Enabled"
                                    MaxHeight="300" Enabled="false" CheckBoxes="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnUpdateAttendance" Text="Update" CssClass="linkBtn"
                                    OnClick="btnUpdateAttendance_Click" ValidationGroup="addAttendance" />
                                |
                                <asp:LinkButton runat="server" ID="btnUpdateAttendanceCancel" Text="Cancel" CssClass="linkBtn"
                                    CausesValidation="False" OnClick="btnUpdateAttendanceCancel_Click" />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowRemoveAttendance" runat="server" Width="550px" Title="Batch Remove"
                Height="250px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="removeAttendanceFields" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Class ID" ControlToValidate="cbAttendanceRemove"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="removeAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Class ID
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbAttendanceRemove" runat="server" EmptyMessage="- class id -"
                                    Width="100" MaxHeight="300" DataSourceID="dsClassID" DataValueField="ClassID"
                                    DataTextField="ClassID" OnSelectedIndexChanged="rbRemoveAttendanceApply_SelectedIndexChanged"
                                    AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="For Date From" ControlToValidate="dpRemoveDateFrom"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="removeAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Date From
                            </td>
                            <td class="tblfield">
                                <telerik:RadDatePicker ID="dpRemoveDateFrom" runat="server" EnableTyping="False"
                                    ShowPopupOnFocus="True" Width="140">
                                    <DateInput ID="DateInput5" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="For Date To" ControlToValidate="dpRemoveDateTo"
                                    SetFocusOnError="True" Display="Dynamic" ValidationGroup="removeAttendance" CssClass="displayerror"
                                    EnableClientScript="False" Text="*" />
                                Date To
                            </td>
                            <td class="tblfield">
                                <telerik:RadDatePicker ID="dpRemoveDateTo" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                    Width="140">
                                    <DateInput ID="DateInput6" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                </telerik:RadDatePicker>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:RadioButtonList ID="rbRemoveAttendanceApply" runat="server" AutoPostBack="true"
                                    RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio" Font-Size="8pt"
                                    OnSelectedIndexChanged="rbRemoveAttendanceApply_SelectedIndexChanged">
                                    <asp:ListItem Value="ApplyToAll" Text="Apply to All" Selected="true" />
                                    <asp:ListItem Value="ApplyTo" Text="Apply To" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                <asp:RequiredFieldValidator ID="rfvRemoveParticipants" runat="server" ErrorMessage="Participants"
                                    ControlToValidate="cbRemoveParticipants" SetFocusOnError="True" Display="Dynamic"
                                    ValidationGroup="removeAttendance" CssClass="displayerror" EnableClientScript="False"
                                    Text="*" Enabled="false" />
                            </td>
                            <td class="tblfield">
                                <telerik:RadComboBox ID="cbRemoveParticipants" runat="server" EmptyMessage="- select participant -"
                                    DataValueField="ParticipantID" DataTextField="ParticipantName" DropDownAutoWidth="Enabled"
                                    MaxHeight="300" Enabled="false" CheckBoxes="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnResetAttendance" Text="Remove" CssClass="linkBtn"
                                    ValidationGroup="removeAttendance" OnClick="btnResetAttendance_Click" />
                                |
                                <asp:LinkButton runat="server" ID="btnResetAttendanceCancel" Text="Cancel" CssClass="linkBtn"
                                    CausesValidation="False" OnClick="btnResetAttendanceCancel_Click" />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div style="padding: 10px 10px 0 10px">
                        <telerik:RadTabStrip ID="tabStrip1" runat="server" MultiPageID="multiPage1" SelectedIndex="0"
                            ShowBaseLine="True" Width="100%" CausesValidation="False" AutoPostBack="true"
                            OnTabClick="MainTab_Click">
                            <Tabs>
                                <telerik:RadTab Text="Class" Selected="True" />
                                <telerik:RadTab Text="Participants" />
                                <telerik:RadTab Text="Attendance" />
                                <telerik:RadTab Text="Test Scores" />
                                <telerik:RadTab Text="Journal" />
                            </Tabs>
                        </telerik:RadTabStrip>
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadMultiPage ID="multiPage1" runat="server" SelectedIndex="0" Width="100%"
                            RenderSelectedPageOnly="true">
                            <telerik:RadPageView ID="pvClass" runat="server" Selected="true">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadTabStrip ID="tsClasss" runat="server" SelectedIndex="0" MultiPageID="mpClass"
                                                Width="100%" OnTabClick="tsClass_TabClick" AutoPostBack="true">
                                                <Tabs>
                                                    <telerik:RadTab Text="Class List" Selected="true" />
                                                    <telerik:RadTab Text="Add Class" />
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </td>
                                        <td>
                                            <div runat="server" id="divClassExport" align="right">
                                                <asp:LinkButton runat="server" ID="btnExportClass" CssClass="linkBtn" OnClick="btnExportClass_Click"
                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Class records to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadMultiPage ID="mpClass" runat="server" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView ID="pvClassList" runat="server" Selected="true">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadGrid runat="server" ID="gridClass" AutoGenerateColumns="False" GridLines="None"
                                                    OnNeedDataSource="gridClass_NeedDataSource" OnItemCommand="gridClass_ItemCommand"
                                                    OnItemDataBound="gridClass_ItemDataBound" AllowFilteringByColumn="True" EnableLinqExpressions="False"
                                                    CssClass="RadGrid1" Skin="Metro" OnPreRender="gridClass_PreRender" AllowPaging="True"
                                                    AllowSorting="True" PageSize="15">
                                                    <GroupingSettings CaseSensitive="False" />
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Selecting AllowRowSelect="True" />
                                                        <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                    </ClientSettings>
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EditMode="PopUp" NoMasterRecordsText="Class list is empty."
                                                        EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                                        DataKeyNames="ClassID,ClientID,TrainingTypeID,ClassNameID" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                        <EditFormSettings CaptionFormatString="Edit Class ID: {0}" CaptionDataField="ClassID"
                                                            PopUpSettings-Height="500px" PopUpSettings-Width="650px">
                                                            <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                        </EditFormSettings>
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px"
                                                                UniqueName="ParticipantColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnParticipant" runat="server" ToolTip="Participants" OnClientClick="View_Participants()"
                                                                        OnClick="btnParticipant_Click">
                                                                            <img style="border:0;" alt="" src="Images/party1.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px"
                                                                UniqueName="Non-TUTestsColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnViewNonTUTest" runat="server" ToolTip="Non-TU Tests" OnClientClick="View_NonTUTests()"
                                                                        OnClick="btnViewNonTUTest_Click">
                                                                            <img style="border:0;"  alt="" src="Images/nontutest.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px"
                                                                UniqueName="TUTestsColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnViewTUTest" runat="server" ToolTip="TU Tests" OnClientClick="View_Tests()"
                                                                        OnClick="btnViewTUTest_Click">
                                                                            <img style="border:0;"  alt="" src="Images/tutest.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px"
                                                                UniqueName="TUCourseColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnViewTUCourse" runat="server" ToolTip="Courses" OnClientClick="View_Courses()"
                                                                        OnClick="btnViewTUCourse_Click">
                                                                            <img style="border:0;"  alt="" src="Images/cor.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                                UniqueName="CertificateColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnViewCertificate" runat="server" ToolTip="Certificate" OnClientClick="View_Certificates()"
                                                                        OnClick="btnViewCertificate_Click">
                                                                            <img style="border:0;"  alt="" src="Images/cert.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn UniqueName="ClassID" HeaderText="Class ID" DataField="ClassID"
                                                                AllowFiltering="true" ShowFilterIcon="False" CurrentFilterFunction="EqualTo"
                                                                AutoPostBackOnFilter="True" FilterControlWidth="50">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="classStatus" DataField="classStatus" HeaderText="Status"
                                                                CurrentFilterValue="Active">
                                                                <FilterTemplate>
                                                                    <telerik:RadComboBox ID="cbFilterStatus" DataTextField="classStatus" DataValueField="classStatus"
                                                                        DropDownAutoWidth="Enabled" DataSourceID="dsStatus" Width="60" SelectedValue='<%# (Container).OwnerTableView.GetColumn("classStatus").CurrentFilterValue %>'
                                                                        runat="server" OnClientSelectedIndexChanged="cbFilterStatusSelectedIndexChanged"
                                                                        AppendDataBoundItems="true">
                                                                        <Items>
                                                                            <telerik:RadComboBoxItem Text="All" />
                                                                        </Items>
                                                                    </telerik:RadComboBox>
                                                                    <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                                                        <script type="text/javascript">
                                                                            function cbFilterStatusSelectedIndexChanged(sender, args) {
                                                                                var tableView = $find("<%# (Container).OwnerTableView.ClientID %>");
                                                                                tableView.filter("classStatus", args.get_item().get_value(), "EqualTo");
                                                                            }

                                                                        </script>
                                                                    </telerik:RadScriptBlock>
                                                                </FilterTemplate>
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="ClientName" HeaderText="Program" DataField="ClientName"
                                                                AllowFiltering="True" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                AutoPostBackOnFilter="True" />
                                                            <telerik:GridBoundColumn UniqueName="TrainingType" HeaderText="Training Type" DataField="TrainingType"
                                                                AllowFiltering="True" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                AutoPostBackOnFilter="True" />
                                                            <telerik:GridBoundColumn UniqueName="StartDate" DataField="StartDate" HeaderText="Start Date"
                                                                AllowFiltering="False" />
                                                            <telerik:GridBoundColumn UniqueName="ClosedDate" DataField="ClosedDate" HeaderText="Close Date"
                                                                AllowFiltering="False" />
                                                            <telerik:GridBoundColumn UniqueName="ClassName" DataField="ClassName" HeaderText="Certification Name"
                                                                AllowFiltering="True" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                AutoPostBackOnFilter="True" />
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template">
                                                            <FormTemplate>
                                                                <table id="tblEditMode" class="tabledefault">
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:ValidationSummary ID="validationSummaryEditClass" ShowMessageBox="False" ShowSummary="True"
                                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                runat="server" ValidationGroup="editClass" CssClass="displayerror" EnableClientScript="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Wave #
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtWaveNumber" runat="server" EmptyMessage="- wave # -"
                                                                                MinValue="0" MaxValue="99999" MaxLength="5" SelectionOnFocus="SelectAll" Text='<%# Eval("WaveNumber") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Trainer Site"
                                                                                ControlToValidate="ddTraineeSite" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editClass"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Trainer Site
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="ddTraineeSite" runat="server" EmptyMessage="- trainer site -"
                                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataTextField="CompanySite" DataValueField="CompanySiteID"
                                                                                DataSourceID="dsSite" SelectedValue='<%# Bind("TraineeSiteID") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Program"
                                                                                ControlToValidate="editClient" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editClass"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Program
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="editClient" runat="server" EmptyMessage="- program -" DataTextField="Client"
                                                                                DataValueField="ClientID" DataSourceID="dsTrainingClient" Height="300" SelectedValue='<%# Bind("ClientID") %>'
                                                                                AutoPostBack="true" OnSelectedIndexChanged="editClient_SelectedIndexChanged" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Training Type"
                                                                                ControlToValidate="editTrainingType" SetFocusOnError="True" Display="Dynamic"
                                                                                ValidationGroup="editClass" CssClass="displayerror" EnableClientScript="False"
                                                                                Text="*" />
                                                                            Training Type
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="editTrainingType" runat="server" EmptyMessage="- training type -"
                                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataSourceID="dsTrainingType" DataValueField="TrainingTypeID"
                                                                                DataTextField="TrainingType" AutoPostBack="true" OnSelectedIndexChanged="editTrainingType_SelectedIndexChanged" />
                                                                        </td>
                                                                    </tr>
                                                                    <td class="tblLabel">
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Certification"
                                                                            ControlToValidate="cbeditClassName" SetFocusOnError="True" Display="Dynamic"
                                                                            CssClass="displayerror" ValidationGroup="editClass" Text="*" EnableClientScript="False" />
                                                                        Certification
                                                                    </td>
                                                                    <td class="tblfield">
                                                                        <telerik:RadComboBox ID="cbeditClassName" runat="server" EmptyMessage="- certification -"
                                                                            DropDownAutoWidth="Enabled" MaxHeight="300px" DataTextField="ClassName" DataValueField="ID"
                                                                            EnableViewState="False">
                                                                        </telerik:RadComboBox>
                                                                        <asp:SqlDataSource ID="dsEditClassNames" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                                                            SelectCommand="SELECT DISTINCT [ID], [ClassName] FROM [ref_TranscomUniversity_Class] 
                                                                                WHERE (([Active] = 1) AND ([ClientID] = @ClientID) AND ([TrainingTypeID] = @TrainingTypeID))">
                                                                            <SelectParameters>
                                                                                <asp:ControlParameter ControlID="editClient" DefaultValue="0" Name="ClientID" PropertyName="SelectedValue" />
                                                                                <asp:ControlParameter ControlID="editTrainingType" DefaultValue="0" Name="TrainingTypeID"
                                                                                    PropertyName="SelectedValue" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Location Type"
                                                                                ControlToValidate="ddlLocType" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editClass"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Location Type
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="ddlLocType" runat="server" EmptyMessage="- location type -"
                                                                                DropDownAutoWidth="Enabled" AllowCustomText="False" MaxHeight="300" DataSourceID="dsTrainingLocType"
                                                                                DataValueField="TrainingLocTypeID" DataTextField="TrainingLocTypeDesc" SelectedValue='<%# Bind("LocationTypeID") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Training Hours"
                                                                                ControlToValidate="ddTrainingHours" SetFocusOnError="True" Display="Dynamic"
                                                                                ValidationGroup="editClass" CssClass="displayerror" EnableClientScript="False"
                                                                                Text="*" />
                                                                            Training Hours
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="ddTrainingHours" runat="server" EmptyMessage="- training hours -"
                                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataSourceID="dsTrainingHours" DataValueField="TrainingHoursID"
                                                                                DataTextField="TrainingHours" SelectedValue='<%# Bind("TrainingHoursID") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            RTP Tandim #
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtRTPTandim" runat="server" EmptyMessage="- RTP # -"
                                                                                MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll" Text='<%# Eval("RTP_Tandim") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Training Validation #
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtTrainingValNo" runat="server" EmptyMessage="- training # -"
                                                                                MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll" Text='<%# Eval("TrainingValidationNo") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Login Request #
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtLoginReqNo" runat="server" EmptyMessage="- login # -"
                                                                                MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll" Text='<%# Eval("LoginReqNo") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Other Attrition
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtOtherAttrition" runat="server" MinValue="0" MaxValue="99999"
                                                                                MaxLength="5" Text='<%# Eval("OtherAttrition") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Day 1 No Show
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtDay1NoShow" runat="server" MinValue="0" MaxValue="99999"
                                                                                MaxLength="5" Text='<%# Eval("Day1NoShow") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Starting Head Count
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtStartingHeadCount" runat="server" MinValue="0"
                                                                                MaxValue="99999" MaxLength="5" Text='<%# Eval("StartingHeadCount") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <h3>
                                                                                Class Dates
                                                                            </h3>
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Start Date"
                                                                                ControlToValidate="dpStartDate" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editClass"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Start Date
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpStartDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                                Width="140" DbSelectedDate='<%# Bind("StartDate") %>'>
                                                                                <DateInput ID="DateInput7" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Close Date"
                                                                                ControlToValidate="dpEndDate" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editClass"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Close Date
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpEndDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                                Width="140" DbSelectedDate='<%# Bind("ClosedDate") %>'>
                                                                                <DateInput ID="DateInput8" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                            <asp:CompareValidator ID="dateCompareValidator1" runat="server" ControlToValidate="dpEndDate"
                                                                                ControlToCompare="dpStartDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="Close Date must be after the Start Date"
                                                                                CssClass="displayerror" ValidationGroup="editClass" EnableClientScript="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Production Start Date
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="RadStartDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                                Width="140" DbSelectedDate='<%# Bind("ProductionStartDate") %>'>
                                                                                <DateInput ID="DateInput9" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Orientation Date
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpOrientationDate" runat="server" EnableTyping="False"
                                                                                ShowPopupOnFocus="True" Width="140" DbSelectedDate='<%# Bind("OrientationDate") %>'>
                                                                                <DateInput ID="DateInput10" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <br />
                                                                            <asp:LinkButton ID="bntUpdate" ValidationGroup="editClass" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                                                                OnClientClick="if(!confirm('Are you sure you want to update this record?')) return false;"
                                                                                CssClass="linkBtn" />
                                                                            |
                                                                            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                                CommandName="Cancel" OnClientClick="if(!confirm('Are you sure you want to close this window?')) return false;"
                                                                                CssClass="linkBtn" />
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvAddClass" runat="server">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <div>
                                                    <table width="100%" class="tabledefault">
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <asp:ValidationSummary ID="validationSummaryAddClass" ShowMessageBox="False" ShowSummary="True"
                                                                    DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                    runat="server" ValidationGroup="addClass" CssClass="displayerror" EnableClientScript="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <h3>
                                                                    Class Details
                                                                </h3>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Wave #
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtWaveNumber" runat="server" EmptyMessage="- wave # -"
                                                                    MinValue="0" MaxValue="99999" MaxLength="5" SelectionOnFocus="SelectAll">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Trainer Site"
                                                                    ControlToValidate="ddTraineeSite" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Trainer Site
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="ddTraineeSite" runat="server" EmptyMessage="- trainer site -"
                                                                    DropDownAutoWidth="Enabled" MaxHeight="300" DataTextField="CompanySite" DataValueField="CompanySiteID"
                                                                    DataSourceID="dsSite" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Program"
                                                                    ControlToValidate="RadClient" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Program
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="RadClient" runat="server" EmptyMessage="- program -" DataTextField="Client"
                                                                    DataValueField="ClientID" DataSourceID="dsTrainingClient" Height="300" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="addClient_SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Training Type"
                                                                    ControlToValidate="ddTrainingType" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Training Type
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="ddTrainingType" runat="server" EmptyMessage="- training type -"
                                                                    DropDownAutoWidth="Enabled" MaxHeight="300" DataSourceID="dsTrainingTYpe" DataValueField="TrainingTypeID"
                                                                    DataTextField="TrainingType" AutoPostBack="true" OnSelectedIndexChanged="addTrainingType_SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=" Certification"
                                                                    ControlToValidate="cbClassName" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                    ValidationGroup="addClass" Text="*" EnableClientScript="False" />
                                                                Certification
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="cbClassName" runat="server" EmptyMessage="- certification -"
                                                                    DropDownAutoWidth="Enabled" MaxHeight="300px" DataTextField="ClassName" DataValueField="ID"
                                                                    DataSourceID="dsClassNames" EnableViewState="False">
                                                                </telerik:RadComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Location Type"
                                                                    ControlToValidate="ddlLocType" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Location Type
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="ddlLocType" runat="server" EmptyMessage="- location type -"
                                                                    DropDownAutoWidth="Enabled" AllowCustomText="False" MaxHeight="300" DataSourceID="dsTrainingLocType"
                                                                    DataValueField="TrainingLocTypeID" DataTextField="TrainingLocTypeDesc" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Training Hours"
                                                                    ControlToValidate="ddTrainingHours" SetFocusOnError="True" Display="Dynamic"
                                                                    ValidationGroup="addClass" CssClass="displayerror" EnableClientScript="False"
                                                                    Text="*" />
                                                                Training Hours
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="ddTrainingHours" runat="server" EmptyMessage="- training hours -"
                                                                    DropDownAutoWidth="Enabled" MaxHeight="300" DataSourceID="dsTrainingHours" DataValueField="TrainingHoursID"
                                                                    DataTextField="TrainingHours" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                RTP Tandim #
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtRTPTandim" runat="server" EmptyMessage="- RTP # -"
                                                                    MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Training Validation #
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtTrainingValNo" runat="server" EmptyMessage="- training # -"
                                                                    MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Login Request #
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtLoginReqNo" runat="server" EmptyMessage="- login # -"
                                                                    MinValue="0" MaxValue="9999999" MaxLength="7" SelectionOnFocus="SelectAll">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Other Attrition
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtOtherAttrition" runat="server" MinValue="0" MaxValue="99999"
                                                                    DisplayText="0" MaxLength="5">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Day 1 No Show
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtDay1NoShow" runat="server" MinValue="0" MaxValue="99999"
                                                                    DisplayText="0" MaxLength="5">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Starting Head Count
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadNumericTextBox ID="txtStartingHeadCount" runat="server" MinValue="0"
                                                                    MaxValue="99999" DisplayText="0" MaxLength="5">
                                                                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                </telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <h3>
                                                                    Class Dates
                                                                </h3>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Start Date"
                                                                    ControlToValidate="dpStartDate" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Start Date
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="dpStartDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                    Width="140">
                                                                    <DateInput ID="DateInput11" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Close Date"
                                                                    ControlToValidate="dpEndDate" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addClass"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Close Date
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="dpEndDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                    Width="140">
                                                                    <DateInput ID="DateInput12" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                                <asp:CompareValidator ID="dateCompareValidator1" runat="server" ControlToValidate="dpEndDate"
                                                                    ControlToCompare="dpStartDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="Close Date must be after the Start Date"
                                                                    CssClass="displayerror" ValidationGroup="addClass" EnableClientScript="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Production Start Date
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="RadStartDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                    Width="140">
                                                                    <DateInput ID="DateInput13" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Orientation Date
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="dpOrientationDate" runat="server" EnableTyping="False"
                                                                    ShowPopupOnFocus="True" Width="140">
                                                                    <DateInput ID="DateInput14" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <h3>
                                                                    Non-TU Tests
                                                                </h3>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Test Name"
                                                                    ControlToValidate="txtTestName" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addTest"
                                                                    CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                Test Name
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadTextBox ID="txtTestName" runat="server" EmptyMessage="- test name -"
                                                                    MaxLength="200" ValidationGroup="addTest" />
                                                                <asp:LinkButton runat="server" ID="btnAddTest" Text="Add to List >" ValidationGroup="addTest"
                                                                    CssClass="linkBtn" OnClick="btnAddTest_Click" Font-Size="8.5" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadGrid runat="server" ID="rgAssignedTests" AutoGenerateColumns="False"
                                                                    AllowPaging="False" GridLines="None" CssClass="RadGrid1" Skin="Metro" Width="600px"
                                                                    OnNeedDataSource="rgAssignedTests_NeedDataSource" OnItemCommand="rgAssignedTests_ItemCommand">
                                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                                    <ClientSettings EnableRowHoverStyle="True">
                                                                        <Selecting AllowRowSelect="True" />
                                                                    </ClientSettings>
                                                                    <MasterTableView ShowHeader="true" DataKeyNames="TestTypeID" AutoGenerateColumns="False"
                                                                        EnableNoRecordsTemplate="True" NoMasterRecordsText="No non-TU test added." ShowHeadersWhenNoRecords="True"
                                                                        TableLayout="Auto" EditMode="InPlace" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        AlternatingItemStyle-HorizontalAlign="Center">
                                                                        <Columns>
                                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                                HeaderStyle-Width="70" EditImageUrl="Images/editpen2.png">
                                                                            </telerik:GridEditCommandColumn>
                                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                            </telerik:GridButtonColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="TestName" HeaderText="Test Name" DataField="TestName"
                                                                                SortExpression="TestName" EditFormHeaderTextFormat="Test Name" ShowFilterIcon="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblTestName" Text='<%# Eval("TestName") %>' />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <telerik:RadTextBox ID="txtEditTestName" runat="server" EmptyMessage="- test name -"
                                                                                        Text='<%# Bind("TestName") %>' MaxLength="200" />
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <h3>
                                                                    TU Tests
                                                                </h3>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Test Categories
                                                            </td>
                                                            <td class="tblfield" colspan="2">
                                                                <telerik:RadComboBox ID="cbAddTUTestCategory" DataTextField="Campaign" DataValueField="CampaignID"
                                                                    runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="cbAddTUTestCategoryAssignSelectedIndexChanged"
                                                                    CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                                <telerik:RadDropDownTree runat="server" ID="rdtAddTUTestCategory" DefaultMessage="- select subcategory -"
                                                                    DefaultValue="0" DataTextField="Campaign" DataValueField="CampaignID" DataFieldParentID="ParentCampaignId"
                                                                    TextMode="FullPath" AutoPostBack="True" OnClientEntryAdding="OnClientEntryAdding"
                                                                    Enabled="false" Skin="Default" OnEntryAdded="rdtAddTUTestCategory_EntryAdded"
                                                                    OnEntryRemoved="rdtAddTUTestCategory_EntryRemoved">
                                                                    <DropDownSettings Width="400px" Height="250px" />
                                                                </telerik:RadDropDownTree>
                                                                <asp:LinkButton ID="btnSelectTUTest" runat="server" Text="Choose TU Test/s" ToolTip="Assign TU Test"
                                                                    CssClass="linkBtn" OnClick="btnSelectTUTest_Click" Visible="false" Font-Size="8.5" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Currently Assigned
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadGrid runat="server" ID="rgAddAssignedTUTest" AutoGenerateColumns="False"
                                                                    AllowPaging="true" PageSize="10" GridLines="None" CssClass="RadGrid1" Skin="Metro"
                                                                    Width="100%" OnItemCommand="rgAddAssignedTUTest_ItemCommand" OnNeedDataSource="rgAddAssignedTUTest_NeedDataSource">
                                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                                    <ClientSettings EnableRowHoverStyle="True">
                                                                        <Selecting AllowRowSelect="True" />
                                                                    </ClientSettings>
                                                                    <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EnableNoRecordsTemplate="True"
                                                                        NoMasterRecordsText="No assigned TU test" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                                                        EditMode="InPlace" DataKeyNames="CourseID,CourseAssignmentID,Title" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                                        <Columns>
                                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                            </telerik:GridButtonColumn>
                                                                            <telerik:GridBoundColumn UniqueName="Title" HeaderText="Test Name" DataField="Title"
                                                                                AllowFiltering="False" ReadOnly="True">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                                                                Visible="false" SortExpression="DueDate" EditFormHeaderTextFormat="Due Date"
                                                                                ShowFilterIcon="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <telerik:RadDatePicker ID="dpDueDateEdit" runat="server" Width="140" EnableTyping="False"
                                                                                        SelectedDate='<%# Bind("DueDate") %>' ShowPopupOnFocus="True">
                                                                                        <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                                    </telerik:RadDatePicker>
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <h3>
                                                                    Assign Courses
                                                                </h3>
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Assignment Filters
                                                            </td>
                                                            <td class="tblfield" colspan="2">
                                                                <telerik:RadComboBox ID="cbCategoryAssign" DataTextField="Category" DataValueField="CategoryID"
                                                                    runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbCategoryAssignSelectedIndexChanged"
                                                                    CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                                <telerik:RadDropDownTree runat="server" ID="ddtSubcategoryAssign" DefaultMessage="- select subcategory -"
                                                                    DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                    DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" Width="150px" AutoPostBack="True"
                                                                    Enabled="false" OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="ddtSubcategoryAssign_EntryAdded"
                                                                    OnEntryRemoved="ddtSubcategoryAssign_EntryRemoved">
                                                                    <DropDownSettings Width="400px" Height="250px" />
                                                                </telerik:RadDropDownTree>
                                                                <asp:LinkButton ID="btnAssign" runat="server" Text="Choose Course/s" ToolTip="Assign"
                                                                    CssClass="linkBtn" OnClick="btnAssign_Click" ValidationGroup="vgAssign" Visible="false"
                                                                    Font-Size="8.5" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                Currently Assigned
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadGrid runat="server" ID="rgAssignedCourses" AutoGenerateColumns="False"
                                                                    AllowPaging="True" PageSize="10" GridLines="None" CssClass="RadGrid1" Skin="Metro"
                                                                    Width="100%" OnItemCommand="rgAssignedCourses_ItemCommand" OnNeedDataSource="rgAssignedCourses_NeedDataSource">
                                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                                    <ClientSettings EnableRowHoverStyle="True">
                                                                        <Selecting AllowRowSelect="True" />
                                                                    </ClientSettings>
                                                                    <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EnableNoRecordsTemplate="True"
                                                                        NoMasterRecordsText="No assigned course" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                                                        EditMode="InPlace" DataKeyNames="CourseID,CourseAssignmentID,Title" ItemStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                                        <Columns>
                                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                                HeaderStyle-Width="70px" EditImageUrl="Images/editpen2.png">
                                                                            </telerik:GridEditCommandColumn>
                                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                            </telerik:GridButtonColumn>
                                                                            <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                                AllowFiltering="False" ReadOnly="True">
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                                                                SortExpression="DueDate" EditFormHeaderTextFormat="Due Date" ShowFilterIcon="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <telerik:RadDatePicker ID="dpDueDateEdit" runat="server" Width="140" EnableTyping="False"
                                                                                        SelectedDate='<%# Bind("DueDate") %>' ShowPopupOnFocus="True">
                                                                                        <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                                    </telerik:RadDatePicker>
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <br />
                                                                <asp:LinkButton runat="server" ID="btnAddClass" Text="Add" ValidationGroup="addClass"
                                                                    CssClass="linkBtn" OnClick="btnAddClass_Click" OnClientClick="if(!confirm('Are you sure you want to add this record?')) return false;" />
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvParticipant" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadTabStrip ID="tsParticipant" runat="server" SelectedIndex="0" MultiPageID="mpParticipant"
                                                Width="100%" OnTabClick="tsParticipant_TabClick" AutoPostBack="True">
                                                <Tabs>
                                                    <telerik:RadTab Text="Participant List" Selected="true" />
                                                    <telerik:RadTab Text="Add Participant" />
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </td>
                                        <td>
                                            <div runat="server" id="divExportParticipant" align="right">
                                                <asp:LinkButton runat="server" ID="btnExportMainViewParticipant" CssClass="linkBtn"
                                                    OnClick="btnExportMainViewParticipant_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Participant records to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadMultiPage ID="mpParticipant" runat="server" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView ID="pvParticipantList" runat="server" Selected="true">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadGrid runat="server" ID="gridParticipants" AutoGenerateColumns="False"
                                                    GridLines="None" OnNeedDataSource="gridParticipants_NeedDataSource" OnItemCommand="gridParticipants_ItemCommand"
                                                    OnItemDataBound="gridParticipants_ItemDataBound" AllowFilteringByColumn="True"
                                                    EnableLinqExpressions="True" CssClass="RadGrid1" Skin="Metro" AllowPaging="True"
                                                    AllowSorting="True" PageSize="15">
                                                    <GroupingSettings CaseSensitive="False" />
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Selecting AllowRowSelect="True" />
                                                        <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                    </ClientSettings>
                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" PageSize="15" EditMode="PopUp"
                                                        NoMasterRecordsText="Participant list is empty." EnableNoRecordsTemplate="True"
                                                        ShowHeadersWhenNoRecords="True" AllowPaging="True" TableLayout="Auto" DataKeyNames="ParticipantID,ReasonID,DescriptionID,CertificationStatus,ClassID,cimnumber"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                        <EditFormSettings CaptionFormatString="Edit Participant ID : {0}" CaptionDataField="ParticipantID"
                                                            PopUpSettings-Height="550px" PopUpSettings-Width="800px">
                                                            <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                        </EditFormSettings>
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="btnEditParticipants" CssClass="linkBtn" OnClientClick="View_windowEditParticipant('1')"
                                                                        OnClick="btnEditParticipants_Click" ToolTip="Edit"><img style="border:0; padding-left: 3px" alt="" src="Images/editpen2.png"/></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn UniqueName="ParticipantID" HeaderText="Participant ID" DataField="ParticipantID"
                                                                SortExpression="ParticipantID" AllowFiltering="true" ReadOnly="True" ShowFilterIcon="False"
                                                                CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" DataType="System.String"
                                                                FilterControlWidth="50">
                                                                <HeaderStyle ForeColor="Silver" />
                                                                <ItemStyle ForeColor="Gray" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="ClassID" HeaderText="Class ID" DataField="ClassID"
                                                                SortExpression="ClassID" AllowFiltering="true" ReadOnly="True" ShowFilterIcon="False"
                                                                CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" DataType="System.String"
                                                                FilterControlWidth="50">
                                                                <HeaderStyle ForeColor="Silver" />
                                                                <ItemStyle ForeColor="Gray" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="ParticipantName" HeaderText="Participant Name"
                                                                DataField="ParticipantName" SortExpression="ParticipantName" AllowFiltering="false" />
                                                            <telerik:GridBoundColumn UniqueName="ParticipantSite" HeaderText="Participant Site"
                                                                DataField="ParticipantSite" SortExpression="ParticipantSite" AllowFiltering="false" />
                                                            <telerik:GridBoundColumn UniqueName="DateJoined" HeaderText="Date Joined" DataField="DateJoined"
                                                                SortExpression="DateJoined" AllowFiltering="false" DataType="System.DateTime" />
                                                            <telerik:GridBoundColumn UniqueName="ParticipantStatus" HeaderText="Participant Status"
                                                                DataField="ParticipantStatus" SortExpression="ParticipantStatus" AllowFiltering="false" />
                                                            <telerik:GridBoundColumn UniqueName="CertificationStatus" HeaderText="Certification Status"
                                                                DataField="CertificationStatus" SortExpression="CertificationStatus" AllowFiltering="False" />
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template">
                                                            <FormTemplate>
                                                                <table id="Table1" class="tabledefault">
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:ValidationSummary ID="validationSummaryEditParticipant" ShowMessageBox="False"
                                                                                ShowSummary="True" DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                runat="server" ValidationGroup="editParticipants" CssClass="displayerror" EnableClientScript="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvClassID" runat="server" ErrorMessage="Class ID"
                                                                                ControlToValidate="cmbClassID" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editParticipants"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Class ID
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="cmbClassID" runat="server" EmptyMessage="- class id -" DropDownAutoWidth="Enabled"
                                                                                MaxHeight="300" DataSourceID="dsClassID1" DataValueField="ClassID" DataTextField="ClassID"
                                                                                Width="100" SelectedValue='<%# Bind("ClassID") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date Joined"
                                                                                ControlToValidate="dpDateJoined" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editParticipants"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            Date Joined
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpDateJoined" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                                DbSelectedDate='<%# Bind("DateJoined") %>' Width="140">
                                                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="CIM Number"
                                                                                ControlToValidate="txtCimNumber" SetFocusOnError="True" Display="Dynamic" ValidationGroup="editParticipants"
                                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                                            CIM Number
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadNumericTextBox ID="txtCimNumber" runat="server" EmptyMessage="- cim number -"
                                                                                MinValue="0" MaxValue="99999999" MaxLength="8" SelectionOnFocus="SelectAll" Enabled="false"
                                                                                Text='<%# Eval("cimnumber") %>'>
                                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                            </telerik:RadNumericTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Name
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox ID="txtName" runat="server" EmptyMessage="- name -" ReadOnly="true"
                                                                                Text='<%# Eval("name") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Site
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox ID="txtSite" runat="server" EmptyMessage="- site -" ReadOnly="true"
                                                                                Text='<%# Eval("ParticipantSite") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Status"
                                                                                ControlToValidate="radioStatus" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                ValidationGroup="editParticipants" EnableClientScript="False" Text="*" />
                                                                            Status
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:RadioButtonList ID="radioStatus" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" CssClass="radio" OnSelectedIndexChanged="radioeditStatus_SelectedIndexChanged"
                                                                                Font-Size="8pt" SelectedValue='<%# Bind("ParticipantStatus") %>'>
                                                                                <asp:ListItem Value="Active" Text="Active" Selected="true" />
                                                                                <asp:ListItem Value="Inactive" Text="Inactive" />
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ErrorMessage="Reason" ControlToValidate="cbeditInactiveReason"
                                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="editParticipants" CssClass="displayerror"
                                                                                EnableClientScript="False" Text="*" Enabled="false" />
                                                                            Reason
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="cbeditInactiveReason" runat="server" EmptyMessage="- reason -"
                                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="id" AutoPostBack="true"
                                                                                DataTextField="details" DataSourceID="dsParticipantReason" Enabled="false" SelectedValue='<%# Bind("ReasonID") %>'
                                                                                OnSelectedIndexChanged="cbeditInactiveReason_SelectedIndexChanged" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ErrorMessage="Description"
                                                                                ControlToValidate="cbeditInactiveDesc" SetFocusOnError="True" Display="Dynamic"
                                                                                ValidationGroup="editParticipants" CssClass="displayerror" EnableClientScript="False"
                                                                                Text="*" Enabled="false" />
                                                                            Description
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="cbeditInactiveDesc" runat="server" EmptyMessage="- description -"
                                                                                DropDownAutoWidth="Enabled" MaxHeight="300" Enabled="false" DataValueField="id"
                                                                                AutoPostBack="true" DataTextField="details" OnSelectedIndexChanged="cbeditInactiveDesc_SelectedIndexChanged" />
                                                                            <asp:SqlDataSource ID="dsParticipantDesc" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                                                                SelectCommand="pr_Infinity_Lkp_ParticipantReason" SelectCommandType="StoredProcedure">
                                                                                <SelectParameters>
                                                                                    <asp:Parameter DefaultValue="0" Name="ReasonID" Type="Int32" />
                                                                                </SelectParameters>
                                                                            </asp:SqlDataSource>
                                                                            <telerik:RadTextBox ID="txtEditDescOthers" runat="server" EmptyMessage="- other description -"
                                                                                MaxLength="255" Width="250px" Visible="false" Text='<%# Eval("OtherDescription") %>' />
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Other Description"
                                                                                ControlToValidate="txtEditDescOthers" SetFocusOnError="True" Display="Dynamic"
                                                                                ValidationGroup="editParticipants" CssClass="displayerror" EnableClientScript="False"
                                                                                Text="*" Enabled="false" />
                                                                            <asp:SqlDataSource ID="dsParticipantOtherDesc" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                                                                SelectCommand="select count(*) FROM tbl_Infinity_lkp_ParticipantDesc where [descid] = @DescID and allowothers = 0">
                                                                                <SelectParameters>
                                                                                    <asp:ControlParameter ControlID="cbeditInactiveDesc" DefaultValue="0" Name="DescID"
                                                                                        PropertyName="SelectedValue" />
                                                                                </SelectParameters>
                                                                            </asp:SqlDataSource>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Certified
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:CheckBox runat="server" ID="chkeditCertified" Text="Yes" Checked="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Comments
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadEditor ID="txteditComments" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                                                                AllowScripts="false" Width="500px" Height="250px" EmptyMessage="- course description -"
                                                                                Content='<%# Bind("comments") %>' BorderColor="LightGray" BorderStyle="Solid"
                                                                                BorderWidth="1px">
                                                                                <CssFiles>
                                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                                </CssFiles>
                                                                            </telerik:RadEditor>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <br />
                                                                            <asp:LinkButton ID="LinkButton1" ValidationGroup="editParticipants" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                                                                CssClass="linkBtn" />
                                                                            <br />
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvAddParticipant" runat="server">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="validationSummaryAddParticipant" ShowMessageBox="False"
                                                                ShowSummary="True" DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="addParticipants" CssClass="displayerror" EnableClientScript="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvClassID" runat="server" ErrorMessage="Class ID"
                                                                ControlToValidate="cmbClassID" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addToList"
                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                            Class ID
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cmbClassID" runat="server" EmptyMessage="- class id -" Width="100"
                                                                MaxHeight="300" DataSourceID="dsClassID" DataValueField="ClassID" DataTextField="ClassID" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="CIM Number"
                                                                ControlToValidate="txtCimNumber" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addToList"
                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                            CIM Number
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox ID="txtCimNumber" runat="server" EmptyMessage="- cim number -"
                                                                MinValue="0" MaxValue="99999999" MaxLength="8" SelectionOnFocus="SelectAll">
                                                                <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                            </telerik:RadNumericTextBox>
                                                            <asp:LinkButton ID="btnGetCIMInfo" runat="server" Text="Add CIM to the list" CssClass="linkBtn"
                                                                OnClick="btnGetCIMInfo_Click" Font-Size="8pt" ValidationGroup="addToList" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Participant/s
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadGrid runat="server" ID="rgAssignedParticipant" AutoGenerateColumns="False"
                                                                AllowPaging="True" PageSize="10" GridLines="None" CssClass="RadGrid1" Skin="Metro"
                                                                Width="600px" OnNeedDataSource="rgAssignedParticipant_NeedDataSource" OnItemCommand="rgAssignedParticipant_ItemCommand">
                                                                <FilterItemStyle HorizontalAlign="Center" />
                                                                <ClientSettings EnableRowHoverStyle="True">
                                                                    <Selecting AllowRowSelect="True" />
                                                                </ClientSettings>
                                                                <PagerStyle Mode="Slider" AlwaysVisible="True"></PagerStyle>
                                                                <MasterTableView ShowHeader="true" DataKeyNames="CimNumber,ParticipantID" AutoGenerateColumns="False"
                                                                    EnableNoRecordsTemplate="True" NoMasterRecordsText="No participant added." ShowHeadersWhenNoRecords="True"
                                                                    TableLayout="Auto" EditMode="InPlace" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                    AlternatingItemStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                            ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                            ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                            ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                        </telerik:GridButtonColumn>
                                                                        <telerik:GridBoundColumn UniqueName="CimNumber" HeaderText="CIM" DataField="CimNumber"
                                                                            AllowFiltering="False" ReadOnly="True">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn UniqueName="ParticipantName" HeaderText="Name" DataField="ParticipantName"
                                                                            AllowFiltering="False" ReadOnly="True">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn UniqueName="Site" HeaderText="Site" DataField="Site" AllowFiltering="False"
                                                                            ReadOnly="True">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date Joined"
                                                                ControlToValidate="dpDateJoined" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addParticipants"
                                                                CssClass="displayerror" EnableClientScript="False" Text="*" />
                                                            Date Joined
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpDateJoined" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                Width="140">
                                                                <DateInput ID="DateInput15" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Status"
                                                                ControlToValidate="radioStatus" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                ValidationGroup="addParticipants" EnableClientScript="False" Text="*" />
                                                            Status
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:RadioButtonList ID="radioStatus" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                RepeatLayout="Flow" CssClass="radio" OnSelectedIndexChanged="radioStatus_SelectedIndexChanged"
                                                                Font-Size="8pt">
                                                                <asp:ListItem Value="Active" Text="Active" Selected="true" />
                                                                <asp:ListItem Value="Inactive" Text="Inactive" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ErrorMessage="Reason" ControlToValidate="cbInactiveReason"
                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="addParticipants" CssClass="displayerror"
                                                                EnableClientScript="False" Text="*" Enabled="false" />
                                                            Reason
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbInactiveReason" runat="server" EmptyMessage="- reason -"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="id" AutoPostBack="true"
                                                                DataTextField="details" DataSourceID="dsParticipantReason" Enabled="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ErrorMessage="Description"
                                                                ControlToValidate="cbInactiveDesc" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addParticipants"
                                                                CssClass="displayerror" EnableClientScript="False" Text="*" Enabled="false" />
                                                            Description
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbInactiveDesc" runat="server" EmptyMessage="- description -"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="id" DataTextField="details"
                                                                AutoPostBack="true" Enabled="false" DataSourceID="dsParticipantDesc" OnSelectedIndexChanged="cbInactiveDesc_SelectedIndexChanged" />
                                                            <telerik:RadTextBox ID="txtAddDescOthers" runat="server" EmptyMessage="- other description -"
                                                                MaxLength="255" Width="250px" Visible="false" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Other Description"
                                                                ControlToValidate="txtAddDescOthers" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addParticipants" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" Enabled="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Certified
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:CheckBox runat="server" ID="chkAddCertified" Text="Yes" Checked="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Comments
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadEditor ID="txtAddComments" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                                                AllowScripts="false" Width="500px" Height="250" EmptyMessage="- course description -"
                                                                BorderColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                </CssFiles>
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <br />
                                                            <asp:LinkButton runat="server" ID="btnAddParticipants" Text="Add" ValidationGroup="addParticipants"
                                                                CssClass="linkBtn" OnClick="btnAddParticipants_Click" OnClientClick="if(!confirm('Are you sure you want to add this record?')) return false;" />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvAttendance" runat="server">
                                <div class="paneldefault">
                                    <div class="insidecontainer">
                                        <div style="vertical-align: middle">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="middle">
                                                        <label>
                                                            Select filter:
                                                        </label>
                                                        <telerik:RadComboBox ID="cbAttendanceClassID" runat="server" EmptyMessage="- class id -"
                                                            Width="100" MaxHeight="300" DataSourceID="dsClassID" DataValueField="ClassID"
                                                            DataTextField="ClassID" OnSelectedIndexChanged="btnViewAttendance_Click" AutoPostBack="true" />
                                                        <telerik:RadDatePicker ID="dpAttendanceForm" runat="server" EnableTyping="False"
                                                            ShowPopupOnFocus="True" OnSelectedDateChanged="btnViewAttendance_Click" AutoPostBack="true"
                                                            Width="140">
                                                            <DateInput ID="DateInput1" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                            <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                        </telerik:RadDatePicker>
                                                    </td>
                                                    <td align="right" valign="middle">
                                                        <div id="managecontrol2" style="width: 100%">
                                                            <asp:LinkButton runat="server" ID="btnCallUpdateAttendance" Text="Batch Update" OnClientClick="View_windowUpdateAttendance()"
                                                                CausesValidation="False" Font-Size="8" />
                                                            <asp:LinkButton runat="server" ID="btnCallRemoveAttendance" Text="Batch Remove" OnClientClick="View_windowRemoveAttendance()"
                                                                CausesValidation="False" Font-Size="8" />
                                                        </div>
                                                    </td>
                                                    <td align="right" style="line-height: 20px; width: 115px">
                                                        <div style="vertical-align: bottom; margin-bottom: 7px">
                                                            <asp:LinkButton runat="server" ID="btnExportAttendance" CssClass="linkBtn" OnClick="btnExportAttendance_Click"
                                                                Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Attendance list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div>
                                            <table id="tblViewAttendance" class="tabledefault">
                                                <tr>
                                                    <td style="width: 60%">
                                                        <telerik:RadGrid runat="server" ID="gridAttendance" AutoGenerateColumns="False" GridLines="None"
                                                            AllowFilteringByColumn="True" EnableLinqExpressions="False" AllowPaging="True"
                                                            AllowSorting="True" CssClass="RadGrid1" Skin="Metro" OnNeedDataSource="gridAttendance_NeedDataSource"
                                                            OnItemCommand="gridAttendance_ItemCommand">
                                                            <GroupingSettings CaseSensitive="False" />
                                                            <FilterItemStyle HorizontalAlign="Center" />
                                                            <ClientSettings EnableRowHoverStyle="True">
                                                                <Selecting AllowRowSelect="True" />
                                                            </ClientSettings>
                                                            <MasterTableView ShowHeader="true" AutoGenerateColumns="False" PageSize="15" EditMode="EditForms"
                                                                NoMasterRecordsText="Participant list is empty." EnableNoRecordsTemplate="True"
                                                                ShowHeadersWhenNoRecords="True" TableLayout="Auto" DataKeyNames="ParticipantID,ClassID,CimNumber,ClassDate,AttendanceStatusID"
                                                                GridLines="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                AlternatingItemStyle-HorizontalAlign="Center">
                                                                <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                                <Columns>
                                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                        HeaderStyle-Width="80px" EditImageUrl="Images/editpen2.png">
                                                                    </telerik:GridEditCommandColumn>
                                                                    <telerik:GridBoundColumn UniqueName="ParticipantName" HeaderText="Participant Name"
                                                                        DataField="ParticipantName" FilterControlWidth="120" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="True" />
                                                                    <telerik:GridBoundColumn UniqueName="ParticipantStatus" HeaderText="Participant Status"
                                                                        DataField="ParticipantStatus" FilterControlWidth="100" ShowFilterIcon="False"
                                                                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" />
                                                                    <telerik:GridBoundColumn UniqueName="Status" HeaderText="Attendance Status" DataField="AttendanceStatus"
                                                                        FilterControlWidth="100" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                        AutoPostBackOnFilter="True" />
                                                                    <telerik:GridBoundColumn UniqueName="IncidentValue" HeaderText="Training Hours" DataField="IncidentValue"
                                                                        AllowFiltering="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                                </Columns>
                                                                <EditFormSettings EditFormType="Template">
                                                                    <FormTemplate>
                                                                        <asp:Panel ID="Panel3" runat="server" DefaultButton="btnUpdateAttendance">
                                                                            <table class="tabledefault">
                                                                                <tr>
                                                                                    <td class="tblLabel">
                                                                                    </td>
                                                                                    <td class="tblfield">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tblLabel">
                                                                                        Attendance Status
                                                                                    </td>
                                                                                    <td class="tblfield">
                                                                                        <telerik:RadComboBox ID="cbAttendanceStatus" runat="server" EmptyMessage="- attendance status -"
                                                                                            DataSourceID="dsAttendanceStatus" DataValueField="AttendanceStatusID" DataTextField="AttendanceStatus"
                                                                                            SelectedValue='<%# Bind("AttendanceStatusID") %>' DropDownAutoWidth="Enabled"
                                                                                            MaxHeight="300" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tblLabel">
                                                                                        Training Hours
                                                                                    </td>
                                                                                    <td class="tblfield">
                                                                                        <telerik:RadNumericTextBox ID="txtTrainingHours" runat="server" EmptyMessage="- training hours -"
                                                                                            MinValue="0" MaxValue="99999999" MaxLength="8" SelectionOnFocus="SelectAll" Text='<%# Eval("IncidentValue") %>'>
                                                                                            <NumberFormat GroupSeparator="" />
                                                                                        </telerik:RadNumericTextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="tblLabel">
                                                                                    </td>
                                                                                    <td class="tblfield">
                                                                                        <br />
                                                                                        <asp:LinkButton ID="btnUpdateAttendance" ValidationGroup="editClass" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                            runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                                                                            CssClass="linkBtn">
                                                                                        </asp:LinkButton>
                                                                                        |
                                                                                        <asp:LinkButton ID="btnCancelAttendance" Text="Cancel" runat="server" CausesValidation="False"
                                                                                            CommandName="Cancel" CssClass="linkBtn" />
                                                                                        <br />
                                                                                        <br />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </FormTemplate>
                                                                </EditFormSettings>
                                                            </MasterTableView>
                                                        </telerik:RadGrid>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvTestScores" runat="server">
                                <div class="paneldefault">
                                    <div class="insidecontainer">
                                        <table width="100%" cellpadding="0" cellspacing="0" style="padding: 0 0 8px 0">
                                            <tr>
                                                <td>
                                                    <label>
                                                        Select filters:
                                                    </label>
                                                    <telerik:RadComboBox ID="cbLoadClass" runat="server" EmptyMessage="- class id -"
                                                        Width="100" MaxHeight="300" AutoPostBack="true" DataSourceID="dsClassID" DataValueField="ClassID"
                                                        DataTextField="ClassID" OnSelectedIndexChanged="rbTestType_SelectedIndexChanged"
                                                        ToolTip="select class id" />
                                                    <telerik:RadComboBox ID="cbTestType" runat="server" EmptyMessage="- test type -"
                                                        ToolTip="select test type" Width="100" MaxHeight="300px" AutoPostBack="True"
                                                        OnSelectedIndexChanged="rbTestType_SelectedIndexChanged">
                                                        <Items>
                                                            <telerik:RadComboBoxItem runat="server" Text="TU Test" Value="TUTest" />
                                                            <telerik:RadComboBoxItem runat="server" Text="Non-TU Test" Value="Manual" />
                                                        </Items>
                                                    </telerik:RadComboBox>
                                                    <telerik:RadComboBox ID="cbLoadTest" runat="server" EmptyMessage="- test name -"
                                                        ToolTip="select test name" DropDownAutoWidth="Enabled" MaxHeight="300" AutoPostBack="true"
                                                        DataValueField="TestTypeID" DataTextField="TestName" OnSelectedIndexChanged="cbLoadTest_SelectedIndexChanged" />
                                                </td>
                                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                                    <div style="vertical-align: bottom" align="right">
                                                        <asp:LinkButton runat="server" ID="btnExportTestScore" CssClass="linkBtn" OnClick="btnExportTestScore_Click"
                                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export Test Score list to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <telerik:RadGrid runat="server" ID="gridTestParticipants" AutoGenerateColumns="False"
                                            GridLines="None" AllowFilteringByColumn="True" EnableLinqExpressions="False"
                                            OnItemDataBound="gridTestParticipants_ItemDataBound" OnNeedDataSource="gridTestParticipants_NeedDataSource"
                                            OnItemCommand="gridTestParticipants_ItemCommand" CssClass="RadGrid1" Skin="Metro"
                                            AllowPaging="True" AllowSorting="True" PageSize="15">
                                            <FilterItemStyle HorizontalAlign="Center" />
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                            <GroupingSettings CaseSensitive="False" />
                                            <MasterTableView ShowHeader="true" AutoGenerateColumns="False" EditMode="EditForms"
                                                NoMasterRecordsText="Participant list is empty." EnableNoRecordsTemplate="True"
                                                DataKeyNames="TestScoresID,ParticipantID,NameAndCim,TestStatus" ShowHeadersWhenNoRecords="True"
                                                TableLayout="Auto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                AlternatingItemStyle-HorizontalAlign="Center">
                                                <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                <Columns>
                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                        HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                    </telerik:GridEditCommandColumn>
                                                    <telerik:GridBoundColumn UniqueName="ParticipantCIM" DataField="ParticipantCIM" HeaderText="Participant CIM"
                                                        AllowFiltering="False" />
                                                    <telerik:GridBoundColumn UniqueName="ParticipantName" DataField="ParticipantName"
                                                        HeaderText="Participant Name" FilterControlWidth="120" AutoPostBackOnFilter="True"
                                                        ShowFilterIcon="False" CurrentFilterFunction="Contains" />
                                                    <telerik:GridBoundColumn UniqueName="ParticipantStatus" DataField="ParticipantStatus"
                                                        HeaderText="Participant Status" FilterControlWidth="100" AutoPostBackOnFilter="True"
                                                        ShowFilterIcon="False" CurrentFilterFunction="Contains" />
                                                    <telerik:GridBoundColumn UniqueName="TestScore" DataField="TestScore" HeaderText="Test Score"
                                                        AllowFiltering="False" />
                                                    <telerik:GridBoundColumn UniqueName="TestDate" DataField="TestDate" HeaderText="Test Date Taken"
                                                        AllowFiltering="False" />
                                                    <telerik:GridBoundColumn UniqueName="TestStatusString" DataField="TestStatusString"
                                                        HeaderText="Passed?" AllowFiltering="False" />
                                                </Columns>
                                                <EditFormSettings EditFormType="Template">
                                                    <FormTemplate>
                                                        <asp:Panel runat="server" DefaultButton="bntUpdateScore">
                                                            <table id="tblEditMode" class="tabledefault">
                                                                <tr>
                                                                    <td class="tblLabel">
                                                                    </td>
                                                                    <td class="tblfield">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tblLabel">
                                                                        Test Score
                                                                    </td>
                                                                    <td class="tblfield">
                                                                        <telerik:RadNumericTextBox ID="txteditTestScore" runat="server" MinValue="0" MaxValue="100"
                                                                            MaxLength="5" SelectionOnFocus="SelectAll" Text='<%# Eval("TestScore1") %>'>
                                                                            <NumberFormat GroupSeparator="" />
                                                                        </telerik:RadNumericTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tblLabel">
                                                                        Test Date Taken
                                                                    </td>
                                                                    <td class="tblfield">
                                                                        <telerik:RadDatePicker ID="dpTestDate" runat="server" EnableTyping="False" ShowPopupOnFocus="True"
                                                                            DbSelectedDate='<%# Bind("TestDate") %>' Width="140">
                                                                            <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                            <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                        </telerik:RadDatePicker>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tblLabel">
                                                                        Passed?
                                                                    </td>
                                                                    <td class="tblfield">
                                                                        <telerik:RadComboBox ID="cbTestStatus" runat="server" EmptyMessage="- Passed? -"
                                                                            Width="100" MaxHeight="300" DataSourceID="dsTestStatus" DataValueField="TestStatus"
                                                                            DataTextField="TestStatusString" ToolTip="Passed?" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tblLabel">
                                                                    </td>
                                                                    <td class="tblfield">
                                                                        <br />
                                                                        <asp:LinkButton ID="bntUpdateScore" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                            runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                                                            CssClass="linkBtn">
                                                                        </asp:LinkButton>
                                                                        |
                                                                        <asp:LinkButton ID="btnScoreCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                            CommandName="Cancel" CssClass="linkBtn" />
                                                                        <br />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </FormTemplate>
                                                </EditFormSettings>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </div>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvJournal" runat="server">
                                <div class="paneldefault">
                                    <div class="insidecontainer">
                                        <div style="padding-bottom: 10px; vertical-align: middle">
                                            <label>
                                                Select filters:
                                            </label>
                                            <telerik:RadComboBox ID="cbClassIDJournal" runat="server" EmptyMessage="- class id -"
                                                Width="100" MaxHeight="300" AutoPostBack="true" DataSourceID="dsClassID" DataValueField="ClassID"
                                                DataTextField="ClassID" OnSelectedIndexChanged="cbClassIDJournal_SelectedIndexChanged"
                                                ToolTip="Select Class ID" />
                                            <telerik:RadDatePicker ID="dtJournalDate" runat="server" AutoPostBack="true" OnSelectedDateChanged="cbClassIDJournal_SelectedIndexChanged"
                                                Width="140" ToolTip="Select Month and Year">
                                                <DateInput ID="DateInput2" DateFormat="MMMM, yyyy" runat="server" Enabled="true" />
                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <telerik:RadGrid runat="server" ID="gridJournals" AutoGenerateColumns="False" GridLines="None"
                                            AllowFilteringByColumn="False" EnableLinqExpressions="False" CssClass="RadGrid1"
                                            Skin="Metro" OnNeedDataSource="gridJournals_NeedDataSource" OnItemDataBound="gridJournals_ItemDataBound"
                                            AllowSorting="True" AllowPaging="False">
                                            <GroupingSettings CaseSensitive="false" />
                                            <FilterItemStyle HorizontalAlign="Center" />
                                            <ClientSettings EnableRowHoverStyle="True">
                                                <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                <Selecting AllowRowSelect="True" />
                                            </ClientSettings>
                                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="JournalDate,Successes,CommonChallenges,DebriefTopics,ActionPlans,Successes_Content,CommonChallenges_Content,DebriefTopics_Content,ActionPlans_Content"
                                                NoDetailRecordsText="Journal is empty." NoMasterRecordsText="Journal is empty."
                                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                <Columns>
                                                    <telerik:GridBoundColumn UniqueName="JournalDate" DataField="JournalDate" HeaderText="Journal Date"
                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200" ItemStyle-HorizontalAlign="Center" />
                                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="Successes"
                                                        HeaderText="Successes" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnSuccesses0" runat="server" Text="Add" ToolTip="Add Journal - Successes"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click" CssClass="linkBtn" />
                                                            <asp:LinkButton ID="btnSuccesses1" runat="server" ToolTip="Edit Journal - Successes"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click">
                                                                            <img style="border:0;" alt="" src="Images/edit1.png"/>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="CommonChallenges"
                                                        HeaderText="Common Challenges" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnCommonChallenges0" runat="server" Text="Add" ToolTip="Add Journal - Common Challenges"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click" CssClass="linkBtn" />
                                                            <asp:LinkButton ID="btnCommonChallenges1" runat="server" ToolTip="Edit Journal - Common Challenges"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click">
                                                                            <img style="border:0;" alt="" src="Images/edit1.png"/>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="DebriefTopics"
                                                        HeaderText="Debrief Topics" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDebriefTopics0" runat="server" Text="Add" ToolTip="Add Journal - Debrief Topics"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click" CssClass="linkBtn" />
                                                            <asp:LinkButton ID="btnDebriefTopics1" runat="server" ToolTip="Edit Journal - Debrief Topics"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click">
                                                                            <img style="border:0;" alt="" src="Images/edit1.png" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" UniqueName="ActionPlans"
                                                        HeaderText="Action Plans" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="200"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnActionPlans0" runat="server" Text="Add" ToolTip="Add Journal - Action Plans"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click" CssClass="linkBtn" />
                                                            <asp:LinkButton ID="btnActionPlans1" runat="server" ToolTip="Edit Journal - Action Plans"
                                                                OnClientClick="View_Journal()" OnClick="btnLoadJournals_Click">
                                                                            <img style="border:0;" alt="" src="Images/edit1.png"/>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                        </telerik:RadGrid>
                                    </div>
                                </div>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="ctrPrereq" runat="server" />
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_PhilippineSites" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="CompanySiteID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTrainingType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_TrainingTypes" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="ClientID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTrainingLocType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_TrainingLocType" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTrainingHours" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_TrainingHours" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTrainingClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand=" SELECT ClientID = Id, Client = ClientName 
            FROM [INTRANET].[dbo].[ref_TranscomUniversity_Class_Client] 
            WHERE [Active] = 1 ORDER BY [ClientName]" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsNustar" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_Nustar" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsQualificationType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_QualificationType" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClassID" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_ClassDetails" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="TrainerCim" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="ViewType" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="ClassIDOnly" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClassID1" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_ClassDetails" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="TrainerCim" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="ViewType" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="ClassIDOnly" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsParticipantReason" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_ParticipantReason" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="isReason" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsParticipantDesc" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_ParticipantReason" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbInactiveReason" DefaultValue="0" Name="ReasonID"
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsParticipantOtherDesc" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select count(*) FROM tbl_Infinity_lkp_ParticipantDesc where [descid] = @DescID and allowothers = 0">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbInactiveDesc" DefaultValue="0" Name="DescID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClassNames" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT DISTINCT [ID], [ClassName] FROM [ref_TranscomUniversity_Class] 
            WHERE (([Active] = 1) AND ([ClientID] = @ClientID) AND ([TrainingTypeID] = @TrainingTypeID)) order by ClassName">
            <SelectParameters>
                <asp:ControlParameter ControlID="RadClient" DefaultValue="0" Name="ClientID" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddTrainingType" DefaultValue="0" Name="TrainingTypeID"
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsAttendanceStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select attendancestatusid,attendancestatus,hidefromlist from tbl_Infinity_Lkp_AttendanceStatus
          where hidefromlist = 0" SelectCommandType="Text"></asp:SqlDataSource>
        <asp:SqlDataSource ID="dsActiveParticipants" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_ActiveParticipants" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="ClassID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTestsTaken" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_Infinity_Lkp_TestsTaken" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="ClassID" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="TestType" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select 'Active' as ClassStatus union all select 'Inactive'as ClassStatus" />
        <asp:SqlDataSource ID="dsTestStatus" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select CAST(1 AS BIT) 'TestStatus', 'Yes' 'TestStatusString'
                            UNION
                           select CAST(0 AS BIT), 'No'" />
        <asp:SqlDataSource ID="dsEditClassNames" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT DISTINCT [ID], [ClassName] FROM [ref_TranscomUniversity_Class] 
                                                                                WHERE (([Active] = 1) AND ([ClientID] = @ClientID) AND ([TrainingTypeID] = @TrainingTypeID))">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="ClientID" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="TrainingTypeID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
