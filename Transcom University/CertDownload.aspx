﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CertDownload.aspx.cs" Inherits="CertDownload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="certificate" style="background-image: url('Images/LawPilotsCert.png'); background-repeat: no-repeat; height: 900px; width:1300px">
            <div style=" position: relative; top: 550px; text-align: center; width: 100%;">
                <asp:Label ID="lblName" runat="server" Font-Bold="true" Font-Names="Time New Roman" Font-Size="XX-Large"></asp:Label>
            </div>
            <div style="position: relative; left: 928px; top: 720px;">
                <asp:Label ID="lblIssueDate" runat="server" Font-Bold="true" Font-Names="Times New Roman" Font-Size="Large"></asp:Label>
            </div>
            <div style=" position: relative; top: 793px; left: 40px;">
                <asp:Label ID="lblAttemptID" Text="Attempt ID" runat="server" Font-Size="8pt" />
            </div>
        </div>
    </form>
</body>
</html>
