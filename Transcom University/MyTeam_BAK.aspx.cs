﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Linq;

public partial class MyTeam : Page
{
    private MasterPage _master;
    private DbHelper _db;

    private DataTable DtToAssign
    {
        get
        {
            return ViewState["dtToAssign"] as DataTable;
        }
        set
        {
            ViewState["dtToAssign"] = value;
        }
    }

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["TwwIdSession"] = value; }
    }

    public static int TwwIdTranscript
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdTranscriptSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["TwwIdTranscriptSession"] = value; }
    }

    public static int TwwIdLearningPlan
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdLearningPlanSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["TwwIdLearningPlanSession"] = value; }
    }

    public static int EnrolmentCim
    {
        get
        {
            var value = HttpContext.Current.Session["EnrolmentCimSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["EnrolmentCimSession"] = value; }
    }

    public static int CourseForApproval
    {
        get
        {
            var value = HttpContext.Current.Session["ForApprovalSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ForApprovalSession"] = value; }
    }

    public static int DataKeyEnrolmentId
    {
        get
        {
            var value = HttpContext.Current.Session["ParamEnrolmentIdSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ParamEnrolmentIdSession"] = value; }
    }

    public static int DataKeyEnroleeCim
    {
        get
        {
            var value = HttpContext.Current.Session["ParamEnroleeCimSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ParamEnroleeCimSession"] = value; }
    }

    public static string DataKeyTitle
    {
        get
        {
            var value = HttpContext.Current.Session["ParamTitleSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["ParamTitleSession"] = value; }
    }

    public static string DataKeyDescription
    {
        get
        {
            var value = HttpContext.Current.Session["ParamDescriptionSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["ParamDescriptionSession"] = value; }
    }

    public static int CtrPrerequisite
    {
        get
        {
            var value = HttpContext.Current.Session["ctrPrerequisite"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ctrPrerequisite"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {

            hidCIM.Value = User.Identity.Name;
            TwwId = "";
            TwwIdTranscript = 0;
            TwwIdLearningPlan = 0;
            EnrolmentCim = 0;
            CourseForApproval = 0;
            DataKeyEnrolmentId = 0;
            DataKeyEnroleeCim = 0;
            DataKeyTitle = "";
            DataKeyDescription = "";
            CtrPrerequisite = 0;

            PopulateComboBoxCategory(cbCategory);
            GetGlobals();
            SetPage_RolePreviledges();
            cbDirectReports.Visible = false;
            cbAssignmentType.SelectedValue = "3";
            setAssignToSource();
        }
    }

    private void GetGlobals()
    {
        if (!string.IsNullOrEmpty(Context.User.Identity.Name))
        {
            TwwId = Context.User.Identity.Name;
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (!Roles.IsUserInRole("Manager") && !Roles.IsUserInRole("Trainer") && !Roles.IsUserInRole("Admin"))
        {
            Response.Redirect("NoAccess.aspx");
        }
    }

    //NEW JAY
    protected void btnProfile_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;

        var twwid = (int)item.GetDataKeyValue("TwwId");

        var ds = DataHelper.GetUserProfile(twwid);
        DetailsView_Profile.DataSource = ds;
        DetailsView_Profile.DataBind();
    }

    protected void btnTranscript_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;

        var twwid = item.GetDataKeyValue("TwwId").ToString();
        var fname = item.GetDataKeyValue("Firstname").ToString();
        var lname = item.GetDataKeyValue("Lastname").ToString();

        lblTranscriptName.Text = fname + " " + lname + " (" + twwid + ")";

        TwwIdTranscript = Convert.ToInt32(twwid);
        gridTranscript.Rebind();
    }

    protected void GridTranscriptNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var db = new DbHelper("Intranet");

        var data = db.GetCoursesEmployee(TwwIdTranscript.ToString());
        if (data != null)
        {
            gridTranscript.DataSource = data;
        }
    }

    protected void GridTranscriptItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridDataItem;
            var testCategoryId = dataItem.GetDataKeyValue("TestCategoryID").ToString();
            var btnPreviousScore = e.Item.FindControl("btnPreviousScore") as LinkButton;
            btnPreviousScore.Visible = Convert.ToUInt32(testCategoryId) > 0;
        }
    }

    protected void btnLearningPlan_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;

        var twwid = item.GetDataKeyValue("TwwId").ToString();
        var fname = item.GetDataKeyValue("Firstname").ToString();
        var lname = item.GetDataKeyValue("Lastname").ToString();

        lblPlanName.Text = fname + " " + lname + " (" + twwid + ")";

        TwwIdLearningPlan = Convert.ToInt32(twwid);
        gridLearningPlan.Rebind();
    }

    protected void GridLearningPlanNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        //new logic
        //DataTable dt = LogicHelper.ConvertToDataTable(DataHelper.GetEnrolledAssignedEncryptedIDs(Convert.ToInt32(TwwIdLearningPlan)));
        //dt.TableName = "encCourseIDs";

        //for (var i = 0; i < dt.Rows.Count; i++)
        //{
        //    if (!string.IsNullOrEmpty(dt.Rows[i][1].ToString()))
        //    {
        //        dt.Rows[i][1] = UTF8.DecryptText(HttpUtility.UrlDecode(dt.Rows[i][1].ToString()));
        //    }
        //    else
        //    {
        //        dt.Rows[i][1] = 0;
        //    }
        //}

        //string strXml;
        //using (var sw = new StringWriter())
        //{
        //    dt.WriteXml(sw);
        //    strXml = sw.ToString();
        //}
        //end new logic

        var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(TwwIdLearningPlan));

        if (data != null)
        {
            gridLearningPlan.DataSource = data;
        }
    }

    protected void GridLearningPlanItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = (GridDataItem)e.Item;

            var title = dataItem.GetDataKeyValue("Title").ToString();

            if (dataItem["DueDate"].Text == "&nbsp;")
            {
                dataItem["DueDate"].Text = "N/A";
            }

            dataItem.ToolTip = title;
        }
    }

    protected void gridMyTeam_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var ds = DataHelper.GetTeamProfiles(int.Parse(TwwId));
        gridMyTeam.DataSource = ds;

        btnExport.Visible = ds.Count > 0;

        var allEnrollmentCnt = DataHelper.CountEnrollments_Sup_ForApproval(Convert.ToInt32(TwwId)); // all enrollment under supervisor
        CourseForApproval = allEnrollmentCnt;
    }

    protected void gridMyTeam_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName)
        {
            foreach (GridItem item in e.Item.OwnerTableView.Items)
            {
                if (item.Expanded && item != e.Item)
                {
                    item.Expanded = false;
                }
            }
        }
    }

    //protected void gridMyTeam_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    //{
    //    var dataItem = e.DetailTableView.ParentItem;

    //    switch (e.DetailTableView.Name)
    //    {
    //        case "gridTranscipt":
    //            {
    //                var userId = dataItem.GetDataKeyValue("TwwId").ToString();
    //                var db = new DbHelper("NuSkillCheck");
    //                e.DetailTableView.DataSource = db.getCoursesEmployee(userId);
    //                break;
    //            }
    //    }
    //}

    protected void btnExport_Click(object sender, EventArgs e)
    {
        var dt = LogicHelper.ConvertToDataTable(DataHelper.GetTeam_Report(int.Parse(TwwId), true));

        var ds = DataHelper.GetTeamExtract(dt,Convert.ToInt32(TwwId));
        ExcelHelper.ToExcel(ds, "My_Team_" + TwwId + "_" + DateTime.Now + ".xls", Page.Response);
    }

    protected void btnExportTranscript_Click(object sender, EventArgs e)
    {
        gridTranscript.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridTranscript.ExportSettings.FileName = "Transcript - " + lblTranscriptName.Text + " - " + DateTime.Now;
        ConfigureExport(gridTranscript);
    }

    protected void btnExportPlan_Click(object sender, EventArgs e)
    {
        gridLearningPlan.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridLearningPlan.ExportSettings.FileName = "LearningPlan - " + lblPlanName.Text + " - " + DateTime.Now;
        ConfigureExport(gridLearningPlan);
    }

    public void ConfigureExport(RadGrid grid)
    {
        grid.ExportSettings.ExportOnlyData = true;
        grid.ExportSettings.IgnorePaging = true;
        grid.ExportSettings.OpenInNewWindow = true;
        grid.ExportSettings.UseItemStyles = true;
        grid.MasterTableView.ExportToExcel();
    }

    protected void btnPreviousScore_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var testCategoryId = item.GetDataKeyValue("TestCategoryID").ToString();
        var courseName = item.GetDataKeyValue("CourseName").ToString();

        var qryList = DataHelper.GetCourseHistoryByCategoryId(TwwId, Convert.ToInt32(testCategoryId));
        if (qryList != null)
        {
            lblCourseName.Text = courseName;
            gridPreviousScores.DataSource = qryList;
            gridPreviousScores.DataBind();
        }
    }

    protected void gridMyTeam_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridDataItem;
            var twwid = dataItem.GetDataKeyValue("TwwId").ToString();
            var btn = (LinkButton)dataItem["PendingEnrolments"].FindControl("btnEnrolmentCount");
            //var enrolmentCol = gridMyTeam.MasterTableView.GetColumn("PendingEnrolments");

            var enrollmentPerEmployeeCnt = DataHelper.CountEnrollments_Emp_ForApproval(Convert.ToInt32(twwid)); // enrollment per employee

            if (CourseForApproval > 0)
            {
                //enrolmentCol.Display = true;

                if (enrollmentPerEmployeeCnt > 0)
                {
                    btn.Visible = true;
                    btn.Text = enrollmentPerEmployeeCnt.ToString();
                }
                else
                {
                    btn.Visible = false;
                }
            }
            else
            {
                //enrolmentCol.Display = false;
                btn.Visible = false;
            }
        }
        //if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "gridTranscipt")
        //{
        //    var dataItem = e.Item as GridDataItem;
        //    var testCategoryId = dataItem.GetDataKeyValue("TestCategoryID").ToString();
        //    var btnPreviousScore = e.Item.FindControl("btnPreviousScore") as LinkButton;
        //    btnPreviousScore.Visible = Convert.ToUInt32(testCategoryId) > 0;
        //}
    }

    protected void btnEnrolmentCount_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var twwid = item.GetDataKeyValue("TwwId").ToString();
        var fname = item.GetDataKeyValue("Firstname").ToString();
        var lname = item.GetDataKeyValue("Lastname").ToString();

        lblNameEnrollment.Text = fname + " " + lname + " (" + twwid + ")";

        EnrolmentCim = Convert.ToInt32(twwid);
        gridEnrolmentForApproval.Rebind();
    }

    protected void gridEnrolmentForApproval_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var data = DataHelper.GetCourseEnrolment_ByStatus(1, EnrolmentCim).ToList();
        gridEnrolmentForApproval.DataSource = data;
    }

    protected void btnApproveCancel_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        if (item != null)
        {
            DataKeyEnrolmentId = Convert.ToInt32(item.GetDataKeyValue("EnrollmentId").ToString());
            DataKeyEnroleeCim = Convert.ToInt32(item.GetDataKeyValue("EnrolleeCim").ToString());
            DataKeyTitle = item.GetDataKeyValue("Title").ToString();
            DataKeyDescription = item.GetDataKeyValue("Description").ToString();

            //var cor = string.Format("Course: {0}", DataKeyTitle);
            var cor = DataKeyTitle;

            if (btn.ID == "btnApprove")
            {
                lblCourse1.Text = cor;
            }
            else
            {
                lblCourse2.Text = cor;
            }
        }
    }

    protected void btnApproveEnrolment_Click(object sender, EventArgs e)
    {
        var retVal = DataHelper.UpdateCourseEnrolmentStatus(DataKeyEnrolmentId, txtAdvisory_Approve.Text, 2, Convert.ToInt32(TwwId), true);
        var master = Master;

        if (retVal)
        {
            var supQry = DataHelper.GetSupervisorDetails(DataKeyEnroleeCim);

            if (supQry != null)
            {
                const string msg = "Your course enrollment has been approved.";

                var recipient = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                var owner = SendEmail.ValidateAddress(supQry.Email1) ? supQry.Email1 : supQry.Email1_fallback;

                var titleLink = "<a href='http://transcomuniversity.com/MyPlan.aspx' target='_blank'>" + DataKeyTitle + "</a>";

                var comment = string.IsNullOrEmpty(txtAdvisory_Approve.Text) ? "" : txtAdvisory_Approve.Text;

                var email = SendEmail.SendEmail_CourseEnrolment(owner, recipient, msg, comment, titleLink, DataKeyDescription);

                const string alert = "An email alert has been sent to enrollee.";

                master.AlertMessage(true, alert);

                txtAdvisory_Approve.Text = "";

                gridEnrolmentForApproval.Rebind();
                windowCourseEnrollment.VisibleOnPageLoad = true;
            }
            else
            {
                master.AlertMessage(false, "Request failed. Please try again.");
            }
        }
        else
        {
            master.AlertMessage(false, "Request failed. Please try again.");
        }
    }

    protected void btnCancelEnrolment_Click(object sender, EventArgs e)
    {
        var retVal = DataHelper.UpdateCourseEnrolmentStatus(DataKeyEnrolmentId, txtAdvisory_Deny.Text, 3, Convert.ToInt32(TwwId), false);
        var master = Master;

        if (retVal)
        {
            var supQry = DataHelper.GetSupervisorDetails(DataKeyEnroleeCim);

            if (supQry != null)
            {
                const string msg = "Your course enrollment has been declined. Kindly contact your manager for more details.";

                var recipient = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                var owner = SendEmail.ValidateAddress(supQry.Email1) ? supQry.Email1 : supQry.Email1_fallback;

                var titleLink = "<a href='http://transcomuniversity.com/MyPlan.aspx' target='_blank'>" + DataKeyTitle + "</a>";

                var comment = string.IsNullOrEmpty(txtAdvisory_Deny.Text) ? "" : txtAdvisory_Deny.Text;

                var email = SendEmail.SendEmail_CourseEnrolment(owner, recipient, msg, comment, titleLink, DataKeyDescription);

                const string alert = "An email alert has been sent to enrollee.";

                master.AlertMessage(true, alert);

                txtAdvisory_Deny.Text = "";

                gridEnrolmentForApproval.Rebind();
                windowCourseEnrollment.VisibleOnPageLoad = true;
            }
            else
            {
                master.AlertMessage(false, "Request failed. Please try again.");
            }
        }
        else
        {
            master.AlertMessage(false, "Request failed. Please try again.");
        }
    }

    #region .ASSIGN COURSE

    protected void gridAssignedCourse_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (cbAssignmentType.SelectedValue == string.Empty)
        {
            gridAssignedCourse.DataSource = new int[] { };
        }
        else
        {
            var ds = DataHelper.GetMyTeamAssignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue) == 3 ? Convert.ToInt32(TwwId) : Convert.ToInt32(cbDirectReports.SelectedValue == "" ? "0" : cbDirectReports.SelectedValue),
           true, Convert.ToInt32(cbAssignmentType.SelectedValue));
            gridAssignedCourse.DataSource = ds;
        }

    }

    protected void chkElapsed_CheckedChanged(object sender, EventArgs e)
    {
        gridAssignedCourse.Rebind();
    }

    protected void btnAssignCourse_Click(object sender, EventArgs e)
    {
        _master = Master;
        if (lbUnassignedCourses.Items.Count > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                     "OpenAssignForm();", true);
        }
        else
        {
            _master.AlertMessage(false, "No available course to assign from the selected category/subcategory.");
        }
    }

    protected void setAssignToSource()
    {

        lbUnassignedCourses.DataSource = DataHelper.GetUnassignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue), Convert.ToInt32(cbAssignmentType.SelectedValue) == 3 ?
            Convert.ToInt32(TwwId) : Convert.ToInt32(cbDirectReports.SelectedValue), cbCategory.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategory.SelectedValue), ddtSubcategory.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategory.SelectedValue));

        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();
    }

    protected void lbUnassignedCourses_ItemDataBound(object sender, RadListBoxItemEventArgs e)
    {
        e.Item.Text = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title;
        e.Item.Value = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).CourseID.ToString();
        e.Item.Attributes.Add("Title", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title);
        e.Item.Attributes.Add("Prerequisite", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Prerequisite);
        e.Item.DataBind();
    }
    
    protected void lbUnassignedCourses_Transferred(object sender, RadListBoxTransferredEventArgs e)
    {
        foreach (RadListBoxItem item in e.Items)
        {
            item.DataBind();
            if (item.Attributes["Prerequisite"] == "Yes")
            {
                if (e.DestinationListBox.ID == "lbAssignedCourses")
                {
                    CtrPrerequisite++;
                }
                else
                {
                    CtrPrerequisite--;
                }
            }
        }
        lbUnassignedCourses.SelectedIndex = -1;
        ctrPrereq.Value = CtrPrerequisite.ToString();
    }

    protected void lbUnassignedCourses_Inserted(object sender, RadListBoxEventArgs e)
    {
        lbUnassignedCourses.SortItems();
    }

    protected void btnAssignCourseCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseAssignForm();", true);
    }

    protected void gridAssignedCourse_ItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var courseAssignmentId = (int)deletedItem.GetDataKeyValue("CourseAssignmentID");

                var retVal = DataHelper.DeleteCourseAssignment(courseAssignmentId, Convert.ToInt32(TwwId));

                if (retVal)
                {
                    gridAssignedCourse.Rebind();
                    setAssignToSource();
                }
                else
                {
                    _master.AlertMessage(false, "Request failed. Please try again.");
                }
            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var courseAssignmentId = (int)editedItem.GetDataKeyValue("CourseAssignmentID");

                var dpDueDateEdit = (RadDatePicker)editedItem.FindControl("dpDueDateEdit");

                var retVal = DataHelper.UpdateCourseAssignment(Convert.ToDateTime(dpDueDateEdit.SelectedDate), Convert.ToInt32(courseAssignmentId), int.Parse(TwwId));

                switch (retVal)
                {
                    case true:
                        _master.AlertMessage(true, "Assignment records have been updated.");

                        gridAssignedCourse.Rebind();
                        break;
                    case false:
                        _master.AlertMessage(false, "Request failed. Please try again.");
                        break;
                }
            }
        }
    }
    protected void CbCategorySelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
        setAssignToSource();
    }

    public void PopulateDropdownTreeSubcategories(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1)
    {
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue)
                             ? Convert.ToInt32(cbCategory1.SelectedValue)
                             : (int?)null;
        ddtSubcategory1.DataSource = GetSubcategory_Courses_Active(categoryId, "Internal");
        ddtSubcategory1.DataBind();
    }

    private DataSet GetSubcategory_Courses_Active(int? categoryId, string accessmode2)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetSubcategory_CoursesActive(categoryId, accessmode2);
        return ds;
    }

    protected void ddtSubcategory_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        setAssignToSource();
    }

    protected void PopulateComboBoxCategory(RadComboBox combo)
    {
        combo.Text = "";
        combo.Items.Clear();

        var ds = DataHelper.GetCategories("Admin");
        combo.DataSource = ds;
        combo.DataBind();
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "RebindAssign")
        {
            if (ddtSubcategory.SelectedText != string.Empty)
            {
                setAssignToSource();
                gridAssignedCourse.Rebind();
            }
        }
        else if (e.Argument == "AddAssignCourse")
        {
            AssignCourseAdd();
        }
    }

    protected void cbAssignmentType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        switch (Convert.ToInt32(cbAssignmentType.SelectedValue))
        {
            case 3:
                cbDirectReports.Visible = false;
                setAssignToSource();
                btnAssignCourse.Visible = true;
                gridAssignedCourse.Rebind();
                break;
            case 5:
                cbDirectReports.SelectedIndex = -1;
                cbDirectReports.Text = string.Empty;
                cbDirectReports.Visible = true;
                btnAssignCourse.Visible = false;
                gridAssignedCourse.Rebind();
                break;
        }

        cbDirectReports.SelectedIndex = -1;
    }

    protected void cbDirectReports_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        setAssignToSource();
        btnAssignCourse.Visible = true;
        gridAssignedCourse.Rebind();
    }

    protected void AssignCourseAdd()
    {
        var msg = string.Empty;
        DtToAssign = new DataTable();
        DtToAssign.Columns.Add("CourseID", typeof(int));
        DtToAssign.Columns.Add("DueDate", typeof(DateTime));

        foreach (RadListBoxItem item in lbAssignedCourses.Items)
        {
            var dr = DtToAssign.NewRow();
            var dpCourseAssigned = (RadDatePicker)item.FindControl("dpCourseAssigned");
            dr["CourseID"] = item.Value;
            dr["DueDate"] = dpCourseAssigned.SelectedDate;
            DtToAssign.Rows.Add(dr);
        }

        var retVal = DataHelper.BulkAddAssignCourse(Convert.ToInt32(cbAssignmentType.SelectedValue),
                                                     Convert.ToInt32(cbAssignmentType.SelectedValue) == 3
                                                         ? Convert.ToInt32(TwwId)
                                                         : Convert.ToInt32(cbDirectReports.SelectedValue),
                                                     Convert.ToInt32(TwwId), DtToAssign);

        msg = retVal ? "The courses has been successfully assigned." : "Request failed. Please try again.";

        setAssignToSource();
        var script = @"function f() {
            Close('" + msg + @"'); 
            Sys.Application.remove_load(f);
            }
            Sys.Application.add_load(f);";

        CtrPrerequisite = 0;

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "key", script, true);
    }
    #endregion
}