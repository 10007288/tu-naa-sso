﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class ChangePassword : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var lv = (LoginView)Master.FindControl("LoginView1");
        lv.Visible = false;
    }

    protected void btnProceed_Click(object sender, EventArgs e)
    {


        var user = Context.User.Identity.Name;

        if (user != null)
        {
            var sSafeNewPassword = Server.HtmlEncode(txtNewPassword.Text);
            var sSafeOldPassword = Server.HtmlEncode(txtOldPassword.Text);

            var retVal = DataHelper.ChangePassword(user, sSafeNewPassword,sSafeOldPassword);
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                           "alert('" + retVal + "');", true);
            if (retVal == "Password Change Success.")
            {
                
                Response.Redirect("~/default.aspx");
            }
            else
            {
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtRetypePassword.Text = string.Empty;
            }
        }
        else
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            var redirectUrl = FormsAuthentication.LoginUrl + "?ReturnUrl=Default.aspx";
            FormsAuthentication.SignOut();
            Response.Redirect(redirectUrl);
        }
    }
}