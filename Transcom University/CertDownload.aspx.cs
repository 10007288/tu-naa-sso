﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class CertDownload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //int attemptID = Convert.ToInt32(Request["AttemptID"]);
            string attemptID = DbHelper.Decrypt(Request["AttemptID"]);

            //var name = Membership.GetUser().UserName;
            var name = Session["CIMNumber"].ToString();
            string empName;

            //external users
            if (name.Contains("@"))
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["Intranet"].ConnectionString))
                {
                    cn.Open();

                    string sql = @"pr_TranscomUniversity_GetNameByEmail";
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Email", name);
                    empName = (string)cmd.ExecuteScalar();
                }
            }
            else
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["Cimenterprise"].ConnectionString))
                {
                    cn.Open();

                    string sql = @"SELECT FirstName + ' '+ LastName FROM tbl_Personnel_Cor_Employee e WHERE e.CimNumber = @CimNumber";
                    SqlCommand cmd = new SqlCommand(sql, cn);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@CimNumber", name);
                    empName = (string)cmd.ExecuteScalar();
                }
            }

            lblIssueDate.Text = DateTime.Now.ToShortDateString();
            lblAttemptID.Text = attemptID.ToString();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "inline;filename=Certificate.pdf");

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            string imageFilePath = Server.MapPath(".") + "/Images/LawPilotsCert.png";

            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);

            // Page site and margin left, right, top, bottom is defined
            Document pdfDoc = new Document(null, 0, 0, 0, 0);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());

            //Resize image depend upon your need
            //For give the size to image
            jpg.ScaleToFit(775, 775);

            //If you want to choose image as background then,

            jpg.Alignment = iTextSharp.text.Image.UNDERLYING;

            //If you want to give absolute/specified fix position to image.
            jpg.SetAbsolutePosition(10, 0);

            PdfWriter pW = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();

            pdfDoc.NewPage();

            Font brown = new Font(Font.FontFamily.HELVETICA, 20f, Font.NORMAL, BaseColor.BLACK);
            Font lightblue = new Font(Font.FontFamily.COURIER, 9f, Font.NORMAL, new BaseColor(43, 145, 175));
            Font courier = new Font(Font.FontFamily.COURIER, 45f);
            Font tnr = FontFactory.GetFont("Times New Roman", 30f);
            tnr.Color = BaseColor.BLACK;

            Font georgiaName = new Font(Font.FontFamily.TIMES_ROMAN, 30f);

            Chunk c1 = new Chunk("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", tnr);
            Chunk c2 = new Chunk("\n\n\n\n\n\n\n\n\n", brown);
            Chunk c3 = new Chunk(empName, georgiaName);
            Chunk c4 = new Chunk("\n\n\n\n\n\n                                                                     ", brown);
            Chunk c5 = new Chunk(DateTime.Now.ToShortDateString());

            Phrase p2 = new Phrase();
            p2.Add(c1);
            p2.Add(c2);
            p2.Add(c3);
            p2.Add(c4);
            p2.Add(c5);

            Paragraph p = new Paragraph();
            p.Alignment = Element.ALIGN_CENTER;
            p.Add(p2);

            pdfDoc.Add(p);
            pdfDoc.Add(jpg);
            pdfDoc.Close();

            Response.Write(pdfDoc);
            Response.End();
        }
    }
}