﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="NoAccess.aspx.cs" Inherits="NoAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" Width="100%">
            <asp:TableRow ID="TableRow1" runat="server" VerticalAlign="Middle">
                <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <hr />
                    <br/>
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text="Access denied." Font-Size="Large"></asp:Label>
                    <br />
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <%--<asp:TableRow ID="TableRow2" runat="server" VerticalAlign="Bottom">
                <asp:TableCell ID="TableCell2" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <asp:LinkButton ID="btnExit" runat="server" Text="Home" OnClick="btnExit_Click" CssClass="linkBtn" />
                    <br />
                </asp:TableCell></asp:TableRow>--%>
        </asp:Table>
    </div>
</asp:Content>
