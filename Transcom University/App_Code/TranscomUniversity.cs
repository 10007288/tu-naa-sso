using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for TranscomUniversity
/// </summary>
public partial class TranscomUniversity
{
    public TranscomUniversity()
    {

    }

	public void Exit()
	{
        HttpContext.Current.Response.Redirect("Error.aspx");
    }
}
