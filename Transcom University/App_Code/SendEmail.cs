﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for SendEmail
/// </summary>
public class SendEmail
{
    public static string SendEmail_ReservationEvent(string recipient, string message, string trainingType, string className, string trainer, string site, string timeSlot, string dateFrom, string dateTo, string room, string coordinatorAdvisory, string courseDesc)
    {
        try
        {
            var msg = new MailMessage { From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]) };

            var addressList = recipient;
            foreach (var address in addressList.Split(';'))
            {
                var mailTo = new MailAddress(address);
                msg.To.Add(mailTo);
            }

            //msg.To.Add(recipient);
            var reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/MailFormat_ReservationEvents.htm"));
            var readFile = reader.ReadToEnd();
            var strContent = readFile;

            strContent = strContent.Replace("[Message]", message);
            strContent = strContent.Replace("[TrainingType]", trainingType);
            strContent = strContent.Replace("[ClassName]", className);
            strContent = strContent.Replace("[Trainer]", trainer);
            strContent = strContent.Replace("[Site]", site);
            strContent = strContent.Replace("[TimeSlot]", timeSlot);
            strContent = strContent.Replace("[Date]", dateFrom);
            strContent = strContent.Replace("[DateTo]", dateTo);
            strContent = strContent.Replace("[AssignedRoom]", room);
            strContent = strContent.Replace("[CoordinatorAdvisory]", coordinatorAdvisory);
            strContent = strContent.Replace("[CourseDescription]", courseDesc);

            msg.Subject = "Transcom University - Reservation & Event Notification";
            msg.Body = strContent;
            msg.IsBodyHtml = true;
            var smtp = new SmtpClient();
            smtp.Send(msg);
            return "Sent!";
        }
        catch (Exception)
        {
            return "Sending failed!";
        }
    }

    public static string SendEmail_CourseEnrolment(string from, string recipient, string message, string comment, string title, string desc)
    {
        try
        {
            MailMessage msg = !recipient.Contains(@from) ? new MailMessage { From = new MailAddress(@from) } : new MailMessage { From = new MailAddress("donotreply@transcom.com") };

            var addressList = recipient;
            foreach (var address in addressList.Split(';'))
            {
                var mailTo = new MailAddress(address);
                msg.To.Add(mailTo);
            }

            //msg.Bcc.Add("wilfredo.millare@transcom.com");

            var reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/MailFormat_CourseEnrolment.htm"));
            var readFile = reader.ReadToEnd();
            var strContent = readFile;

            strContent = strContent.Replace("[Message]", message);
            //strContent = strContent.Replace("[Comment]", comment);
            strContent = strContent.Replace("[Title]", title);
            strContent = strContent.Replace("[Description]", desc);

            msg.Subject = "Transcom University - Course Enrollment Notification";
            msg.Body = strContent;
            msg.IsBodyHtml = true;
            var smtp = new SmtpClient();
            smtp.Send(msg);
            return "Sent!";
        }
        catch (Exception)
        {
            return "Sending failed!";
        }
    }

    public static bool ValidateAddress(string email)
    {
        var regex = new Regex(@"^([\w\.\-]+)@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$");
        Match match = regex.Match(email);
        return match.Success;
    }
}