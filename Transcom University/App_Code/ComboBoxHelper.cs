﻿using Telerik.Web.UI;

/// <summary>
/// Summary description for ComboBoxHelper
/// </summary>
public class ComboBoxHelper
{
	public ComboBoxHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void Populate_TrainingType(RadComboBox cbTrainingType, string client)
    {
        cbTrainingType.DataSource = client == "OD" ? DataHelper.GetTrainingTypes_OD() : DataHelper.GetTrainingTypes();
        cbTrainingType.DataBind();
    }
}