﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TranscomUniversityUI
{
    public class CalendarProfessionalOnModel
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public string Site { get; set; }
        public string Room { get; set; }

        public string Time { get; set; }
        public string Facilitator { get; set; }
        public string Instruction { get; set; }
    }

    public class CalendarClientOnModel
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public string TraningClassName { get; set; }
        public string Room { get; set; }
        public string Time { get; set; }
        public string Facilitator { get; set; }
    }

    public class CalendarSiteOnModel
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public string TraningClassName { get; set; }
        public string Room { get; set; }
        public string Time { get; set; }
        public string Facilitator { get; set; }
    }

    public class CalendarRoomOnModel
    {
        public long ID { get; set; }
        public DateTime Date { get; set; }
        public string TraningClassName { get; set; }
        public string Room { get; set; }
        public string Time { get; set; }
        public string Facilitator { get; set; }
    }

    public class ClientOnModel
    {
        public int ClientId { get; set; }
        public string ClientDescription { get; set; }
    }

    public class SiteOnModel
    {
        public int SiteId { get; set; }
        public string SiteDescription { get; set; }
    }

    public class RoomOnModel
    {
        public short RoomId { get; set; }
        public string RoomDescription { get; set; }
    }

    public class CalendarOnClass
    {
        public CalendarOnClass()
        {
        }

        public static List<CalendarProfessionalOnModel> ProfessionalView(DateTime dayOftheWeek)
        {
            var db = new IntranetDBDataContext();

            try
            {
                var infos = (from r in db.pr_TranscomUniversity_Lkp_Calendar_ProfessionalView(dayOftheWeek.ToShortDateString())
                             //where r.Date.Day == dayOftheWeek.Day && r.Date.Month == dayOftheWeek.Month && r.Date.Year == dayOftheWeek.Year
                             select new CalendarProfessionalOnModel
                             {
                                 ID = r.ID,
                                 Date = r.Date,
                                 CourseName = r.CourseName ?? "(undefined)",
                                 Room = r.Room ?? "(undefined)",
                                 Time = r.Time ?? "(undefined)",
                                 Description = r.Description ?? "(undefined)",
                                 Instruction = r.Instruction ?? "(undefined)",
                                 Site = r.Site ?? "(undefined)",
                                 Facilitator = r.Facilitator ?? "(undefined)"
                             });

                return infos.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<CalendarClientOnModel> ClientView(DateTime dayOftheWeek, int? clientId)
        {
            var db = new IntranetDBDataContext();

            try
            {
                var infos = (from r in db.pr_TranscomUniversity_Lkp_Calendar_ClientView(dayOftheWeek.ToShortDateString(), clientId)
                             select new CalendarClientOnModel
                             {
                                 ID = r.ID,
                                 Date = r.Date,
                                 TraningClassName = r.TraningClassName ?? "(undefined)",
                                 Room = r.Room ?? "(undefined)",
                                 Time = r.Time ?? "(undefined)",
                                 Facilitator = r.Facilitator ?? "(undefined)"
                             });

                return infos.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<CalendarSiteOnModel> SiteView(DateTime dayOftheWeek, int? siteId)
        {
            var db = new IntranetDBDataContext();

            try
            {
                var infos = (from r in db.pr_TranscomUniversity_Lkp_Calendar_SiteView(dayOftheWeek.ToShortDateString(), siteId)
                             select new CalendarSiteOnModel
                             {
                                 ID = r.ID,
                                 Date = r.Date,
                                 TraningClassName = r.TraningClassName ?? "(undefined)",
                                 Room = r.Room ?? "(undefined)",
                                 Time = r.Time ?? "(undefined)",
                                 Facilitator = r.Facilitator ?? "(undefined)"
                             });

                return infos.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<CalendarRoomOnModel> RoomView(DateTime dayOftheWeek, short? roomId)
        {
            var db = new IntranetDBDataContext();

            try
            {
                var infos = (from r in db.pr_TranscomUniversity_Lkp_Calendar_RoomView(dayOftheWeek.ToShortDateString(), roomId)
                             select new CalendarRoomOnModel
                             {
                                 ID = r.ID,
                                 Date = r.Date,
                                 TraningClassName = r.TraningClassName ?? "(undefined)",
                                 Room = r.Room ?? "(undefined)",
                                 Time = r.Time ?? "(undefined)",
                                 Facilitator = r.Facilitator ?? "(undefined)"
                             });

                return infos.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public static List<DateTime> GetDateTaken(int month, int view)
        {
            var db = new IntranetDBDataContext();

            var _dates = new List<DateTime>();

            if (view == 1) //Professional Development
            {
                var dates = db.tbl_TranscomUniversity_Reservations
                    .Where(d => ((d.Date.Month == month && d.Date.Year == DateTime.Now.Year) || (d.DateTo.Value.Month == month && d.DateTo.Value.Year == DateTime.Now.Year))
                        && d.StatusId == 2 && d.TrainingTypeId == 5)
                    .Select(d => new
                    {
                        d.Date,
                        Diff = System.Data.Linq.SqlClient.SqlMethods.DateDiffDay(d.Date, d.DateTo)
                    })
                    .Distinct()
                    .ToList();

                foreach (var item in dates)
                {
                    int? diff = item.Diff + 1;

                    for (int i = 0; i < diff; i++)
                    {
                        _dates.Add(item.Date.AddDays(i));
                    }
                }
            }
            else //Client
            {
                var dates = db.tbl_TranscomUniversity_Reservations
                    .Where(d => ((d.Date.Month == month && d.Date.Year == DateTime.Now.Year) || (d.DateTo.Value.Month == month && d.DateTo.Value.Year == DateTime.Now.Year))
                        && d.StatusId == 2 && d.TrainingTypeId != 5)
                    .Select(d => new
                    {
                        d.Date,
                        Diff = System.Data.Linq.SqlClient.SqlMethods.DateDiffDay(d.Date, d.DateTo)
                    })
                    .Distinct()
                    .ToList();

                foreach (var item in dates)
                {
                    int? diff = item.Diff + 1;

                    for (int i = 0; i < diff; i++)
                    {
                        _dates.Add(item.Date.AddDays(i));
                    }
                }
            }

            return _dates.Distinct().ToList();
        }
    }

    public class ClientOnClass
    {
        public ClientOnClass()
        {
        }

        public static List<ClientOnModel> GetClients()
        {
            var db = new IntranetDBDataContext();

            try
            {
                var clients = db.ExecuteQuery<ClientOnModel>
                    (@"SELECT DISTINCT 
		                A.Id AS ClientID, 
		                A.ClientName AS ClientDescription
	                FROM 
		                [Intranet].[dbo].[ref_TranscomUniversity_Class_Client] A
	                WHERE A.Id IN
	                (
		                SELECT clientid
		                from dbo.tbl_TranscomUniversity_Reservations
		                WHERE statusid = 2
	                )
	                ORDER BY
		                A.ClientName").ToList();

                return clients;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class SiteOnClass
    {
        public SiteOnClass()
        {
        }

        public static List<SiteOnModel> GetSites()
        {
            var db = new IntranetDBDataContext();

            try
            {
                var sites = db.ExecuteQuery<SiteOnModel>
                    (@"SELECT DISTINCT
	                    [CompanySiteID] AS SiteId,
	                    [CompanySite] AS SiteDescription
                    FROM SUSL3PSQLDB02.CIMEnterprise.[dbo].[tbl_Personnel_Lkp_Country] C
                    INNER JOIN SUSL3PSQLDB02.CIMEnterprise.[dbo].[tbl_Personnel_Lkp_CompanyGroup] CG
	                    ON C.CountryID = CG.CountryID
	                    AND CG.HideFromList = 0
                    INNER JOIN SUSL3PSQLDB02.CIMEnterprise.[dbo].[tbl_Personnel_Lkp_CompanySite] CS
	                    ON CG.CompanyGroupID = CS.CompanyGroupID
	                    AND CS.HideFromList = 0
                    WHERE C.CountryID = 135
                    AND [CompanySiteID] IN (SELECT
	                    SiteId
                    FROM dbo.tbl_TranscomUniversity_Reservations
                    WHERE statusid = 2)
                    ORDER BY [CompanySite]").ToList();

                return sites;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class RoomOnClass
    {
        public RoomOnClass()
        {
        }

        public static List<RoomOnModel> GetRooms()
        {
            var db = new IntranetDBDataContext();

            try
            {
                var rooms = db.ExecuteQuery<RoomOnModel>
                    (@"SELECT DISTINCT
	                    A.[RoomId] AS RoomId,
	                    A.[Room] AS RoomDescription
                    FROM [INTRANET].[dbo].[ref_TranscomUniversity_RoomAssignment] AS A
                    WHERE A.[RoomId] IN (SELECT
	                    AssignedRoomId AS RoomId
                    FROM [INTRANET].[dbo].[tbl_TranscomUniversity_Reservations] AS R
                    INNER JOIN [INTRANET].[dbo].[tbl_TranscomUniversity_Events] AS E
	                    ON E.ReservationReqId = R.ReservationReqId
                    WHERE R.statusid = 2)
                    ORDER BY [Room]").ToList();

                return rooms;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}