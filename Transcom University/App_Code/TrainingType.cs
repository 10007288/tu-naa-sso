﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TranscomUniversityUI
{
    public class TrainingTypeOnModel
    {
        public int TrainingTypeId { get; set; }
        public string TrainingTypeDescription { get; set; }
    }

    public class TrainingTypeOnClass
    {
        public TrainingTypeOnClass()
        {
        }

        public static List<TrainingTypeOnModel> GetTrainingType()
        {
            var db = new IntranetDBDataContext();

            try
            {
                var types = db.ref_TranscomUniversity_TrainingTypes
                .Where(i => i.Active)
                .Select(i => new TrainingTypeOnModel
                {
                    TrainingTypeId = i.TrainingTypeId,
                    TrainingTypeDescription = i.TrainingType
                })
                .ToList();

                return types;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}