﻿using Microsoft.AspNet.Membership.OpenAuth;
using Microsoft.AspNet.Membership.OpenAuth.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Apis.Auth.OAuth2;
using DotNetOpenAuth.GoogleOAuth2;
using System.Web.Configuration;

/// <summary>
/// Summary description for AuthConfig
/// </summary>
public class AuthConfig
{
    public static void RegisterOpenAuth()
    {
        // See http://go.microsoft.com/fwlink/?LinkId=252803 for details on setting up this ASP.NET
        // application to support logging in via external services.

        //OpenAuth.AuthenticationClients.AddTwitter(
        //    consumerKey: "your Twitter consumer key",
        //    consumerSecret: "your Twitter consumer secret");

        //OpenAuth.AuthenticationClients.AddFacebook(
        //    appId: "your Facebook app id",
        //    appSecret: "your Facebook app secret");

        //OpenAuth.AuthenticationClients.AddMicrosoft(
        //    clientId: "your Microsoft account client id",
        //    clientSecret: "your Microsoft account client secret");
        //OpenAuth.AuthenticationClients.Add("Test Provider", null);
        string clientID = WebConfigurationManager.AppSettings["GoogleClientId"];
        string clientSecret = WebConfigurationManager.AppSettings["GoogleClientSecret"]; ;
        var client = new GoogleOAuth2Client(clientID, clientSecret);
        var extraData = new Dictionary<string, string>();
        OpenAuth.AuthenticationClients.Add("Google", () => client, extraData);
        //OpenAuth.AuthenticationClients.AddGoogle();
    }
}