﻿using System;
using System.Web.UI;
using TWW.Data.Intranet;
using NuComm.Security.Encryption;
using System.Web.Security;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;

public partial class CourseRedirect : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request["TestCategoryID"] != null)
            {
                string sDecryptedTestCategoryId = null;

                try
                {
                    sDecryptedTestCategoryId = Server.UrlDecode(UTF8.DecryptText(Request["TestCategoryID"]));
                }
                catch (Exception)
                {
                    Response.Redirect("Error.aspx");
                }

                SubSonic.Query qry = new SubSonic.Query(TblNuCommUniversityLkpCourse.Schema).WHERE("CourseID", sDecryptedTestCategoryId);

                //Check if there's existing courseid on testing data
                var iTestingRecordCount = qry.GetRecordCount();

                if (iTestingRecordCount > 0)
                {
                    if ((Request["TestCategoryID"] != null))
                    {
                        string sCourseId = null;

                        try
                        {
                            sCourseId = Server.UrlDecode(UTF8.DecryptText(Request["CourseID"]));
                        }
                        catch (Exception)
                        {
                            Response.Redirect("Error.aspx");
                        }

                        var colTranscomCourse = new TblTranscomUniversityCorCourseCollection().Where("CourseID", sCourseId).Load();
                        var colNucommCourse = new TblNuCommUniversityLkpCourseCollection();

                        colNucommCourse.LoadAndCloseReader(qry.ExecuteReader());

                        //Raymark - Get course path from nucomm university course lookup
                        if (colTranscomCourse.Count > 0)
                        {
                            try
                            {
                                //Fix Raymarks
                                string sPath = "";
                                string LkpAsiaCountry = Page.User.Identity.Name.Contains("@") ? string.Empty : DataHelper.GetEmployeeCompanySite(Convert.ToInt32(Session["CIMNumber"].ToString())).ToString();

                                foreach (TblNuCommUniversityLkpCourse item in colNucommCourse)
                                {
                                    if ("Philippines" == LkpAsiaCountry || "Australia" == LkpAsiaCountry)
                                    {
                                        //Asia
                                        sPath = item.CoursePathAsia ?? string.Empty;
                                    }
                                    //Default
                                    sPath = sPath == string.Empty ? item.CoursePath : sPath;
                                }

                                //if (Membership.GetUser().ProviderUserKey.ToString().Contains("@"))
                                if (Page.User.Identity.Name.Contains("@"))
                                {
                                    SPs.PrNuCommUniversitySavExternalUserCourseLogin(
                                        Page.User.Identity.Name.ToString(),
                                        Convert.ToInt32(sDecryptedTestCategoryId)).Execute();
                                }
                                else
                                {
                                    SPs.PrNuCommUniversitySavEmployeeCourseLogin(
                                        Convert.ToInt32(Page.User.Identity.Name),
                                        Convert.ToInt32(sDecryptedTestCategoryId)).Execute();
                                }
                                //Goto course page
                                Response.Redirect(sPath, true);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            lblNotes.Text = "Invalid Course!";
                        }
                    }
                    else
                    {
                        lblNotes.Text = "Invalid Test Category ID!";
                    }
                }
                else
                {
                    lblNotes.Text = "Invalid Course ID!";
                }
            }
        }
    }

    protected void btnExit_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}