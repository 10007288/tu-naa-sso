﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewProfile.aspx.cs" Inherits="ViewProfile" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function conditionalPostback(sender, args) {
                if ((args.get_eventTarget() == "<%= btnUpdateProfile.UniqueID %>") || (args.get_eventTarget() == "<%= btnUpdateProfile2.UniqueID %>")) {
                    args.set_enableAjax(false);
                }
            }
            function RequestFullScreen(sender, args) {
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollLeftOffset;
                var scrollTopOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnViewMyProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEditProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateProfile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelUpdate">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpdateProfile2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelUpdate2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="tblProfile" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnSearch">
            <div class="containerdefault">
                <%--<table runat="server" id="tblAdmin" visible="False" width="100%">
                        <tr>
                            <td align="left" style="width: 130px">
                                <telerik:RadTextBox ID="txtSearch" runat="server" Width="130px" ToolTip="Search by CIM/Username"
                                    SelectionOnFocus="SelectAll" />
                                <asp:RequiredFieldValidator ID="reqSearch" runat="server" ControlToValidate="txtSearch"
                                    ErrorMessage="" CssClass="displayerror" ValidationGroup="go" Display="Dynamic"
                                    SetFocusOnError="True" />
                            </td>
                            <td align="left" style="width: 30px">
                                <telerik:RadButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btnSearchImage"
                                    ValidationGroup="go">
                                    <Image EnableImageButton="true" />
                                </telerik:RadButton>
                            </td>
                            <td align="right">
                                <asp:LinkButton ID="btnViewProfileReport" runat="server" Text="View Profile Report >"
                                    PostBackUrl="Profile_Admin.aspx" CausesValidation="False" CssClass="linkBtn" />
                            </td>
                        </tr>
                    </table>--%>
                <div runat="server" id="tblAdmin" style="display: inline-block !important; padding-bottom: 5px">
                    <input type='text' placeholder='Search...' id='search-text-input' name="txtSearch"
                        value="<% =SearchValue %>" />
                    <div id='button-holder'>
                        <asp:ImageButton runat="server" ID="btnSearch" ImageUrl="Images/find.png" OnClick="btnSearch_Click"
                            CssClass="button-control" />
                    </div>
                </div>
                <div class="container">
                    <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel1" ClientEvents-OnRequestStart="conditionalPostback"
                        Width="100%">
                        <div style="border-style: inset; background-color: white; width: 165px; height: 166px;
                            border: 1px solid #999; float: left; position: absolute">
                            <asp:Image runat="server" ID="profilePhoto" alt="" Height="166" Width="165" />
                        </div>
                        <table width="100%" align="center" style="height: 170px">
                            <tr>
                                <%--<td style="width: 180px">--%>
                                <%--</td>--%>
                                <td align="center">
                                    <div>
                                        <asp:Label runat="server" ID="lblName" Font-Size="XX-Large" ForeColor="Black" />
                                    </div>
                                    <div>
                                        <asp:Label ID="lblRole" runat="server" />
                                    </div>
                                    <div>
                                        <asp:Label runat="server" ID="lblSite" />
                                    </div>
                                    <div>
                                    </div>
                                    <div style="padding-top: 15px">
                                        <div id="managecontrol2" style="width: 100%">
                                            <asp:LinkButton ID="btnEditProfile" runat="server" Text="Edit" OnClick="btnEditProfile_Click"
                                                CausesValidation="False" ToolTip="Edit my profile" Font-Size="7.5" />
                                            <asp:LinkButton runat="server" ID="btnUpdateProfile" Text="Update Profile" OnClick="btnUpdateProfile_Click"
                                                ToolTip="Update Profile" ValidationGroup="update" Font-Size="7.5" Visible="false" />
                                            <asp:LinkButton runat="server" ID="btnCancelUpdate" Text="View Profile" Font-Size="7.5"
                                                ToolTip="View Profile" CausesValidation="False" OnClick="btnViewMyProfile_Click"
                                                Visible="false" />
                                        </div>
                                    </div>
                                    <div runat="server" id="divUpload" visible="False">
                                        Photo:
                                        <asp:FileUpload ID="fileUpload" runat="server" ToolTip="Select your photo" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </div>
                <div style="padding-bottom: 20px">
                    <table runat="server" id="tblProfile" class="tableProfile">
                        <tr>
                            <td class="labelMyTeam">
                                CIM Number
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblTWWID" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblEditTWWID" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Gender
                            </td>
                            <td>
                                <asp:Label ID="lblGender" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditGender" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Date of Birth
                            </td>
                            <td>
                                <asp:Label ID="lblDateOfBirth" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditDateOfBirth" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Start Date with Transcom
                            </td>
                            <td>
                                <asp:Label ID="lblStartDateWithTranscom" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditStartDateWithTranscom" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Email Address
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblEmailAddress" />
                            </td>
                            <td>
                                <telerik:RadTextBox runat="server" ID="txtEditEmailAddress" Width="250" SelectionOnFocus="SelectAll"
                                    onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEditEmailAddress"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                                <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                                    ErrorMessage="* Invalid format " ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                    ControlToValidate="txtEditEmailAddress" CssClass="displayerror" ValidationGroup="update">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Confirm Email Address
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblEmailAddress2" />
                            </td>
                            <td>
                                <telerik:RadTextBox runat="server" ID="txtEditEmailAddress2" Width="250" SelectionOnFocus="SelectAll"
                                    onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEditEmailAddress2"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                                <asp:RegularExpressionValidator ID="emailValidator2" runat="server" Display="Dynamic"
                                    ErrorMessage="* Invalid format " ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                    ControlToValidate="txtEditEmailAddress2" CssClass="displayerror" ValidationGroup="update">
                                </asp:RegularExpressionValidator>
                                <asp:CompareValidator ID="emailCompareValidator" runat="server" ControlToValidate="txtEditEmailAddress2"
                                    ControlToCompare="txtEditEmailAddress" Operator="Equal" Type="String" Display="Dynamic"
                                    ErrorMessage="* Email mismatched " CssClass="displayerror" ValidationGroup="update"
                                    EnableClientScript="True">
                                </asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Mobile No
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblMobileNo" />
                            </td>
                            <td>
                                <telerik:RadTextBox runat="server" ID="txtEditMobileNo" SelectionOnFocus="SelectAll"
                                    Width="150" MaxLength="20">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Region
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRegion" />
                            </td>
                            <td>
                                <telerik:RadComboBox runat="server" ID="cbEditRegion" DataTextField="Description"
                                    DataValueField="ID" EmptyMessage="- select region -" DataSourceID="SqlDataSource_Regions"
                                    AppendDataBoundItems="true" MaxHeight="250" DropDownAutoWidth="Enabled" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="cbEditRegion" SetFocusOnError="True"
                                    ValidationGroup="update" ErrorMessage="*" Display="Dynamic" CssClass="displayerror" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Country
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCountry" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditCountry" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Reporting Manager/Supervisor
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblSupervisor" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditSupervisor" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Department
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblDepartment" />
                            </td>
                            <td>
                                <telerik:RadComboBox ID="cbEditDepartment" runat="server" DataSourceID="SqlDataSource_Department"
                                    DataTextField="description" DataValueField="id" EmptyMessage="- select department -"
                                    MaxHeight="300" AppendDataBoundItems="True" DropDownAutoWidth="Enabled" OnSelectedIndexChanged="cbDepartment_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="Other" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadTextBox runat="server" ID="txtEditDepartment" Visible="False" Width="120"
                                    EmptyMessage="Please specify" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="cbEditDepartment" SetFocusOnError="True"
                                    ValidationGroup="update" ErrorMessage="*" Display="Dynamic" CssClass="displayerror" />
                                <asp:RequiredFieldValidator ID="rfvOtherDepartment" runat="server" ControlToValidate="txtEditDepartment"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Client
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblClient" />
                            </td>
                            <td>
                                <telerik:RadComboBox runat="server" ID="cbEditClient" DataTextField="client" DataSourceID="SqlDataSource_Clients"
                                    DataValueField="clientID" EmptyMessage="- select client -" MaxHeight="300" DropDownAutoWidth="Enabled"
                                    AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="cbClient_SelectedIndexChanged"
                                    ToolTip="Please select/re-select Client to load Campaign List if necessary">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="Other" />
                                        <telerik:RadComboBoxItem Text="Shared" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadTextBox runat="server" ID="txtEditClient" Visible="False" Width="120"
                                    EmptyMessage="Please specify" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cbEditClient"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                                <asp:RequiredFieldValidator ID="rfvOtherClient" runat="server" ControlToValidate="txtEditClient"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" Enabled="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Campaign
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCampaign" />
                            </td>
                            <td>
                                <telerik:RadComboBox runat="server" ID="cbEditCampaign" DataTextField="campaign"
                                    DataSourceID="SqlDataSource_Campaigns" DataValueField="campaignID" EmptyMessage="- select campaign -"
                                    MaxHeight="300" DropDownAutoWidth="Enabled" AutoPostBack="True" AppendDataBoundItems="True"
                                    OnSelectedIndexChanged="cbCampaign_SelectedIndexChanged" ToolTip="">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="Other" />
                                        <telerik:RadComboBoxItem Text="Shared" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadTextBox runat="server" ID="txtEditCampaign" Visible="False" Width="120"
                                    EmptyMessage="Please specify" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="cbEditCampaign" SetFocusOnError="True"
                                    ValidationGroup="update" ErrorMessage="*" Display="Dynamic" CssClass="displayerror" />
                                <asp:RequiredFieldValidator ID="rfvOtherCampaign" runat="server" ControlToValidate="txtEditCampaign"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" Enabled="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Primary Language
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblLanguage" />
                            </td>
                            <td>
                                <telerik:RadComboBox runat="server" ID="cbEditLanguage" DataTextField="Language"
                                    DataSourceID="DataSource_Language" DataValueField="Id" EmptyMessage="- select language -"
                                    MaxHeight="300" DropDownAutoWidth="Enabled" AutoPostBack="True" AppendDataBoundItems="True"
                                    OnSelectedIndexChanged="cbEditLanguage_SelectedIndexChanged" ToolTip="Please select Language">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="Other" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadTextBox runat="server" ID="txtEditLanguage" Visible="False" Width="120"
                                    EmptyMessage="Please specify" />
                                <asp:RequiredFieldValidator ID="rfvOtherLanguage" runat="server" ControlToValidate="txtEditLanguage"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                I am interested in pursuing the following career path in Transcom
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCareerPath" />
                            </td>
                            <td>
                                <telerik:RadComboBox ID="cbEditCareerPath" runat="server" DataSourceID="SqlDataSource_CareerPath"
                                    DataTextField="Description" DataValueField="ID" DropDownAutoWidth="Enabled" EmptyMessage="- select career -"
                                    MaxHeight="300px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="cbEditCareerPath_SelectedIndexChanged">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="Other" />
                                    </Items>
                                </telerik:RadComboBox>
                                <telerik:RadTextBox runat="server" ID="txtEditCareerpath" Visible="False" Width="120"
                                    EmptyMessage="Please specify" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="cbEditCareerPath" SetFocusOnError="True"
                                    ValidationGroup="update" ErrorMessage="*" Display="Dynamic" CssClass="displayerror" />
                                <asp:RequiredFieldValidator ID="rfvOtherCareerpath" runat="server" ControlToValidate="txtEditCareerpath"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" Enabled="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                Please tell me what courses you are interested in taking for your professional growth
                                and development
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblCoursesInterested" />
                            </td>
                            <td>
                                <telerik:RadTextBox runat="server" ID="txtEditCoursesInterested" Width="300" Height="50"
                                    SelectionOnFocus="SelectAll" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEditCoursesInterested"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam">
                                I am interested in receiving updates via
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblReceiveUpdates" />
                            </td>
                            <td>
                                <telerik:RadComboBox ID="cbEditReceiveUpdates" runat="server" DataSourceID="SqlDataSource_UpdateVia"
                                    DataTextField="Description" DataValueField="ID" CheckBoxes="True" DropDownAutoWidth="Enabled"
                                    EmptyMessage="- select item/s -" MaxHeight="300px" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cbEditReceiveUpdates"
                                    SetFocusOnError="True" ValidationGroup="update" ErrorMessage="*" Display="Dynamic"
                                    CssClass="displayerror" />
                            </td>
                        </tr>
                        <tr>
                            <td class="labelMyTeam" style="width: 45%">
                            </td>
                            <td>
                            </td>
                            <td>
                                <telerik:RadAjaxPanel runat="server" ID="RadAjaxPanel2" ClientEvents-OnRequestStart="conditionalPostback"
                                    Width="100%">
                                    <div id="managecontrol2" style="width: 100%; padding-top: 10px">
                                        <asp:LinkButton runat="server" ID="btnUpdateProfile2" Text="Update Profile" OnClick="btnUpdateProfile_Click"
                                            ToolTip="Update Profile" ValidationGroup="update" Font-Size="7.5" CausesValidation="True" />
                                        <asp:LinkButton runat="server" ID="btnCancelUpdate2" Text="View Profile" Font-Size="7.5"
                                            ToolTip="View Profile" CausesValidation="False" OnClick="btnViewMyProfile_Click" />
                                    </div>
                                </telerik:RadAjaxPanel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidCIM" />
    </div>
    <div>
        <asp:SqlDataSource ID="SqlDataSource_Clients" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise%>"
            SelectCommand="SELECT distinct clientid, client FROM SUSL3PSQLDB04.CIMeReport.[dbo].[tbl_Hierarchy_Account] where enddate > getdate()  order by 2" />
        <asp:SqlDataSource ID="SqlDataSource_Campaigns" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise%>"
            SelectCommand="SELECT DISTINCT campaignID, campaign FROM SUSL3PSQLDB04.CIMeReport.[dbo].[tbl_Hierarchy_Account] where (clientid = @clientid and @clientid <> 0)">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbEditClient" Name="clientid" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource_Regions" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [ID], [Description] FROM [Regions] WHERE ([ACTIVE] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSource_Department" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [id], [description] FROM [refDepartments] WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="SqlDataSource_TypeOfSupport" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [id], [description] FROM [refSupportType] WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="DataSource_Language" runat="server" ConnectionString="<%$ ConnectionStrings: NuSkillCheck %>"
            SelectCommand="SELECT [Id], [Language] FROM [refLanguage] WHERE ([Active] = 1) ORDER BY [Language]" />
        <asp:SqlDataSource ID="DataSourse_EducLevel" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [Id], [Title] FROM [refEducationLevel] WHERE ([Active] = 1) ORDER BY [Id]" />
        <asp:SqlDataSource ID="SqlDataSource_CareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refCareerPath] WHERE ([Active] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="SqlDataSource_UpdateVia" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refUpdateVia] WHERE ([Active] = 1) ORDER BY [Description]" />
        <%--<asp:SqlDataSource ID="ds_DirectReports" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="sp_Get_TWWID_ByTeam" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="hidCIM" DefaultValue="0" Name="CIM" PropertyName="Value"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
    </div>
</asp:Content>
