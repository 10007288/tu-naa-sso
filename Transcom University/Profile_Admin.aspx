﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Profile_Admin.aspx.cs" Inherits="Profile_Admin"
    MasterPageFile="~/MasterPage.master" %>

<%@ OutputCache Location="None" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function RequestFullScreen(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                    args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollTopOffset;
                var scrollLeftOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxLoadingPanel ID="loadingPanel1" runat="server">
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridProfiles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridProfiles" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridProfiles" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div class="insidecontainer">
                        <%--<table runat="server" id="tblAdmin" width="100%" style="padding: 5px 0 5px 0">
                            <tr>
                                <td align="left" style="width: 131px">
                                    <telerik:RadTextBox ID="txtSearch" runat="server" Width="130px" ToolTip="Search by CIM/Username"
                                        EmptyMessage="- Cim/Username -" />
                                    <asp:RequiredFieldValidator ID="reqSearch" runat="server" ControlToValidate="txtSearch"
                                        ErrorMessage="" CssClass="displayerror" ValidationGroup="go" Display="Dynamic"
                                        SetFocusOnError="True" />
                                </td>
                                <td align="left" style="width: 30px">
                                    <telerik:RadButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btnSearchImage"
                                        ValidationGroup="go">
                                        <Image EnableImageButton="true" />
                                    </telerik:RadButton>
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="btnLogin" runat="server" Text="View My Profile >" PostBackUrl="ViewProfile.aspx"
                                        CausesValidation="False" CssClass="linkBtn" />
                                </td>
                            </tr>
                        </table>--%>
                        <div align="right" style="padding-bottom: 5px">
                            <telerik:RadTextBox ID="txtSearch" runat="server" Width="130px" ToolTip="Search by CIM/Username"
                                EmptyMessage="- Cim/Username -" Visible="False" />
                            <asp:LinkButton ID="btnLogin" runat="server" Text="View My Profile >" PostBackUrl="ViewProfile.aspx"
                                CausesValidation="False" CssClass="linkBtn" />
                        </div>
                        <div>
                            <telerik:RadGrid ID="gridProfiles" AutoGenerateColumns="False" runat="server" OnNeedDataSource="gridProfiles_NeedDataSource"
                                OnGridExporting="gridProfiles_GridExporting" OnExcelMLExportRowCreated="gridProfiles_ExcelMLExportRowCreated"
                                AllowSorting="True" AllowPaging="True" PageSize="50" CssClass="RadGrid1" Skin="Metro"
                                Height="800" GridLines="None" ShowFooter="False" ShowStatusBar="False">
                                <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                                    <Excel Format="ExcelML" />
                                    <Pdf Title="Transcom Unniversity Profiles" />
                                </ExportSettings>
                                <ClientSettings>
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" EnableVirtualScrollPaging="false" />
                                </ClientSettings>
                                <MasterTableView Name="CareerPath" AutoGenerateColumns="False" DataKeyNames="twwid"
                                    CommandItemDisplay="Top" ShowFooter="False" TableLayout="Auto" GridLines="None"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                    <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true"
                                        ShowExportToPdfButton="true" ShowExportToWordButton="true" ShowAddNewRecordButton="false" />
                                    <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                    <Columns>
                                        <telerik:GridBoundColumn UniqueName="Cimnumber" HeaderText="CIM Number" DataField="twwid" />
                                        <telerik:GridBoundColumn UniqueName="Firstname" HeaderText="Firstname" DataField="Firstname" />
                                        <telerik:GridBoundColumn UniqueName="Lastname" HeaderText="Lastname" DataField="Lastname" />
                                        <telerik:GridBoundColumn UniqueName="ReportingManager" HeaderText="Reporting Manager"
                                            DataField="TLName" />
                                        <telerik:GridBoundColumn UniqueName="ReportingManagerManager" HeaderText="Reporting Manager's Manager"
                                            DataField="TLName" />
                                        <telerik:GridBoundColumn UniqueName="Department" HeaderText="Department" DataField="DepartmentName" />
                                        <telerik:GridBoundColumn UniqueName="Country" HeaderText="Country" DataField="Country" />
                                        <telerik:GridDateTimeColumn UniqueName="companysite" HeaderText="Site" DataField="companysite" />
                                        <telerik:GridDateTimeColumn UniqueName="DateHired" HeaderText="Date Hire" DataField="startdate" />
                                        <telerik:GridDateTimeColumn UniqueName="EmailAddress" HeaderText="E-mail Address"
                                            DataField="email" />
                                        <telerik:GridDateTimeColumn UniqueName="MobileNo" HeaderText="Mobile No." DataField="MobileNo" />
                                        <telerik:GridBoundColumn UniqueName="Gender" HeaderText="Gender" DataField="gender" />
                                        <telerik:GridBoundColumn UniqueName="birthday" HeaderText="Date of Birth" DataField="birthday" />
                                        <telerik:GridBoundColumn UniqueName="YearsWithTranscom" HeaderText="Years With Transcom"
                                            DataField="YearsWithTranscom" />
                                        <telerik:GridBoundColumn UniqueName="Role" HeaderText="Role" DataField="Role" />
                                        <telerik:GridBoundColumn UniqueName="YearsWithRole" HeaderText="Years in current role"
                                            DataField="YearsWithRole" />
                                        <telerik:GridBoundColumn UniqueName="client" HeaderText="Client" DataField="ClientName" />
                                        <telerik:GridBoundColumn UniqueName="campaign" HeaderText="Campaign" DataField="CampaignName" />
                                        <telerik:GridBoundColumn UniqueName="Language" HeaderText="Primary Language" DataField="LanguageName" />
                                        <telerik:GridBoundColumn UniqueName="CourseInterests" HeaderText="Course Interests"
                                            DataField="courseinterests" />
                                        <telerik:GridBoundColumn UniqueName="careerpath" HeaderText="Career Path" DataField="careerpath" />
                                        <telerik:GridBoundColumn UniqueName="updatevia_name" HeaderText="I am interested in receiving updates via"
                                            DataField="updatevia_name" />
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnableRowHoverStyle="True">
                                    <Selecting AllowRowSelect="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
