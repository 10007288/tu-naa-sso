﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MyReports.aspx.cs" Inherits="MyReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExportExcel") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }
                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }

                if ($telerik.isSafari) {
                    var scrollTopOffset = document.body.scrollTop;
                    var scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    var scrollTopOffset = document.documentElement.scrollTop;
                    var scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function triggerValidation() {
                Page_ClientValidate();
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnView">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReports" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div>
                        <div align="center">
                            <telerik:RadComboBox ID="cmbReportList" DataTextField="ReportName" DataValueField="ReportID"
                                runat="server" EmptyMessage="- select report -" DropDownAutoWidth="Enabled" MaxHeight="300"
                                AutoPostBack="true" DataSourceID="dsReports" ToolTip="Select Report" OnSelectedIndexChanged="cmbReportList_SelectedIndexChanged" />
                        </div>
                        <table width="100%">
                            <tr align="center">
                                <td>
                                    <asp:Label runat="server" ID="lblRptName" Text="Please select a Report" Font-Size="X-Large" />
                                </td>
                            </tr>
                            <tr id="trHoriz" runat="server" visible="False">
                                <td>
                                    <hr />
                                </td>
                            </tr>
                        </table>
                        <table id="tblFilters" runat="server" visible="true" class="divReportParams">
                            <tr id="tr1" runat="server">
                            </tr>
                        </table>
                        <table id="tblViewAndExport" width="100%" visible="false" runat="server" style="padding-bottom: 10px">
                            <tr>
                                <td colspan="2">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <telerik:RadButton ID="btnView" runat="server" Text="View Report" OnClick="btnView_Click"
                                        ValidationGroup="vgTRA" />
                                </td>
                                <td align="right" style="line-height: 20px; vertical-align: bottom">
                                    <div style="vertical-align: bottom">
                                        <asp:LinkButton runat="server" ID="btnExportExcel" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                            Height="100%" OnClientClick="if(!confirm('Are you sure you want to export this report to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <telerik:RadGrid runat="server" ID="gridReports" GridLines="None" OnNeedDataSource="gridReports_NeedDataSource"
                            AllowSorting="True" EnableLinqExpressions="True" CssClass="RadGrid1" Skin="Metro">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" SaveScrollPosition="True" EnableVirtualScrollPaging="false" />
                                <%--<Scrolling AllowScroll="True" SaveScrollPosition="True" FrozenColumnsCount="1" />--%>
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" NoMasterRecordsText="No result for selected report."
                                AllowPaging="True" PageSize="50" ShowHeadersWhenNoRecords="False" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center"
                                TableLayout="Auto" Width="1000">
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="True" Mode="NextPrevAndNumeric" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:SqlDataSource ID="dsReports" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_MyReports" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="" Name="RoleName" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsParameters" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select reportid,telerikcontrol,label,datasourceid,datavaluefield,datatextfield
            from tbl_TranscomUniversity_Lkp_Report_Params where active = 1  order by ordinal"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsClassID" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select classid from tbl_Infinity_Cor_class where hidefromlist = 0 order by classid"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsTrainerCIM" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select trainerCIM from tbl_Infinity_Cor_class where hidefromlist = 0 group by trainerCIM order by trainerCIM"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsCourse" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="
			SELECT CourseID, CourseName 
            FROM tbl_NuCommUniversity_Lkp_Course 
            WHERE HideFromList = 0
			UNION ALL
			SELECT CourseID, Title AS CourseName FROM
			[dbo].[tbl_TranscomUniversity_Cor_Course]
			WHERE CourseTypeID = 2
			ORDER BY 2" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsCourse2" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT CourseID,Title as CourseName
            FROM tbl_TranscomUniversity_Cor_Course 
            WHERE HideFromList = 0  ORDER BY Title" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand=" SELECT Id,ClientName as Program 
            FROM [INTRANET].[dbo].[ref_TranscomUniversity_Class_Client] 
            WHERE [Active] = 1 order by ClientName" SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsClassName" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT distinct [ClassName] FROM [ref_TranscomUniversity_Class] WHERE [Active] = 1"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsAudience" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT AudienceID, AudienceName FROM [dbo].[tbl_TranscomUniversity_Cor_Audience] ORDER BY AudienceName"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsAssignCourseReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_lkp_AssignCourseReportingManager" SelectCommandType="StoredProcedure"
            OnSelecting="dsAssignCourseReportingManager_Selecting">
            <SelectParameters>
                <asp:Parameter Name="CIMNumber" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsCourseEnrollmentReportingManager" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SElECT DISTINCT SupCimNo 'Value', SupCimNo 'ReportingManager'
	            FROM tbl_TranscomUniversity_CourseEnrollments" SelectCommandType="Text">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsHarmonyClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_Client" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsDepartment" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            SelectCommand="SELECT [id] as DepartmentID, [description] as Department FROM nuskillcheck.dbo.refDepartments WHERE ([active] = 1) ORDER BY [description]" />
        <asp:SqlDataSource ID="dsClients" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise%>"
            SelectCommand="SELECT 0 as programid,'Shared' as program  union all        select distinct clientid as programid, client as program FROM SUSL3PSQLDB04.CIMeReport.[dbo].[tbl_Hierarchy_Account] where enddate > getdate()  order by 2" />

        <asp:SqlDataSource ID="dsManagers" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT cimnumber as ManagerCIM, firstname + ' ' + lastname as ManagerName
            FROM [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_Employee e
            INNER JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_Role R       
            ON e.RoleID = r.RoleID AND (e.CIMNumber IN (6183,9066,15722,12100,1372,27848,33094,36974,14384)
            OR RoleGroupID IN (3,5,7,10) OR e.RoleID IN (1105,1106,99,195,397,517,927))         
            INNER JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_EmployeeCompany AS C 
            ON C.EmployeeID = e.EmployeeID AND C.EndDate > GETDATE() order by firstname + ' ' + lastname" SelectCommandType="Text" />


        <asp:SqlDataSource ID="dsSupervisors" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="	select distinct e1.cimnumber as ManagerCIM, e1.firstname + ' ' + e1.lastname as ManagerName
	        from [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee e
	        inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employeecompany ec
	        on e.employeeid = ec.employeeid and  
	        ec.enddate >= getdate() left join [susl3psqldb02].CIMEnterprise.dbo.tbl_business_cor_account a
	        on isnull(e.departmentid, e.campaignid) = a.accountid
	        and a.enddate >= getdate()
	        left join [susl3psqldb02].CIMEnterprise.dbo.tbl_Hierarchy_Rlt_Employee ee
	        on ee.employeeid =  e.employeeid 
	        and ee.enddate >=getdate()
	        left join [susl3psqldb02].CIMEnterprise.dbo.tbl_Hierarchy_Rlt_Employee ee1
	        on ee1.[hierarchyid] =  ee.parenthierarchyid and ee1.enddate >=getdate()
	        left join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee e1
	        on e1.employeeid = ee1.employeeid order by 2 " SelectCommandType="Text"
        />

    </div>
</asp:Content>
