﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Test2.aspx.cs" Inherits="Test2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="container">
        <div id="skillset" class="sixteen columns ">
            <div class="eight columns">
                <div class="bars MT30">
                    <div id="bar-1">
                    </div>
                    <div id="bar-2">
                    </div>
                    <div id="bar-3">
                    </div>
                    <div id="bar-4">
                    </div>
                    <div id="bar-5">
                    </div>
                    <div id="bar-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jqbar.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#bar-1').jqbar({ label: 'ASP.NET', value: 99, barColor: '#D64747' });

            $('#bar-2').jqbar({ label: 'C#', value: 99, barColor: '#FF681F' });

            $('#bar-3').jqbar({ label: 'Javascript', value: 90, barColor: '#ea805c' });

            $('#bar-4').jqbar({ label: 'HTML5', value: 50, barColor: '#88bbc8' });

            $('#bar-5').jqbar({ label: 'CSS3', value: 60, barColor: '#939393' });

            $('#bar-6').jqbar({ label: 'jQuery', value: 70, barColor: '#3a89c9' });

        });
    </script>
</asp:Content>
