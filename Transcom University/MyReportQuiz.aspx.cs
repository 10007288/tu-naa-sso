﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Collections;
using System.Collections.Generic;

public partial class MyReportQuiz : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Roles.IsUserInRole("Admin") && !Roles.IsUserInRole("Coordinator") && !Roles.IsUserInRole("Manager") && !Roles.IsUserInRole("Trainer"))
        {
            Response.Redirect("NoAccess.aspx");
        }

        if (!Page.IsPostBack)
        {
            var roles = Roles.GetRolesForUser(Context.User.Identity.Name);
            var role1 = "";

            foreach (string role in roles)
            {
                if (role1 == "")
                {
                    role1 = role;
                }
                else
                {
                    role1 = role1 + "," + role;
                }
            }

            dsReports.SelectParameters["rolename"].DefaultValue = role1;
            cmbReportList.Text = "Quiz";
        }
    }

    protected void cmbReportList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cmbReportList.SelectedValue == "13")
            Response.Redirect("~/MyReportQuiz.aspx");
        else
            Response.Redirect("~/MyReports.aspx");
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        gridReports.Rebind();
        gridReports.Height = Unit.Pixel(700);
        gridReports.MasterTableView.Width = Unit.Pixel(1800);
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (gridReports.MasterTableView.Items.Count > 0)
        {
            gridReports.ExportSettings.ExportOnlyData = true;
            gridReports.ExportSettings.IgnorePaging = true;
            gridReports.ExportSettings.OpenInNewWindow = true;
            gridReports.ExportSettings.FileName = "Quiz_" + DateTime.Now;
            gridReports.MasterTableView.ExportToExcel();
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AlertBox", "alert('No Data to export');", true);
        }
    }

    protected void gridReports_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (e.RebindReason == GridRebindReason.ExplicitRebind || e.RebindReason == GridRebindReason.PostBackEvent)
        {
            if ((from checkedItem in cboDepartment.Items.ToList()
                 where checkedItem.Checked == true
                 select Convert.ToInt32(checkedItem.Value)).Count() > 0 &&
                 (from checkedItem in cboCourseName.Items.ToList()
                  where checkedItem.Checked == true
                  select Convert.ToInt32(checkedItem.Value)).Count() > 0 &&
                  dtpStartDate.SelectedDate.HasValue && dtpEndDate.SelectedDate.HasValue)
            {
                using (SqlConnection cn = new SqlConnection(WebConfigurationManager.ConnectionStrings["Intranet"].ConnectionString))
                {
                    List<int> selectedValues = (from checkedItem in cboCourseName.Items.ToList()
                                                where checkedItem.Checked == true
                                                select Convert.ToInt32(checkedItem.Value)).ToList();
                    string a = string.Join(",", selectedValues.ToArray());

                    cn.Open();
                    SqlCommand cmd = new SqlCommand(@"pr_TranscomUniversity_rpt_Quiz", cn);
                    cmd.CommandTimeout = 20000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CourseID",
                        string.Join(",", (from checkedItem in cboCourseName.Items.ToList()
                                          where checkedItem.Checked == true
                                          select Convert.ToInt32(checkedItem.Value)).ToArray()));
                    cmd.Parameters.AddWithValue("DepartmentID",
                                                string.Join(",", (from checkedItem in cboDepartment.Items.ToList()
                                                                  where checkedItem.Checked == true
                                                                  select Convert.ToInt32(checkedItem.Value)).ToArray()));
                    cmd.Parameters.AddWithValue("StartDate", dtpStartDate.SelectedDate);
                    cmd.Parameters.AddWithValue("EndDate", dtpEndDate.SelectedDate);
               
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.CommandTimeout = 0;
                    da.SelectCommand.CommandTimeout = 0;
                    da.Fill(dt);
                    gridReports.DataSource = dt;
                }
            }
            else
            {
                gridReports.DataSource = new int[] { };
            }
        }
    }

    protected void cboCat_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

    }
    protected void cboSubCat_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

    }
}