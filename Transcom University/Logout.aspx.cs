﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;

public partial class Logout : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        var redirectUrl = FormsAuthentication.LoginUrl + "?ReturnUrl=Default.aspx";
        FormsAuthentication.SignOut();
        Session.Clear();
        Session.RemoveAll();
        Session.Abandon();
        Response.Redirect(redirectUrl); 
    }
}