<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Courses.aspx.cs" Inherits="Courses" Title="Transcom University - Available Courses" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadScriptBlock ID="scriptBlock1" runat="server">
        <script type="text/javascript">
            function LaunchCourse(urlWithParams, isEnrolmentReq, title, courseId, loginStr, sender, args) {
                if (loginStr.indexOf("@") > -1) {
                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit) {
                            window.open(urlWithParams, 'PopupWindow', 'width=700,height=550,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');
                        }
                    });
                    var text = "Are you sure you want to take this course?";
                    radconfirm(text, callBackFunction, 350, 180, null, "Launch Course");
                    args.set_cancel(true);
                } else {
                    $.ajax({
                        type: "POST",
                        url: "ServiceContainer.aspx/GetPrerequisiteDetails",
                        data: '{courseId: "' + courseId + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function OnSuccess(response) {
                            document.getElementById('<%=hidCourseId.ClientID %>').value = courseId;
                            document.getElementById('<%=hidUrlWithParams.ClientID %>').value = urlWithParams;
                            var msg = response.d.MSG;
                            if (msg != '') {
                                document.getElementById('<%=lblPrerequisiteMessage.ClientID%>').innerHTML = msg;
                                var oWindow = window.radopen(null, "windowPrereq");
                                oWindow.add_beforeClose(onBeforeClose);
                                oWindow.center();
                            } else {

                                if (isEnrolmentReq == "True") {
                                    var wnd = $find("<%=windowEnrollCourse.ClientID %>");
                                    document.getElementById('<%=lblCourseTitle.ClientID %>').innerHTML = title;
                                    var oWindow = window.radopen(null, "windowEnrollCourse");
                                    oWindow.add_beforeClose(onBeforeClose);
                                    oWindow.center();
                                } else {
                                    var callBackFunction = Function.createDelegate(sender, function (shouldSubmit) {
                                        if (shouldSubmit) {
                                            window.open(urlWithParams, 'PopupWindow', 'width=700,height=550,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');
                                        }
                                    });
                                    var text = "Are you sure you want to take this course?";
                                    radconfirm(text, callBackFunction, 350, 180, null, "Launch Course");
                                    args.set_cancel(true);
                                }
                            }
                        },
                        failure: function (response) {
                            alert('Error occured cannot take exam.');
                        }
                    });
                }
            }
            function RequestFullScreen(sender, args) {
                if (args.get_eventTarget().indexOf("btnExport") >= 0) {
                    args.set_enableAjax(false);
                }
                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollTopOffset;
                var scrollLeftOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            function OnClientEntryAdding(sender, eventArgs) {
                setTimeout(function () { sender.closeDropDown(); }, 10);
            }
            //RADWINDOW
            function CloseEnrollCourseWindow(from1) {
                //setTimeout(function () { $find("<%=windowEnrollCourse.ClientID %>").close(); }, 3000);
                from = from1;
                var window = $find('<%=windowEnrollCourse.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            //CONFIRM CLOSING WINDOW
            var from;
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);
                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="RequestFullScreen" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourse" />
                    <%--<telerik:AjaxUpdatedControl ControlID="lblCourseTitle" />--%>
                    <telerik:AjaxUpdatedControl ControlID="lblCourseDesc" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="ddtSubcategory" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnEnrollCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridEmployee" />
                    <telerik:AjaxUpdatedControl ControlID="lblNote" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowEnrollCourse" runat="server" Title="Enrollment Required"
                Width="450px" Height="240px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center" style="padding: 10px">
                        <div style="padding-bottom: 10px">
                            <fieldset>
                                <legend align="right">Note</legend>
                                <label>
                                    Thank you for your interest in taking this course. Enrollment is required to proceed
                                    with this course.
                                </label>
                            </fieldset>
                        </div>
                        <div style="padding-bottom: 10px">
                            <asp:Label runat="server" ID="lblCourseTitle" Text="Course Title" />
                        </div>
                        <div style="padding-bottom: 15px">
                            <label>
                                <strong>Enroll Now?</strong></label>
                            <br />
                            <asp:LinkButton runat="server" ID="btnEnrollCourse" Text="YES" CssClass="linkBtn"
                                OnClick="btnEnrolCourse_Click" />
                            |
                            <asp:LinkButton runat="server" ID="btnCloseEnrollCourse" Text="NO" CssClass="linkBtn"
                                CausesValidation="False" />
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowPrereq" runat="server" Title="Pre-requisite requirements"
                Width="450px" Height="340px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="center" style="padding: 10px">
                        <div style="padding-bottom: 15px">
                            <fieldset>
                                <legend align="right">Note</legend>
                                <label>
                                    Thank you for your interest in taking this course. The following requirement/s must
                                    be fulfilled:
                                </label>
                            </fieldset>
                        </div>
                        <div style="padding-bottom: 20px">
                            <label>
                                <asp:Label runat="server" ID="lblPrerequisiteMessage" />
                            </label>
                        </div>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnSearch">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <%--<table width="100%" style="vertical-align: bottom" runat="server" id="tblDefault1">
                        <tr>
                            
                            <td align="right">

                            </td>
                        </tr>
                    </table>--%>
                    <div class="insidecontainer">
                        <table width="100%" runat="server" style="padding-bottom: 5px" id="tblExternal" visible="False">
                            <tr>
                                <td>
                                    <asp:Label ID="lblCourse" runat="server" Font-Size="Medium" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" style="padding-bottom: 5px" runat="server" id="tblDefault2">
                            <tr>
                                <td style="width: 15%;">
                                    <telerik:RadComboBox ID="cbCategory" DataTextField="Category" DataValueField="CategoryID"
                                        runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbCategorySelectedIndexChanged"
                                        CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                </td>
                                <td align="left" style="width: 35%;">
                                    <telerik:RadDropDownTree runat="server" ID="ddtSubcategory" DefaultMessage="- select subcategory -"
                                        DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                        DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" Width="300px" AutoPostBack="True"
                                        OnEntryAdded="ddtSubcategory_EntryAdded" OnClientEntryAdding="OnClientEntryAdding"
                                        Skin="Default">
                                        <DropDownSettings Width="400px" Height="250px" />
                                    </telerik:RadDropDownTree>
                                </td>
                                <td align="right" style="width: 33%;">
                                    <span class="searchLabel">
                                        <asp:Label ID="Label1" runat="server" Font-Size="14px">Today, I want to learn about: </asp:Label></span>
                                </td>
                                <td align="right" style="width: 14%;">
                                    <telerik:RadTextBox ID="txtSearch" runat="server" Width="210px" EmptyMessage="- search courses -"
                                        SelectionOnFocus="SelectAll" ToolTip="To view full library, type * then GO. Otherwise use the category/subcategory drop downs on the left to filter."
                                        EnabledStyle-HorizontalAlign="Center" />
                                </td>
                                <td align="right" style="width: 3%;">
                                    <telerik:RadButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btnSearchImage"
                                        ValidationGroup="go" ToolTip="To view full library, type * then GO. Otherwise use the category/subcategory drop downs on the left to filter.">
                                        <Image EnableImageButton="true" />
                                    </telerik:RadButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                </td>
                                <td colspan="2">
                                    <span class="searchLabel">
                                        <asp:Label ID="lblInstructions" runat="server" Font-Size="10px">To view full library, type * then GO. Otherwise use the category/subcategory
                                    drop downs on the left to filter</asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="line-height: 20px" align="right">
                                    <asp:LinkButton runat="server" ID="btnExport" CssClass="linkBtn" OnClick="btnExport_Click"
                                        CausesValidation="false" Height="100%" Font-Size="12px" OnClientClick="if(!confirm('Are you sure you want to export Course/s to Excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <telerik:RadGrid runat="server" ID="gridCourse" AutoGenerateColumns="False" GridLines="None"
                            OnNeedDataSource="GridCourseNeedDataSource" EnableLinqExpressions="True" AllowSorting="True"
                            AllowFilteringByColumn="False" OnItemDataBound="gridCourse_ItemDataBound" CssClass="RadGrid1"
                            Skin="Metro">
                            <GroupingSettings CaseSensitive="false" />
                            <FilterItemStyle HorizontalAlign="Center" />
                            <ClientSettings EnableRowHoverStyle="True">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CourseID,EncryptedCourseID,EnrolmentRequired,Title,Description,CourseTypeID"
                                EditMode="EditForms" NoMasterRecordsText="Course list is empty. Please select a category and subcategory."
                                EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="CourseID" HeaderText="CourseID" DataField="CourseID"
                                        AllowFiltering="False" SortExpression="CourseID" Display="False" />
                                    <%--<telerik:GridHyperLinkColumn UniqueName="Title" HeaderText="Title" DataTextField="Title"
                                        SortExpression="Title" HeaderStyle-Width="200" NavigateUrl="#" AllowFiltering="False" />--%>
                                    <telerik:GridTemplateColumn ReadOnly="True" UniqueName="Title" HeaderText="Title"
                                        SortExpression="Title" DataField="Title" AllowFiltering="False">
                                        <ItemStyle HorizontalAlign="Center" CssClass="gridHyperlink" />
                                        <HeaderStyle HorizontalAlign="Center" Width="200" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="linkCourseTitle" runat="server" Text='<%# Eval("Title") %>' OnClick="linkCourseTitle_Click" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Description" HeaderText="Description" DataField="Description"
                                        AllowFiltering="False" ItemStyle-Font-Size="90%" HeaderStyle-Width="300" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn UniqueName="Duration" HeaderText="Duration (min)" DataField="Duration"
                                        AllowFiltering="False" SortExpression="Duration" HeaderStyle-Width="100" />
                                    <telerik:GridCheckBoxColumn UniqueName="EnrolmentRequired" DataField="EnrolmentRequired"
                                        HeaderText="Enrolment Required?" AllowFiltering="False" SortExpression="EnrolmentRequired"
                                        HeaderStyle-Width="80" Display="False" />
                                    <telerik:GridBoundColumn UniqueName="TargetAudience" HeaderText="Target Audience"
                                        DataField="TargetAudience" AllowFiltering="False" HeaderStyle-Width="130" SortExpression="TargetAudience" />
                                    <telerik:GridBoundColumn UniqueName="LastUpdated" HeaderText="Last Updated" DataField="LastUpdated"
                                        AllowFiltering="False" HeaderStyle-Width="130" SortExpression="LastUpdated" DataFormatString="{0:d}" />
                                    <telerik:GridBoundColumn UniqueName="Subcategory" HeaderText="Course Path" DataField="Subcategory"
                                        AllowFiltering="False" SortExpression="Subcategory" ItemStyle-Font-Size="90%"
                                        Display="False" HeaderStyle-Width="150" />
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidCourseId" Value="" />
        <asp:HiddenField runat="server" ID="hidUrlWithParams" Value="" />
    </div>
</asp:Content>
